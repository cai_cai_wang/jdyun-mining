//let public = 'http://192.168.0.143';

var id_box_register = new Array(
		id_register_phone,
		id_register_code,
		id_register_password,
	    id_register_password2,
	    id_register_inviter);

var classErrTip = new Array(
	    "class_register_err_phoneNum",
	    "class_register_err_code",
	    "class_register_err_password",
	    "class_register_err_password2",
	    "class_register_err_inviter");


/**
 * 邀请链接注册
 */
$(function(){
	function GetQueryString(name)
	{
	    var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)");
	    var r = window.location.search.substr(1).match(reg);
	    if(r!=null)return  unescape(r[2]); return null;
	}
    if(GetQueryString("Code")!=null&&GetQueryString("Code")!=undefined){
        $("#id_register_inviter").val(GetQueryString("Code"));
        $("#id_register_inviter").attr("readonly","readonly");
    }
    if (is_weixn()){
    	
    	alert("微信打开");
    }
});


var registerFormFlgArr = new Array(false, false,false, false, true);

//表单验证
CallEvent();

function CallEvent(){
	for (var i = 0; i < id_box_register.length; i++){
		KeyUpEvent(id_box_register[i], registerFormFlgArr, classErrTip[i],id_register_submit);
	}
	ButtonChange(registerFormFlgArr, id_register_submit);
	passwordEye("password-btn");
}


function spanClick(e){
    e.preventDefault();
    let number = $('input[type=tel]').val();

    if (false == VarifyPhone($("#id_register_phone"))) {
        $('.class_register_err_phoneNum').show();
        $('input[type=tel]').focus();
        return;
    }else{
    	$('.class_register_err_phoneNum').hide();
        // return; 
    }
    //先判断手机号是否已存在
    $.ajax({
		url: public + '/User/exists',
        type: 'post',
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        data: {
        	userVertifyInfo: number
        },
        dataType: 'json',
        success: function (res) {
        	console.log(res,number);
        	if(res.code == 208){
        		$('.class_register_err_phoneNum').text("手机号码已存在");
        		$('.class_register_err_phoneNum').show();
        	}else{
        		var count = 120;
        	    $(".getCode").off("click");
        	    $(".getCode").text("倒计时" + count + "秒");
        	    $(".getCode").css({background:'gray'});
        	    var timer = setInterval(function () {
        	        count--;
        	        $(".getCode").text("倒计时" + count + "秒");
        	        if (count == 0) {
        	            clearInterval(timer);
        	            $(".getCode").on('click',spanClick);
        	            $(".getCode").text("重发验证码");
        	            $(".getCode").css({background:'#26bbf0'});
        	            code = ""; //清除验证码。如果不清除，过时间后，输入收到的验证码依然有效 
        	        }
        	    }, 1000);
        	    $.ajax({
        	        url: public + '/User/vertify',
        	        type: 'post',
        	        xhrFields: {
        	            withCredentials: true
        	        },
        	        crossDomain: true,
        	        data: {
						userVertifyInfo: number,
						businessCode:'REGIST'
        	        },
        	        dataType: 'json',
        	        success: function (res) {
        	            console.log(res.msg);
        	        }
        	    });
        	}
        }
	});
}
//获取验证码
//function getCode(){
    $('.getCode').on('click', spanClick);
//}


//提交注册
$("#id_register_submit").on('click', function (e) {
    e.preventDefault();
    let userName = $('input[type=tel]').val(),
        reCode = $('.recode>input').val(),
        pwdOne = $('#id_register_password').val(),
        pwdTwo = $('#id_register_password2').val(),
        getCode = $('.usercode>input').val();
//    if(getCode == "")getCode = "0";

    $.ajax({
        url:public+'/User/regist',
        type:"POST",
        data:{
            userVertifyInfo:userName,
            code:reCode,
            pwd:hex_md5(pwdOne),
            repeatPwd:hex_md5(pwdTwo),
            refereeCode:getCode,
            nickName:userName
        },
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        dataType:'json',
        success:function(res){
        	errTips(res.code);
            if(res.code === 200){
                window.location.href = 'login.html';
            }
        }
    });
})

function is_weixn(){
    var ua = navigator.userAgent.toLowerCase();
    if(ua.match(/MicroMessenger/i)=="micromessenger") {
        return true;
    } else {
        return false;
    }
}