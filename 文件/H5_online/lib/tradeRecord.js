let public = 'http://192.168.0.143';
let id = getQueryString('id');
$.ajax({
    url:public+"/Transaction/detail",
    type:'POST',
    data:{
        transactionId:id
    },
    xhrFields: {
        withCredentials: true
    },
    crossDomain: true,
    dataType:'json',
    success:function(res){
        console.log(res);
        if(res.code === 200){
            let htmls = `<p>发款方</p>
            <span>${res.data.fromUserNickName}</span>
            <p>收款方</p>
            <span>${res.data.toUserNickName}</span>
            <p>手续费</p>
            <span>${res.data.fee}</span><span>${res.data.coinType}</span></span>
            <p>交易时间</p>
            <span>${getTime(res.data.createTime)}</span>
            <p>备注</p>
            <span>${res.data.memo}</span>`
            $('.Records-title p>span').text(res.data.amount);
            $('.Records-title p>old').text(res.data.coinType);
            $('.records-list').html(htmls);
        }
    }
})

function getTime(times){
	var date = new Date(times); //时间戳为10位需*1000，时间戳为13位的话不需乘1000
	Y = date.getFullYear() + '-';
	M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '-';
	D = date.getDate() + ' ';
	h = (date.getHours() < 10 ? '0' + date.getHours() : date.getHours()) + ':';
	m = (date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes()) + ':';
	s = (date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds());
	return Y + M + D + h + m + s;
}

//获取url参数
function getQueryString(name) {
    var result = window.location.search.match(new RegExp("[\?\&]" + name + "=([^\&]+)", "i"));
    if (result == null || result.length < 1) {
        return "";
    }
    return result[1];
}