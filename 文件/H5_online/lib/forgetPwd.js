//let public = 'http://192.168.0.143',
 let pass = [];
let nameReg = /^[1][3,4,5,7,8][0-9]{9}$/,
    regCode = /^\d{6}$/,
    pwdReg = /^\w{6,20}$/;

var id_box_forget = new Array(
		id_forget_phone,
		id_forget_code,
		id_forget_password,
	    id_forget_password2);

var classErrTip_forget = new Array(
	    "class_forget_err_phoneNum",
	    "class_forget_err_code",
	    "class_forget_err_password",
	    "class_forget_err_password2");

var forgetFormFlgArr = new Array(false, false,false, false);

CallEvent();

function CallEvent(){
	for (var i = 0; i < id_box_forget.length; i++){
		KeyUpEvent(id_box_forget[i], forgetFormFlgArr, classErrTip_forget[i],id_forget_submit);
	}
	ButtonChange(forgetFormFlgArr, id_forget_submit);
	passwordEye("password-btn");
}

if(pass == false){
    $('input[type=submit]').attr('disabled',true).css({background:'gray'});
}

function spanClick(e){
	e.preventDefault();
    let number = $('.phone>input').val();
    if (!VarifyPhone($("#id_forget_phone"))){
        $('.class_forget_err_phoneNum').show();
        return;
    }else{
//        $('.phone .errorTip').text('');
    	$('.class_forget_err_phoneNum').hide();
        // return; 
    }
    
    //判断手机号码是否存在
    $.ajax({
		url: public + '/User/exists',
        type: 'post',
        data: {
			userVertifyInfo: number
        },
        dataType: 'json',
        success: function (res) {
        	if(res.code == 208){
        		var count = 120;
        	    $(".getCode").off("click");
        	    $(".getCode").text("倒计时" + count + "秒");
        	    $(".getCode").css({background:'gray'});
        	    var timer = setInterval(function () {
        	        count--;
        	        $(".getCode").text("倒计时" + count + "秒");
        	        if (count == 0) {
        	            clearInterval(timer);
        	            $(".getCode").on("click", spanClick); //启用按钮
        	            $(".getCode").text("重新发送验证码");
        	            $(".getCode").css({background:'#26bbf0'});
        	            code = ""; //清除验证码。如果不清除，过时间后，输入收到的验证码依然有效 
        	        }
        	    }, 1000);
        	    $.ajax({
        	        url: public + '/User/vertify',
        	        type: 'post',
        	        data: {
						userVertifyInfo: number,
						businessCode:'FINDPWD'
        	        },
        	        dataType: 'json',
        	        success: function (res) {
        	            console.log(res.msg);
        	        }
        	    });
        	}else{
        		$('.class_forget_err_phoneNum').text("号码不存在");
        		$('.class_forget_err_phoneNum').show();
        	}
        }
	});
    
}

$('.getCode').on('click', spanClick);

$("#id_forget_submit").on('click', function (e) {
    e.preventDefault();
    let userName = $('.phone>input').val(),
        reCode = $('.yzm>input').val(),
        pwdOne = $('#id_forget_password').val(),
        pwdTwo = $('#id_forget_password2').val();
    $.ajax({
        url:public+'/User/modifyPwd',
        type:"POST",
        data:{
            userVertifyInfo:userName,
            code:reCode,
            pwd:hex_md5(pwdOne),
            repeatPwd:hex_md5(pwdTwo),
        },
        dataType:'json',
        success:function(res){
        	errTips(res.code);
            if(res.code === 200){
                window.location.href = 'login.html';
            }
        }
    })
})