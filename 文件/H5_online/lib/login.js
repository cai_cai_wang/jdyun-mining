//let public = 'http://192.168.0.143';

var id_box_login = new Array(
		id_login_phone,
		null,
	    id_login_password);
var classErrTip = new Array(
	    "class_login_err_phoneNum",
	    "",
	    "class_login_err_password");

var loginFormFlg = new Array(false, true, false);


$(".password-btn").click(function(){
    if($(this).parent().find("input").attr("type")=="password"){
        $(this).parent().find("input").prop("type","text");
        $(this).css({"background":"url('images/password_none-see.png')no-repeat 0","background-size":"100% auto"});
    }else{
        $(this).parent().find("input").prop("type","password");
        $(this).css({"background":"url('images/password_see.png')no-repeat 0","background-size":"100% auto"});
    }
});



$('#id_login_submit').on('click',function(e){

    e.preventDefault();
    let name = $('#id_login_phone').val();
    let pwd = $('#id_login_password').val();
    pwd = hex_md5(pwd); //MD5
    $.ajax({
        url:public+'/User/login',
        type:'post',
        data:{
            username:name,
            password:pwd,
        },
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        success:function(res){
        	console.log(res);
            errTips(res.code,res.msg);
            if(res.code === 200){
                sessionStorage.setItem('isLogin','true');
                sessionStorage.setItem('uId',res.data.id);
                sessionStorage.setItem('uName',res.data.nickName);
                window.location.href = 'home.html';
            }
        }
    });
});

callEvent();
function callEvent(){
	for(var i=0;i<id_box_login.length;i++){
		KeyUpEvent(id_box_login[i],loginFormFlg, classErrTip[i],id_login_submit);
	}
	ButtonChange(loginFormFlg, id_login_submit);
}
	

