var id_box_modifyPass = new Array(
		id_modifyPass_phone,
		id_modifyPass_code,
		id_modifyPass_password,
		id_modifyPass_password2,null,null,null,
		id_modifyPass_loginPassword);

var classErrTip = new Array(
	    "class_modifyPass_err_phone",
	    "class_modifyPass_err_code",
	    "class_modifyPass_err_password",
	    "class_modifyPass_err_password2","","","",
	    "class_modifyPass_err_loginPassword");

var modifyPassFormFlg = new Array(true,false,false,false,true,true,true,false);

CallEvent();
function CallEvent(){
	for (var i = 0; i < id_box_modifyPass.length; i++){
		KeyUpEvent(id_box_modifyPass[i],modifyPassFormFlg, classErrTip[i],id_modifyPass_submit);
	}
	ButtonChange(modifyPassFormFlg, id_modifyPass_submit);
}

function getCode(){
	var count = 120;
    $(".yzm-btn").off("click");
    $(".yzm-btn").text("倒计时" + count + "秒");
    $(".yzm-btn").css({background:'gray'});
    var number = $("#id_modifyPass_phone").val();
    var timer = setInterval(function () {
        count--;
        $(".yzm-btn").text("倒计时" + count + "秒");
        if (count == 0) {
            clearInterval(timer);
            $(".yzm-btn").on('click',spanClick_modify);
            $(".yzm-btn").text("重发验证码");
            $(".yzm-btn").css({background:'#26bbf0'});
            code = ""; //清除验证码。如果不清除，过时间后，输入收到的验证码依然有效 
        }
    }, 1000);
    
    $.ajax({
        url: public + '/UserCenter/vertify',
        type: 'post',
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        data:{
            businessCode:'FINDTRADEPWD'
        },
        success: function (res) {
        	console.log(res);
        	errTips(res.code,res.msg);
        }
    });
}


function spanClick_modify(e){
	
    e.preventDefault();
	var number = $("#id_modifyPass_phone").val();
	getCode();
}

//获取验证码
$(".yzm-btn").on('click', spanClick_modify);

$("#id_modifyPass_submit").click(function(){
//	console.log("--");
    var code = $("#id_modifyPass_code").val();
    var logpwd = $('#id_modifyPass_loginPassword').val();
	var passwd = $("#id_modifyPass_password").val();
	var passwd2 = $("#id_modifyPass_password2").val();
	
	$.ajax({
        url:public+'/UserCenter/updateUserSecPwd',
        type:'post',
        data:{
            code:code,
            loginpwd:hex_md5(logpwd),
            password:hex_md5(passwd),
        	repeat:hex_md5(passwd2)
        },
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        success:function(res){
        	console.log(res);
            if(res.code === 200){
            	alert("交易密码设置修改成功!")
                window.location.href = 'mine.html';
            }
            errTips(res.code,res.msg);
        }
    });
});
