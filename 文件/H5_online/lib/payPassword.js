//let public = 'http://192.168.0.143';
let pass = [];

var id_box_tradePass = new Array(
		null,
		null,
		id_tradePass_password,
		id_tradePass_password2,
	    null,
	    null,
	    null,
	    id_tradePass_loginPassword);

var classErrTip = new Array(
	    "",
	    "",
	    "class_tradePass_err_password",
	    "class_tradePass_err_password2",
	    "",
	    "",
	    "",
	    "class_tradePass_err_loginPassword");

var tradePassFormFlgArr = new Array(true, true,false, false, true,true,true,false);
CallFun();
function CallFun(){
	for (var i = 0; i < id_box_tradePass.length; i++){
		KeyUpEvent(id_box_tradePass[i], tradePassFormFlgArr, classErrTip[i],id_tradePass_submit);
	}
	ButtonChange(tradePassFormFlgArr, id_tradePass_submit);
//	passwordEye("password-btn");
}




$(".password-btn").click(function(){
    if($(this).siblings().attr("type")=="password"){
        $(this).siblings().prop("type","text");
        $(this).css({"background":"url('images/password_none-see.png')no-repeat 0","background-size":"100% auto"});
    }else{
        $(this).siblings().prop("type","password");
        $(this).css({"background":"url('images/password_see.png')no-repeat 0","background-size":"100% auto"});
    }
});

if(pass.length === 0){
    $('input[type=submit]').attr('disabled',true).css({background:'#bbb'});
}

$("#id_tradePass_submit").on('click',function(e){
    e.preventDefault();
    let oldPwd = $('form p:nth-child(1)').find('input').val(),
        payPwd = $('form p:nth-child(2)').find('input').val(),
        payPwdEnter = $('form p:nth-child(3)').find('input').val();
    $.ajax({
        url:public+'/UserCenter/updateUserPwd',
        type:'POST',
        data:{
        	oldpwd:hex_md5(oldPwd),
            password:hex_md5(payPwd),
            repeat:hex_md5(payPwdEnter)
        },
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        dataType:'json',
        success:function(res){
            console.log(res);
            if(res.code === 200){
            	alert("登录密码修改成功，请重新登录")
            	sessionStorage.removeItem('isLogin');
                window.location.href = 'login.html';
            }
            if(res.code === 202){
            	alert("登录失效，请重新登录")
                sessionStorage.removeItem('isLogin');
                window.location.href = 'home.html';
            }
            errTips(res.code,res.msg);
        }
    });
});