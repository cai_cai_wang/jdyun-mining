let public = 'http://bbpc.info';
let isLogin = sessionStorage.getItem('isLogin');
let balance = '';


function varifyAmount(){
	let val = parseFloat($('input[type=text]').val());
	let userable = parseFloat(balance);
	if (val < 1000 || val > userable){
		$('input[type=button]').attr('disabled',true).css({background:'#bbb'});
        if (val<1000){
        	$(".class_minging_err_amount").text("1000起步");
        }else{
        	$(".class_minging_err_amount").text("余额不足");
        }
        $(".class_minging_err_amount").show();
    }else{
        $('input[type=button]').attr('disabled',false).css({background:'#3ABFF4'});
        $(".class_minging_err_amount").hide();
    }
}

if(isLogin){
	
	// input amount
    $('input[type=text]').on('input',function(e){
        
        varifyAmount();
    })
    //all button
    $('.fr.L_12_5').find('a').on('click',function(e){
        let val = $('.L_12_6 .L_12_7.num').text();
        $('input[type=text]').val(val);
        varifyAmount();
    })
    //查询币种资产
    $.ajax({
        url:public+'/UserCenter/userAsset',
        type:'POST',
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        success:function(res){
            console.log(res);
            if(res.code === 200){
                balance = res.data.balance;
                $('.L_12_6 .L_12_7.num').text(res.data.balance);
            }
            if(res.code === 500){
                alert('余额不足!');
            }
            if(res.code === 202){
                alert('登录失效请重新登录');
                sessionStorage.removeItem('isLogin');
                window.location.href = 'home.html';
            }
        }
    })

    //获取挖矿信息
    $.ajax({
        url:public+'/Transaction/lockRecords',
        type:'POST',
        dataType:'json',
        data:{
            coinType:'TEX',
            pageNumber:1,
            pageSize:20
        },
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        success:function(res){
            console.log(res);
            let htmls = '';
            if(res.code === 200){
                let data = res.data.list;
                for(let i = 0; i<data.length;i++){
                    htmls += `
                        <li class="L_13_8">
                            <span class="L_13_9 fl">${getTime(data[i].createTime)}</span>
                            <span class="L_13_10 fl">${data[i].initLockCoinAmount}</span>
                            <span class="L_13_11 fl">${data[i].initLockCoinAmount}</span>
                            <span class="L_13_6 fl" data-id=${i}><a>详情</a></span>
                        </li>`
                }
                htmls === ''?$('.L_13_2.history').html(''):$('.L_13_2.history li:nth-child(2)').html(htmls);
                $('.L_13_6.fl').find('a').on('click',function(e){
                    let index = $(this).parent().attr('data-id');
                    let htmls = `<div class="L_14_1">
                        <p class="L_14_2">开始时间</p>
                        <p class="L_14_3">${getTime(data[index].createTime)}</p>
                        <p class="L_14_2">挖矿数量</p>
                        <p class="L_14_3">${data[index].initLockCoinAmount}</p>
                        <p class="L_14_2">累计释放</p>
                        <p class="L_14_3">${parseFloat(data[index].initLockCoinAmount)-parseFloat(data[index].lockCoinAmount)}</p>
                        <p class="L_14_2">锁仓剩余</p>
                        <p class="L_14_3">${data[index].lockCoinAmount}</p>
                        <p class="L_14_2">累计收益</p>
                        <p class="L_14_3">${data[index].income}</p>
                    </div>`;
                    $('.L_14').html(htmls);
                    $('.L_14').css('display','block');

                })
            }
        }
    })
    
}

$('input[type=button]').on('click',function(e){
    if(!isLogin){
        alert('请先登录!');
        window.location.href = 'login.html';
        return;
    }
    let num = $('input[type=text]').val();
    layer.prompt({
        formType: 1,
        title: '请输入支付密码',
      }, function(value, index, elem){
    	value = hex_md5(value);
        //确认挖矿
        $.ajax({
            url:public+'/Transaction/lockPosition',
            type:'POST',
            data:{
                lockAmount:num,
                secPassword:value
            },
            xhrFields: {
                withCredentials: true
            },
            crossDomain: true,
            success:function(res){
                console.log(res);
                if(res.code === 200){
                    alert('挖矿成功');
                    window.location.reload();
                }else if(res.code === 202){
                    alert('登录失效请重新登录');
                    sessionStorage.removeItem('isLogin');
                    window.location.href = 'home.html';
                }else{
                	alert(res.msg);
                }
            }
        });
        layer.close(index);
    });
})




//时间戳转时间
function getTime(times){
	var date = new Date(times); //时间戳为10位需*1000，时间戳为13位的话不需乘1000
	Y = date.getFullYear() + '-';
	M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '-';
	D = date.getDate() + ' ';
	h = (date.getHours() < 10 ? '0' + date.getHours() : date.getHours()) + ':';
	m = (date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes()) + ':';
	s = (date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds());
	return Y + M + D + h + m + s;
}