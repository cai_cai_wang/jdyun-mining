let public = 'http://bbpc.info';
let isLogin = sessionStorage.getItem('isLogin');

if(isLogin){
    $.ajax({
        url:public+'/Miner/getMineralPoolTex',
        type:'POST',
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        dataType:'json',
        success:function(res){
            console.log(res);
            if(res.code === 200){
                $('.items.tex .total span').text(res.data.poolAmount);
                $('.items.tex .earnings span').text(res.data.miningRate);
                $('.items.tex .today-earnings').text(res.data.miningCoin);
            }
            if(res.code === 202){
                alert('登录失效请重新登录');
                sessionStorage.removeItem('isLogin');
                window.location.href = 'home.html';
            }
        }
    })
}