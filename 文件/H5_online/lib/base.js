let public = 'http://bbpc.info';
let isLogin = sessionStorage.getItem('isLogin');

//判断是否登陆
if(isLogin){
    $('.isLogin').text('注销')
    $.ajax({
        url:public+'/Miner/minerInfo',
        type:'POST',
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        success:function(res){
            console.log(res);
            if(res.code === 200){
                let Assets = $('.L_6_9').find('.L_6_11');
                Assets[0].innerHTML = res.data.honor;
                Assets[1].innerHTML = res.data.lockedCoin;
                Assets[2].innerHTML = res.data.miningCoin;
                $('.L_6_14.fl').find('.L_6_15').text('普通');
                $('.L_6_14.fl').find('.L_6_16').text(res.data.level);
                $('.L_6_14.fr').find('.rank').text(res.data.rank);
                $('.L_6_18.fl').find('.L_6_20').text(res.data.poolAmountInit);
                $('.L_6_19.fr').find('.L_6_20').text(res.data.poolAmount);
            }
            if(res.code === 202){
                alert('登录失效请重新登录');
                sessionStorage.removeItem('isLogin');
                window.location.href = 'home.html';
            }
        }
    })

}else{
    $('.isLogin').text('登录')
}

//注销
$('.isLogin').on('click', function () {
	if(isLogin){
		$.post(public + '/User/logout', function (res) {
        if (res.code === 200) {
            sessionStorage.removeItem('isLogin');
            window.location.href = 'home.html';
        }
        if(res.code === 211){

        }
        if(res.code === 202){
            alert('登录失效请重新登录');
            sessionStorage.removeItem('isLogin');
            window.location.href = 'home.html';
        }
    })
	}else{
		window.location.href = 'login.html';
	}
    
});