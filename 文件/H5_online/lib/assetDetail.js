let public = 'http://bbpc.info';
let isLogin = sessionStorage.getItem('isLogin');
let date = Date.parse(new Date())/1000;
let coin = getQueryString('coin'),
	coinall = JSON.parse(sessionStorage.getItem('coin'));

if(isLogin){
	let htmls = '';
	for(let i = 0;i<coinall.length;i++){
		htmls += `<li><span>${coinall[i]}</span></li>`
	}
	$('.select_box .select_txt').text(coin);
	$('.select_box .option').html(htmls);
	changeCoin();
	$('.change_mony.lt').attr({href:'transferOut.html'});
	$('.get_mony.rt').attr({href:'transferIn.html'});
	RefreshPage(coin);
	getAsset(coin);
}

//切换币种
function changeCoin(){
    $(".select_box").click(function (event) {
		event.stopPropagation();
		$(this).find(".option").toggle();
		$(this).parent().siblings().find(".option").hide();
	});

	$(document).click(function (event) {
		var eo = $(event.target);
		if ($(".select_box").is(":visible") && eo.attr("class") != "option" && !eo.parent(".option").length)
			$('.option').hide();
	});

	$(".option li").click(function () {
		check_value = $(this).text();
		$(this).parent().siblings(".select_txt").text(check_value);
		RefreshPage(check_value.trim());
		getAsset(check_value.trim());
	});
}

//获取资产
function getAsset(coin){
    $.ajax({
        url:public+'/UserCenter/userAsset',
        type:'POST',
		xhrFields: {
            withCredentials: true
        },
		crossDomain: true,
		data:{
			coinType:coin
		},
        dataType:'json',
        success:function(res){
			console.log(res);
			if(res.code === 200){
				let htmls = `<span class="col">${parseFloat(res.data.balance).toFixed(4)}</span>
				<span>${parseFloat(res.data.freezeCoin).toFixed(4)}</span>
				<span>${parseFloat(res.data.lockedCoin).toFixed(4)}</span>`;
				$('.content .top').find('p').text(parseFloat(res.data.asset).toFixed(4));
				$('.content .top').find('span').text(res.data.coinType);
				$('.content .num').html(htmls);
			}
        }
    })
}

//获取转账记录
function RefreshPage(coin){
    $.ajax({
        url:public+'/Transaction/bdexCoinsRecords',
        type:'POST',
        data:{
			coinType:coin,
			pageNumber:1,
			pageSize:20
		},
		xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        dataType:'json',
        success:function(res){
			console.log(res);
			let id = sessionStorage.getItem('uId');
			if(res.code === 200){
				let data = res.data.list,
					htmls = '';
				data.sort(soryBy('id'));
				for(let i=0;i<data.length;i++){
					htmls += `<a href="./tradeRecord.html?id=${data[i].id}"><li>
					<i class="lt i_record"></i>
					<div class="line lt">
					  <div class="lt time">
						<span class="time_top">${data[i].orderId}</span>
						<span class="time_down">${getTime(data[i].createTime)}</span>
					  </div>
					  <div class="rt count">
						<span class="lt">${data[i].fromUser === id?'-'+data[i].amount:'+'+data[i].amount}&nbsp</span>
						<span class="rt">${data[i].coinType}</span>
					  </div>
					</div>
				  </li></a>`
				}
				$('.record .assetDetail').html(htmls);
			}
        }
    })
}

//时间戳转时间
function getTime(times){
	var date = new Date(times); //时间戳为10位需*1000，时间戳为13位的话不需乘1000
	Y = date.getFullYear() + '-';
	M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '-';
	D = date.getDate() + ' ';
	h = (date.getHours() < 10 ? '0' + date.getHours() : date.getHours()) + ':';
	m = (date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes()) + ':';
	s = (date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds());
	return Y + M + D + h + m + s;
}

//获取url参数
function getQueryString(name) {
    var result = window.location.search.match(new RegExp("[\?\&]" + name + "=([^\&]+)", "i"));
    if (result == null || result.length < 1) {
        return "";
    }
    return result[1];
}

function soryBy(value){
	return function(a,b){
		return b[value] - a[value]
	}
}