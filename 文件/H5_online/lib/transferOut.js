let Logining = sessionStorage.getItem('isLogin');
let balance = '',
	check_value='';
var id_box_transferOut = new Array(
	id_transferOut_amount,
	id_transferOut_userId,
	id_transferOut_payPassword,
	id_transferOut_notes,
	id_transferOut_phone);

var classErrTip_TransferOut = new Array(
	"class_transferOut_err_amount",
	"class_transferOut_err_userId",
	"class_transferOut_err_password",
	"class_transferOut_err_notes",
	"class_transferOut_err_phone");

var forgetFormFlgArr = new Array(false, false, false, true, true);


function CallFunction() {
	for (var i = 0; i < id_box_transferOut.length; i++) {
		KeyUpEvnet_T(id_box_transferOut[i], classErrTip_TransferOut[i], forgetFormFlgArr, id_transferOut_submit);
	}
	ButtonChange(forgetFormFlgArr, id_transferOut_submit);
}

function KeyUpEvnet_T(id, tipClass, flgArr, idBtn) {
	$(id).keyup(function () {
		var idArr = id.id.split("_");
		var classTmp = $("." + tipClass);
		switch (idArr[2]) {
			case "amount":
				$(this).val($(this).val().replace(/[^\d{1,}\.\d{1,}|\d{1,}]/g, ''));
				if(parseFloat($(this).val()) > parseFloat(balance)){
					$('.usernum em').show();
				}else{
					$('.usernum em').hide();
				}
				var amount = $(this).val();
				flgArr[0] = true;
				break;
			case "userId":
				$(this).val($(this).val().replace(/[^\d]/g, ''));
				var userId = $(this).val();
				var CodeReg = /^[0-9]\d{7}(?!\d)$/;
				flgArr[1] = CodeReg.test(userId);
				if (!flgArr[1]) {
					classTmp.show();
				} else {
					classTmp.hide();
				}
				break;
			case "payPassword":
				var passwordReg = /^\S{6,16}$/;
				var paypass = $(this).val();
				flgArr[2] = passwordReg.test(paypass);
				if (!flgArr[2]) {
					classTmp.show();
				} else {
					classTmp.hide();
				}
				break;
			case "notes":
				var checkReg = /^[\u4e00-\u9fa5\w]{1,50}$/;
				var notes = $(this).val();
				if (notes != "") {
					flgArr[3] = checkReg.test(notes);
				} else {
					flgArr[3] = true;
				}

				if (!flgArr[3]) {
					classTmp.show();
				} else {
					classTmp.hide();
				}
				break;
			case "phone":
				var phoneReg = /^[1][3,4,5,7,8][0-9]{9}$/;
				var phone = $(this).val();
				if (phone != "") {
					flgArr[4] = phoneReg.test(phone);
				} else {
					flgArr[4] = true;
				}
				if (!flgArr[4]) {
					classTmp.show();
				} else {
					classTmp.hide();
				}
				break;
		}
		ButtonChange(flgArr, idBtn);

	});
}

$(function () {

	$(".select_box").click(function (event) {
		event.stopPropagation();
		$(this).find(".option").toggle();
		$(this).parent().siblings().find(".option").hide();
	});

	$(document).click(function (event) {
		var eo = $(event.target);
		if ($(".select_box").is(":visible") && eo.attr("class") != "option" && !eo.parent(".option").length)
			$('.option').hide();
	});

	$(".option li").click(function () {
		check_value = $(this).text();
		$(this).parent().siblings(".select_txt").text(check_value);
		RefreshPage(check_value.trim());
	});
});

//改变币种后刷新页面
function RefreshPage(coinType) {
	let url = "images/"+coinType.toLowerCase()+".png"
	getCoin(coinType);
	$('form .usernum').find('i').css("background-image","url("+ url +")");
}

//获取每个币种的余额
function getCoin(coin){
	$.ajax({
        url:public+'/UserCenter/userAsset',
		type:'POST',
		data:{
			coinType:coin
		},
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        success:function(res){
            console.log(res);
            if(res.code === 200){
				balance=res.data.balance;
            }
            if(res.code === 202){
                alert('登录失效请重新登录');
                sessionStorage.removeItem('isLogin');
                window.location.href = 'home.html';
            }
        }
    })
}

//确认转出
$('button').on('click', function (e) {
	e.preventDefault();
	if(!Logining){
		alert('请先登录!');
	}
	let num = $('.usernum>input').val(),
		id = $('.userId>input').val(),
		pwd = $('input[type=password]').val(),
		Text = $('.bzxx>input').val(),
		phone = $('.userphone>input').val();
	$.ajax({
		url: public + '/Transaction/bdexCoins',
		type: 'POST',
		data: {
			coinType:check_value.trim(),
			to: id,
			amount: num,
			secPassword: hex_md5(pwd)
		},
		xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
		dataType: 'json',
		success: function (res) {
			console.log(res)
			if(res.code === 200){
				alert('转账成功!');
				//转账成功后发送短信提示
				if(VarifyPhone(id_transferOut_phone)){
					$.ajax({
	        	        url: public + '/User/vertify',
	        	        type: 'post',
	        	        data: {
	        	            userVertifyInfo: phone
	        	        },
	        	        dataType: 'json',
	        	        success: function (res) {
	        	            console.log(res.msg);
	        	        }
	        	    });
				}
				window.location.reload();
			}
			if(res.code === 202){
				alert('登录失效请重新登录');
                sessionStorage.removeItem('isLogin');
                window.location.href = 'home.html';
			}
			if(res.code === 500){
				alert(res.msg);
			}
		}
	})
});

$(".password-btn").click(function () {
	if ($(this).parent().find("input").attr("type") == "password") {
		$(this).parent().find("input").prop("type", "text");
		$(this).css({
			"background": "url('images/password_none-see.png')no-repeat 0",
			"background-size": "100% auto"
		});
	} else {
		$(this).parent().find("input").prop("type", "password");
		$(this).css({
			"background": "url('images/password_see.png')no-repeat 0",
			"background-size": "100% auto"
		});
	}
});



//判断没有登录就不验证与提交
if(isLogin){
	CallFunction();
	getCoin('ETH');
}