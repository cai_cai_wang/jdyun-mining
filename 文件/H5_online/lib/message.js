let public = 'http://bbpc.info';
let isLogin = sessionStorage.getItem('isLogin');

if (isLogin) {
    $.ajax({
        url: public + '/UserCenter/messages',
        type: 'POST',
        data: {
            flag: 0,
            pageNumber: 1,
            pageSize: 20
        },
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        success: function (res) {
            console.log(res);
            let htmls = '';
            if (res.code === 200) {
                for (let i = 0; i < res.data.list.length; i++) {
                    htmls += `<div class="message-items">
                            <p class="time"></p>
                            <p class="reward"></p>
                            <p class="task"></p>
                            <p class="giving"></p>
                            <span><span></span></span>
                        </div>`
                }
                htmls === '' ? $('.message-center').html('您还没有消息') : $('.message-center').html(htmls);
            }
            if (res.code === 202) {
                alert('登录失效请重新登录');
                sessionStorage.removeItem('isLogin');
                window.location.href = 'home.html';
            }
        }
    })
}