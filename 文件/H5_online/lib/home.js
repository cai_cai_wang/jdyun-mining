let public = 'http://bbpc.info';
let isLogin = sessionStorage.getItem('isLogin');

$('.L_6_1').on('click',function(e){
	window.location.href = 'minning.html';
});

//判断是否登陆
if(isLogin){
    $('.isLogin').text('注销')
    $.ajax({
        url:public+'/Miner/minerInfo',
        type:'POST',
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        success:function(res){
            console.log(res);
            if(res.code === 200){
                let Assets = $('.L_6_9').find('.L_6_11');
                Assets[0].innerHTML = res.data.honor;
                Assets[1].innerHTML = res.data.lockedCoin;
                Assets[2].innerHTML = res.data.miningCoin;
                $('.L_6_14.fl').find('.L_6_16').text(res.data.level);
                $('.L_6_14.fr').find('.rank').text(res.data.rank);
                $('.L_6_18.fl').find('.L_6_20').text(res.data.poolAmountInit);
                $('.L_6_19.fr').find('.L_6_20').text(res.data.poolAmount);
            }
            if(res.code === 202){
                alert('登录失效请重新登录');
                sessionStorage.removeItem('isLogin');
                window.location.href = 'home.html';
            }
        }
    })
    //获取动态热点
    getMsg(4);


}else{
    $('.isLogin').text('登录')
}

//注销
$('.isLogin').on('click', function () {
	if(isLogin){
		$.post(public + '/User/logout', function (res) {
        if (res.code === 200) {
            sessionStorage.removeItem('isLogin');
            window.location.href = 'home.html';
        }
        if(res.code === 211){

        }
        if(res.code === 202){
            alert('登录失效请重新登录');
            sessionStorage.removeItem('isLogin');
            window.location.href = 'home.html';
        }
    })
	}else{
		window.location.href = 'login.html';
	}
    
});

//获取更多的动态热点
$('.fr.L_9_2').on('click',function(e){
    getMoreMsg(5,20);
})


function getMsg(num){
    $.ajax({
        url:public+'/HotTopic/index',
        type:'POST',
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        data:{
            number:num
        },
        success:function(res){
            console.log(res);
            let htmls = '';
            if(res.code === 200){
                for(let i=0;i<res.data.length;i++){
                    htmls +=`<li class="L_9_4">
                        <a href="javasrcipt:;">
                            <img class="L_9_5 fl" src="images/21.png" />
                            <span class="L_9_6 fl">${res.data[i].title}</span>
                            <span class="L_9_7 fl">${res.data[i].date}</span>
                        </a>
                    </li>`
                }
                $('.L_9_3.hotMsg').html(htmls);
            }
            if(res.code === 202){
                alert('登录失效请重新登录');
                sessionStorage.removeItem('isLogin');
                window.location.href = 'home.html';
            }
        }
    })
}

function getMoreMsg(num,page){
    $.ajax({
        url:public+'/HotTopic/more',
        type:'POST',
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        data:{
            pageNumber:num,
            pageSize:page
        },
        success:function(res){
            console.log(res);
            let htmls = '';
            if(res.code === 200){
            }
            if(res.code === 202){
                alert('登录失效请重新登录');
                sessionStorage.removeItem('isLogin');
                window.location.href = 'home.html';
            }
        }
    })
}

//倒计时
countTime();
function countTime() {
    //获取当前时间  
    var date = new Date();
    var now = date.getTime();
    //设置截止时间  
    var str = "2018/9/20 00:00:00";
    var endDate = new Date(str);
    var end = endDate.getTime();

    //时间差  
    var leftTime = end - now;
    //定义变量 d,h,m,s保存倒计时的时间  
    var d, h, m, s;
    if (leftTime >= 0) {
        d = Math.floor(leftTime / 1000 / 60 / 60 / 24);
        h = Math.floor(leftTime / 1000 / 60 / 60 % 24);
        m = Math.floor(leftTime / 1000 / 60 % 60);
        s = Math.floor(leftTime / 1000 % 60);
    }
    //将倒计时赋值到div中  
    $('.L_6_7.fr .L_6_8.day').text(d);
    $('.L_6_7.fr .L_6_8.hours').text(h);
    $('.L_6_7.fr .L_6_8.minutes').text(m);
    $('.L_6_7.fr .L_6_8.seconds').text(s);
    setTimeout(countTime, 1000);
}