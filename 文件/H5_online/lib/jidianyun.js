let public = 'http://bbpc.info';
let isLogin = sessionStorage.getItem('isLogin');

if(isLogin){
    $.ajax({
        url:public+'/UserCenter/userWallets',
        type:'POST',
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        success:function(res){
            console.log(res);
            if(res.code === 200){
                let htmls = '',
                    coin=[];
                for(let i =0;i<res.data.length;i++){
                    htmls += `<li  class=${res.data[i].coinType.toLowerCase()}>
                   <a href='./assetDetails.html?coin=${res.data[i].coinType}'><span class="fl"><i></i>${res.data[i].coinType}</span>
                    <span class="fr">${res.data[i].asset}<i></i></span></a>
                    </li>`;
                    coin.push(res.data[i].coinType);
                }
                sessionStorage.setItem('coin',JSON.stringify(coin));
                $('.yun-list ul').html(htmls);
            }
            if(res.code === 202){
                alert('登录失效请重新登录');
                sessionStorage.removeItem('isLogin');
                window.location.href = 'home.html';
            }
        }
    })
}


