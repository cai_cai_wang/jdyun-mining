let public = 'http://bbpc.info';
let isLogin = sessionStorage.getItem('isLogin');


//判断是否登陆
if(isLogin){
    $('.isLogin').text('注销')
    $.ajax({
        url:public+'/Miner/minerInfo',
        type:'POST',
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        success:function(res){
            console.log(res);
            if(res.code === 200){
                let Assets = $('.L_6_9').find('.L_6_11');
                Assets[0].innerHTML = res.data.honor;
                Assets[1].innerHTML = res.data.lockedCoin;
                Assets[2].innerHTML = res.data.miningCoin;
                $('.L_6_14.fl').find('.L_6_15').text('普通');
                $('.L_6_14.fl').find('.L_6_16').text(res.data.level);
                $('.L_6_14.fr').find('.rank').text(res.data.rank);
                $('.L_6_18.fl').find('.L_6_20').text(res.data.poolAmountInit);
                $('.L_6_19.fr').find('.L_6_20').text(res.data.poolAmount);
            }
            if(res.code === 202){
                alert('登录失效请重新登录');
                sessionStorage.removeItem('isLogin');
                window.location.href = 'home.html';
            }
        }
    })

}else{
    $('.isLogin').text('登录')
}

//注销
$('.isLogin').on('click', function () {
	if(isLogin){
		$.post(public + '/User/logout', function (res) {
        if (res.code === 200) {
            sessionStorage.removeItem('isLogin');
            window.location.href = 'home.html';
        }
        if(res.code === 211){

        }
        if(res.code === 202){
            alert('登录失效请重新登录');
            sessionStorage.removeItem('isLogin');
            window.location.href = 'home.html';
        }
    })
	}else{
		window.location.href = 'login.html';
	}
    
});

if(typeof StatusCode == "undefined"){
	var StatusCode = {};
	StatusCode.OK 			 			= 200;	//接口正常返回目标数据
	StatusCode.UNDEFINED_ERROR 			= 201;	//未定义的错误
	StatusCode.ACCESS_DENIED 			= 202;	//因不符合访问的条件服务端拒绝提供服务
	StatusCode.SERVICE_BUSY 			= 203;	//服务繁忙
	StatusCode.FREQUENT_REQUEST 		= 204;	//频繁请求接口引发的错误
	StatusCode.ILLEGAL_PARAMETER_VALUE 	= 205;	//输入错误，请重新输入
	StatusCode.DATABASE_ERROR 			= 206;	//服务端内部数据错误
	StatusCode.SMS_SEND_FAILED 			= 207;	//（验证码）信息发送失败
	StatusCode.ACCOUNT_EXISTS 			= 208;	//账户已存在
	StatusCode.ACCOUNT_NOT_EXISTS		= 209;	//账户不存在
	StatusCode.ACCOUNT_LOCKED			= 210;	//账户已锁定
	StatusCode.ACCOUNT_FOBIDDEN			= 211;	//账户已禁用
	StatusCode.ACCOUNT_LOGIN_FAIL		= 212;	//登录失败，用户名或者密码错误
	StatusCode.CODE_ERROR				= 213;	//验证码错误
	StatusCode.CODE_EXPIRED				= 214;	//验证码失效
	StatusCode.REFEREE_CODE_ERROR		= 215;	//推荐码不存在
	StatusCode.CAPTCHA_CODE_ERROR		= 216;	//图形验证码错误
	StatusCode.ACCOUNT_NAME_ERROR		= 217;	//用户名错误
	StatusCode.ACCOUNT_PWD_ERROR		= 218;	//用户密码错误
	StatusCode.PHONE_CODE_SEND_LIMIT	= 219;	//手机验证码发送已经超过当日最大次数限制
}

/**
 * 手机号码有效性验证
 * @param {*} id 
 */
function VarifyPhone(id) {
    var phoneReg = /^[1][3,4,5,7,8][0-9]{9}$/;
	return phoneReg.test($(id).val());
}

/**
 * 密码验证
 * @param {*} id 
 */
function VarifyPassword(id){
    var passwordReg = /^\S{6,16}$/;
    return passwordReg.test($(id).val());
}

/**
 * 验证码有效性验证
 * @param {*} id 
 */
function VarifyCode(id) {
    var CodeReg = /^[0-9]\d{5}(?!\d)$/;
	return CodeReg.test($(id).val());
}

/**
 * 邀请人校验
 * @param {*} id 
 */
function VarifyInviter(id){
	var CodeReg = /^[a-zA-Z0-9]{4}$/;
    return CodeReg.test($(id).val());
}

/**
 * 校验姓名
 * @param id
 * @returns
 */
function VarifyName(id){
	var NameReg = /^[\u4E00-\u9FA5\uf900-\ufa2d·s]{2,20}$/;
	return NameReg.test($(id).val());
}
/**
 * 校验身份证
 * @param id
 * @returns
 */
function VarifyIdcard(id){
	var regIdNo = /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/;  
	return regIdNo.test($(id).val());
}

function keyUpOperate(flg, classErr){
	classTmp = $("." + classErr);
	if (!flg){
		classTmp.show();
	}else{
		classTmp.hide();
	}
}
function KeyUpEvent(id,arrFlg,classErr,idBtn){
	
	$(id).keyup(function(){
		var idArr = id.id.split("_");
		switch(idArr[2]){
		case "phone":
			arrFlg[0] = VarifyPhone(id);
			keyUpOperate(arrFlg[0], classErr);
			break;
		case "code":
			arrFlg[1] = VarifyCode(id);
			keyUpOperate(arrFlg[1], classErr);
			break;
		case "password":
			arrFlg[2] = VarifyPassword(id);
			keyUpOperate(arrFlg[2], classErr);
			break;
		case "password2":
			var id_password1 = "#" + idArr[0]+ "_" + idArr[1] + "_" + "password";
			var id_password2 = "#" + idArr[0]+ "_" + idArr[1] + "_" + "password2";
            arrFlg[3] = ($(id_password1).val() == $(id_password2).val());
            keyUpOperate(arrFlg[3], classErr);
			break;
		case "inviter":
			if ($(id).val()!=""){
				arrFlg[4] =  VarifyInviter(id);
			}else{
				arrFlg[4] = true;
			}
			keyUpOperate(arrFlg[4], classErr);
			break;
			
		case "name":
			arrFlg[5] =  VarifyName(id);
			keyUpOperate(arrFlg[5], classErr);
			break;
		case "idCard":
			arrFlg[6] =  VarifyIdcard(id);

			keyUpOperate(arrFlg[6], classErr);
			break;
		case "loginPassword":
			arrFlg[7] =  VarifyPassword(id);

			keyUpOperate(arrFlg[7], classErr);
			break;
		default:
			break;
		}
		ButtonChange(arrFlg, idBtn);
	});
}

function ButtonChange(arr, idButton){
	var tmpFlg = true;
//	console.log(idButton);
	for(var i=0;i<arr.length;i++){
		if (false == arr[i]){
			tmpFlg = false;
			break;
		}
	}
	
//	console.log("tmpflg=", tmpFlg);
	if (true==tmpFlg){
		$(idButton).removeClass("btn-disabled");
        $(idButton).removeAttr("disabled");
		$(idButton).css({
            cursor: "pointer"
		});
	}else{
		$(idButton).addClass("btn-disabled");
		$(idButton).attr("disabled", "false");
		
        $(idButton).css({
            cursor: "not-allowed"
		});
	}
}

function passwordEye(className){
	var obj = $("." + className);
	obj.click(function(){
	    if($(this).parent().find("input").attr("type")=="password"){
	        $(this).css({"background":"url('images/password_none-see.png')no-repeat 0","background-size":"100% auto"});
	        $(this).parent().find("input").prop("type","text");
	    }else{
	        $(this).parent().find("input").prop("type","password");
	        $(this).css({"background":"url('images/password_see.png')no-repeat 0","background-size":"100% auto"});
	    }
	});
}


function tips(){
	layer.msg('即将开放', {
		  icon: 4,
		  time: 1000 //2秒关闭（如果不配置，默认是3秒）
		}, function(){
		  //do something
		});  
}

function tips_register(){
	layer.msg('注册成功',{
		icon:1,
		time:1000
	},function(){
		
	});
}

function tipsAll(tipsMsg,iconCode, time,func){
	layer.msg(tipsMsg, {
		icon:iconCode,
		time:time
	},func);
}

function errTips(errCode, msg){
	switch(errCode){
	case StatusCode.OK:
		tipsAll("成功",1,1000);
		break;
	case StatusCode.UNDEFINED_ERROR:
		tipsAll("未定义错误", 5, 2000);
		break;
	case StatusCode.ACCESS_DENIED:
		tipsAll(msg, 5, 2000);
		break;
	case StatusCode.ILLEGAL_PARAMETER_VALUE:
		tipsAll(msg, 5, 2000);
		break;
	case StatusCode.SERVICE_BUSY:
		tipsAll("服务繁忙", 7, 2000);
		break;
	case StatusCode.DATABASE_ERROR:
		tipsAll("服务器内部数据错误", 5, 2000);
		break;
	case StatusCode.SMS_SEND_FAILED:
		tipsAll("(验证码)信息发送失败", 5, 2000);
		break;
	case StatusCode.ACCOUNT_EXISTS:
		tipsAll("用户已存在", 5, 2000);
		break;
	case StatusCode.ACCOUNT_NOT_EXISTS:
		tipsAll("用户不存在", 5, 2000);
		break;
	case StatusCode.ACCOUNT_LOCKED:
		tipsAll("用户已锁定", 5, 2000);
		break;
	case StatusCode.ACCOUNT_FOBIDDEN:
		tipsAll("用户已禁用", 5, 2000);
		break;
	case StatusCode.ACCOUNT_LOGIN_FAIL:
		tipsAll("登录失败，用户名或者密码错误", 2, 2000);
		break;
	case StatusCode.CODE_ERROR:
		tipsAll("验证码错误", 2, 2000);
		break;
	case StatusCode.CODE_EXPIRED:
		tipsAll("验证码失效", 5, 2000);
		break;
	case StatusCode.REFEREE_CODE_ERROR:
		tipsAll("推荐码不存在", 7, 2000);
		break;
	case StatusCode.CAPTCHA_CODE_ERROR:
		tipsAll("图形验证码错误", 7, 2000);
		break;
	case StatusCode.ACCOUNT_NAME_ERROR:
		tipsAll("用户名错误", 2, 2000);
		break;
	case StatusCode.ACCOUNT_PWD_ERROR:
		tipsAll("用户密码错误", 2, 2000);
		break;
	case StatusCode.PHONE_CODE_SEND_LIMIT:
		tipsAll("手机验证码发送已经超过当日最大次数限制", 2, 2000);
		break;
		
	default:	
		break;
	}
}