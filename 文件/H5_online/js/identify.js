var id_box_identify = new Array(
		null,
		null,
		null,
		null,
		null,
		id_identify_name,
		id_identify_idCard);
var classErrTip = new Array(
	    "",
	    "",
	    "",
	    "",
	    "",
	    "class_identify_err_name",
	    "class_identify_err_idCard");

var identifyFormFlg = new Array(true,true,true,true,true,false,false);

var varifiyStat = -1;

CallEvent();
function CallEvent(){
	for (var i = 0; i < id_box_identify.length; i++){
		KeyUpEvent(id_box_identify[i],identifyFormFlg, classErrTip[i],id_identify_submit);
	}
	ButtonChange(identifyFormFlg, id_identify_submit);
}

$("#id_identify_submit").click(function(){
//	console.log("--");
	var name = $("#id_identify_name").val();
	var idcard = $("#id_identify_idCard").val();
	console.log(name,idcard);
	$.ajax({
        url:public+'/UserCenter/certification',
        type:'post',
        data:{
        	idCardNo:idcard,
        	idCardName:name,
        },
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        success:function(res){
        	console.log(res);
            errTips(res.code);
            if(res.code === 200){
            	
            }
        }
    });
});

function Refresh(){
	$.ajax({
        url:public+'/UserCenter/userSec',
        type:'post',
        data:{
        },
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        success:function(res){
            if(res.code === 200 && res.data != null){
            	varifiyStat = res.data.flag;
            }
            setFlag(varifiyStat + 1);
        }
    });
}

(function(){
	Refresh();
}).call(this);

function setFlag(index_flag) {

	var $point_arr, $points, $progress, activate, active, max, val;

	$points = $('.progress-points').first();

	$point_arr = $('[data-point]');

	$progress = $('.progress').first();

	max = $point_arr.length - 1;

	activate = function(index) {
		if (index == 0) {
			$(".sub_or_no").text("未提交");
			$point_arr.removeClass('completed active');
			return $progress.css('width', (index / max * 100) + "%");
		} else if (3 == index) {
			index = index - 1;
			$(".sucess_failed").text("审核失败");
			$point_arr.removeClass('completed active');
			$point_arr.slice(0, index + 1).addClass('completed');
		} else {

			$point_arr.removeClass('completed active');
			$point_arr.slice(0, index + 1).addClass('completed');
			return $progress.css('width', (index / max * 100) + "%");

		}
	}

	activate(index_flag);

}