/*
Navicat MySQL Data Transfer

Source Server         : 192.168.0.139
Source Server Version : 50641
Source Host           : 192.168.0.139:3306
Source Database       : phone_mining

Target Server Type    : MYSQL
Target Server Version : 50641
File Encoding         : 65001

Date: 2018-09-10 17:46:19
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for phone_sys_config
-- ----------------------------
DROP TABLE IF EXISTS `phone_sys_config`;
CREATE TABLE `phone_sys_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(45) NOT NULL COMMENT '参数code',
  `code_name` varchar(45) NOT NULL COMMENT '参数名称',
  `init_value` varchar(45) NOT NULL COMMENT '初始化值',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NOT NULL COMMENT '更新时间',
  `operator_id` varchar(8) NOT NULL COMMENT '操作人',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQUE_CONFIG_CODE` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='系统配置';
