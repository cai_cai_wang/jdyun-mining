/*
Navicat MySQL Data Transfer

Source Server         : 192.168.0.139
Source Server Version : 50641
Source Host           : 192.168.0.139:3306
Source Database       : phone_mining

Target Server Type    : MYSQL
Target Server Version : 50641
File Encoding         : 65001

Date: 2018-09-10 17:45:56
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for phone_referee
-- ----------------------------
DROP TABLE IF EXISTS `phone_referee`;
CREATE TABLE `phone_referee` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(8) NOT NULL COMMENT '用户id',
  `referee_id` varchar(8) DEFAULT NULL COMMENT '推荐人id',
  `referee_parent_id` varchar(8) DEFAULT NULL COMMENT '推荐人的上级推荐人id',
  `context` varchar(5000) DEFAULT NULL COMMENT '用户关系树',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NOT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=208 DEFAULT CHARSET=utf8 COMMENT='推荐人关系表';
