/*
Navicat MySQL Data Transfer

Source Server         : 192.168.0.139
Source Server Version : 50641
Source Host           : 192.168.0.139:3306
Source Database       : phone_mining

Target Server Type    : MYSQL
Target Server Version : 50641
File Encoding         : 65001

Date: 2018-09-10 17:45:27
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for phone_message
-- ----------------------------
DROP TABLE IF EXISTS `phone_message`;
CREATE TABLE `phone_message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(24) DEFAULT NULL COMMENT '标题',
  `message` varchar(255) NOT NULL COMMENT '发布的消息',
  `amount` varchar(45) DEFAULT NULL COMMENT '获取的数额',
  `coin_type` varchar(6) DEFAULT NULL COMMENT '币种类型',
  `message_type` tinyint(4) NOT NULL COMMENT '消息类型：0 系统消息，1 个人消息',
  `publisher` varchar(8) DEFAULT NULL COMMENT '发布人',
  `flag` tinyint(4) NOT NULL COMMENT '消息状态：0 未发布 1 已发布',
  `create_time` datetime NOT NULL COMMENT '创建的时间',
  `update_time` datetime NOT NULL COMMENT '更新的时间',
  PRIMARY KEY (`id`),
  KEY `create_time` (`create_time`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COMMENT='消息中心';
