/*
Navicat MySQL Data Transfer

Source Server         : 192.168.0.139
Source Server Version : 50641
Source Host           : 192.168.0.139:3306
Source Database       : phone_mining

Target Server Type    : MYSQL
Target Server Version : 50641
File Encoding         : 65001

Date: 2018-09-10 17:47:32
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for phone_user_wallet
-- ----------------------------
DROP TABLE IF EXISTS `phone_user_wallet`;
CREATE TABLE `phone_user_wallet` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(8) NOT NULL COMMENT '用户id',
  `coin_type` varchar(6) NOT NULL COMMENT '币种',
  `asset` varchar(45) NOT NULL DEFAULT '0' COMMENT '总资产(余额+锁仓+冻结)',
  `balance` varchar(45) NOT NULL DEFAULT '0' COMMENT '余额(可用资产：转入（除去锁仓）+挖矿+签到)',
  `ming_coin` varchar(45) NOT NULL DEFAULT '0' COMMENT '挖矿所得',
  `task_coin` varchar(45) NOT NULL DEFAULT '0' COMMENT '签到所得',
  `locked_coin` decimal(28,6) NOT NULL DEFAULT '0.000000' COMMENT '锁仓【用户充值的总和】',
  `freeze_coin` varchar(45) NOT NULL DEFAULT '0' COMMENT '已冻结资产',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NOT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=869 DEFAULT CHARSET=utf8 COMMENT='用户钱包';
