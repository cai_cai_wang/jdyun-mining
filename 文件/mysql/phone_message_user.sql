/*
Navicat MySQL Data Transfer

Source Server         : 192.168.0.139
Source Server Version : 50641
Source Host           : 192.168.0.139:3306
Source Database       : phone_mining

Target Server Type    : MYSQL
Target Server Version : 50641
File Encoding         : 65001

Date: 2018-09-10 17:45:34
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for phone_message_user
-- ----------------------------
DROP TABLE IF EXISTS `phone_message_user`;
CREATE TABLE `phone_message_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(8) NOT NULL COMMENT '用户id',
  `message_id` int(11) NOT NULL COMMENT '消息id',
  `flag` tinyint(4) NOT NULL DEFAULT '0' COMMENT '读取状态：0 未读 1 已读 2 已删除',
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COMMENT='消息的实际阅读情况';
