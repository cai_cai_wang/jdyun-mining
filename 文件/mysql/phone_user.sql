/*
Navicat MySQL Data Transfer

Source Server         : 192.168.0.139
Source Server Version : 50641
Source Host           : 192.168.0.139:3306
Source Database       : phone_mining

Target Server Type    : MYSQL
Target Server Version : 50641
File Encoding         : 65001

Date: 2018-09-10 17:47:07
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for phone_user
-- ----------------------------
DROP TABLE IF EXISTS `phone_user`;
CREATE TABLE `phone_user` (
  `id` varchar(8) NOT NULL COMMENT '用户id',
  `phone` varchar(11) DEFAULT NULL COMMENT '手机号码',
  `email` varchar(100) DEFAULT NULL COMMENT '邮箱',
  `password` varchar(64) NOT NULL COMMENT '用户密码',
  `nick_name` varchar(45) NOT NULL COMMENT '用户昵称',
  `head_pic` varchar(100) DEFAULT NULL COMMENT '用户图像',
  `referee` varchar(16) DEFAULT NULL COMMENT '推荐人',
  `code` varchar(4) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '当前用户的推荐码',
  `flag` tinyint(4) NOT NULL DEFAULT '0' COMMENT '用户状态',
  `admin` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否是管理员：0 否 1 是',
  `level` int(11) NOT NULL DEFAULT '0' COMMENT '用户等级(默认的等级为0)',
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户信息';
