/*
Navicat MySQL Data Transfer

Source Server         : 192.168.0.139
Source Server Version : 50641
Source Host           : 192.168.0.139:3306
Source Database       : phone_mining

Target Server Type    : MYSQL
Target Server Version : 50641
File Encoding         : 65001

Date: 2018-09-10 17:46:09
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for phone_reward_points
-- ----------------------------
DROP TABLE IF EXISTS `phone_reward_points`;
CREATE TABLE `phone_reward_points` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(8) NOT NULL COMMENT '用户id',
  `coin_amount` varchar(45) NOT NULL DEFAULT '0' COMMENT '历史充值获取总积分',
  `coin_type` varchar(6) NOT NULL COMMENT '币种',
  `times` int(11) NOT NULL DEFAULT '0' COMMENT '已经参与过的抽奖次数',
  `leaves` int(11) NOT NULL DEFAULT '0' COMMENT '剩余抽奖次数',
  `leaves_amount` varchar(45) DEFAULT '0' COMMENT '剩余积分',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NOT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=180 DEFAULT CHARSET=utf8 COMMENT='用户积分汇总表';
