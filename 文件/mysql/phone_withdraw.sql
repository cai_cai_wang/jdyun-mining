/*
Navicat MySQL Data Transfer

Source Server         : 192.168.0.139
Source Server Version : 50641
Source Host           : 192.168.0.139:3306
Source Database       : phone_mining

Target Server Type    : MYSQL
Target Server Version : 50641
File Encoding         : 65001

Date: 2018-09-10 17:47:39
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for phone_withdraw
-- ----------------------------
DROP TABLE IF EXISTS `phone_withdraw`;
CREATE TABLE `phone_withdraw` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(8) NOT NULL COMMENT '用户id',
  `coin_amount` varchar(45) NOT NULL COMMENT '数量',
  `coin_type` varchar(6) NOT NULL COMMENT '币种',
  `flag` tinyint(4) NOT NULL COMMENT '状态：0 处理中，1 已完成',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
