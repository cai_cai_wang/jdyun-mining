/*
Navicat MySQL Data Transfer

Source Server         : 192.168.0.139
Source Server Version : 50641
Source Host           : 192.168.0.139:3306
Source Database       : phone_mining

Target Server Type    : MYSQL
Target Server Version : 50641
File Encoding         : 65001

Date: 2018-09-10 17:44:56
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for phone_lock_coin_release_log
-- ----------------------------
DROP TABLE IF EXISTS `phone_lock_coin_release_log`;
CREATE TABLE `phone_lock_coin_release_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(8) NOT NULL COMMENT '用户id',
  `lock_coin_id` int(11) NOT NULL COMMENT '锁仓id',
  `coin_type` varchar(6) NOT NULL COMMENT '币种',
  `locked_coin` decimal(28,8) NOT NULL COMMENT '用户当前的锁仓额（释放前）',
  `amount` decimal(28,8) NOT NULL COMMENT '释放数量',
  `release_type` tinyint(4) NOT NULL COMMENT '释放类型：0,好友锁仓释放,1，每日定量释放，2，每日获得好友释放',
  `create_time` datetime NOT NULL COMMENT '创建日期',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=222589 DEFAULT CHARSET=utf8;
