/*
Navicat MySQL Data Transfer

Source Server         : 192.168.0.139
Source Server Version : 50641
Source Host           : 192.168.0.139:3306
Source Database       : phone_mining

Target Server Type    : MYSQL
Target Server Version : 50641
File Encoding         : 65001

Date: 2018-09-10 17:45:49
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for phone_pool_log
-- ----------------------------
DROP TABLE IF EXISTS `phone_pool_log`;
CREATE TABLE `phone_pool_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pool_balance` varchar(45) NOT NULL COMMENT '矿池实时余额',
  `coin_amount` varchar(45) NOT NULL COMMENT '变动额度（+表示增发，收入，-表示发出）',
  `coin_type` varchar(6) NOT NULL COMMENT '币种类型',
  `user_id` varchar(8) NOT NULL COMMENT '用户id',
  `memo` varchar(45) DEFAULT NULL COMMENT '备注',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='矿池资金变化记录';
