/*
Navicat MySQL Data Transfer

Source Server         : 192.168.0.139
Source Server Version : 50641
Source Host           : 192.168.0.139:3306
Source Database       : phone_mining

Target Server Type    : MYSQL
Target Server Version : 50641
File Encoding         : 65001

Date: 2018-09-10 17:46:02
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for phone_referee_income
-- ----------------------------
DROP TABLE IF EXISTS `phone_referee_income`;
CREATE TABLE `phone_referee_income` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(8) NOT NULL COMMENT '用户id',
  `to_balance` decimal(28,8) NOT NULL DEFAULT '0.00000000' COMMENT '转入余额部分',
  `to_locked_coin` decimal(28,8) NOT NULL DEFAULT '0.00000000' COMMENT '转入锁仓部分',
  `date` varchar(10) NOT NULL COMMENT '统计日期',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NOT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户的一二级推荐人收益';
