/*
Navicat MySQL Data Transfer

Source Server         : 192.168.0.139
Source Server Version : 50641
Source Host           : 192.168.0.139:3306
Source Database       : phone_mining

Target Server Type    : MYSQL
Target Server Version : 50641
File Encoding         : 65001

Date: 2018-09-10 17:46:56
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for phone_transfer_records
-- ----------------------------
DROP TABLE IF EXISTS `phone_transfer_records`;
CREATE TABLE `phone_transfer_records` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` varchar(64) NOT NULL COMMENT '交易id',
  `transfer_type` tinyint(4) NOT NULL COMMENT '转账类型：0 转账，1，充值，2，挖矿所得，3，签到奖励，4，抽奖奖励，5，推荐奖励',
  `amount` varchar(45) NOT NULL COMMENT '交易额度',
  `fee` varchar(45) NOT NULL COMMENT '转账费',
  `coin_type` varchar(6) NOT NULL COMMENT '币种',
  `from_user` varchar(8) NOT NULL COMMENT '发起人',
  `to_user` varchar(8) NOT NULL COMMENT '收账人',
  `memo` varchar(100) DEFAULT NULL COMMENT '备注',
  `create_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='交易记录';
