/*
Navicat MySQL Data Transfer

Source Server         : 192.168.0.139
Source Server Version : 50641
Source Host           : 192.168.0.139:3306
Source Database       : phone_mining

Target Server Type    : MYSQL
Target Server Version : 50641
File Encoding         : 65001

Date: 2018-09-10 17:47:25
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for phone_user_sec
-- ----------------------------
DROP TABLE IF EXISTS `phone_user_sec`;
CREATE TABLE `phone_user_sec` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(8) NOT NULL COMMENT '用户id',
  `sec_password` varchar(64) DEFAULT NULL COMMENT '用户支付密码',
  `id_card_no` varchar(18) DEFAULT NULL COMMENT '身份证号',
  `id_card_name` varchar(45) DEFAULT NULL COMMENT '真实姓名',
  `id_card_up` varchar(100) DEFAULT NULL COMMENT '身份证正面图片路径',
  `id_card_down` varchar(100) DEFAULT NULL COMMENT '身份证反面图片路径',
  `flag` tinyint(4) NOT NULL DEFAULT '0' COMMENT '实名认证状态：0 审核中 1 审核通过，2审核不通过',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NOT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='用户安全信息';
