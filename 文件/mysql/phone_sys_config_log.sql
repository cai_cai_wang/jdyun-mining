/*
Navicat MySQL Data Transfer

Source Server         : 192.168.0.139
Source Server Version : 50641
Source Host           : 192.168.0.139:3306
Source Database       : phone_mining

Target Server Type    : MYSQL
Target Server Version : 50641
File Encoding         : 65001

Date: 2018-09-10 17:46:41
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for phone_sys_config_log
-- ----------------------------
DROP TABLE IF EXISTS `phone_sys_config_log`;
CREATE TABLE `phone_sys_config_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(45) NOT NULL COMMENT '配置日志',
  `old_value` varchar(45) DEFAULT NULL COMMENT '原值',
  `new_value` varchar(45) NOT NULL COMMENT '新值',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `operator` varchar(8) NOT NULL COMMENT '操作人',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=93 DEFAULT CHARSET=utf8 COMMENT='系统配置修改日志';
