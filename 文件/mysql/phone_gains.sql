/*
Navicat MySQL Data Transfer

Source Server         : 192.168.0.139
Source Server Version : 50641
Source Host           : 192.168.0.139:3306
Source Database       : phone_mining

Target Server Type    : MYSQL
Target Server Version : 50641
File Encoding         : 65001

Date: 2018-09-10 17:44:25
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for phone_gains
-- ----------------------------
DROP TABLE IF EXISTS `phone_gains`;
CREATE TABLE `phone_gains` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(8) NOT NULL COMMENT '用户id',
  `date` varchar(10) NOT NULL COMMENT '日期',
  `vested` varchar(45) NOT NULL DEFAULT '0' COMMENT '已经收获的矿',
  `rate` varchar(10) DEFAULT NULL COMMENT '收获时实际比例',
  `times` int(11) NOT NULL DEFAULT '0' COMMENT '收取次数',
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=80 DEFAULT CHARSET=utf8 COMMENT='收获挖矿';
