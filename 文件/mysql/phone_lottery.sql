/*
Navicat MySQL Data Transfer

Source Server         : 192.168.0.139
Source Server Version : 50641
Source Host           : 192.168.0.139:3306
Source Database       : phone_mining

Target Server Type    : MYSQL
Target Server Version : 50641
File Encoding         : 65001

Date: 2018-09-10 17:45:08
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for phone_lottery
-- ----------------------------
DROP TABLE IF EXISTS `phone_lottery`;
CREATE TABLE `phone_lottery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(8) NOT NULL COMMENT '用户id',
  `recharge_id` int(11) NOT NULL COMMENT '充值记录id',
  `rate` varchar(10) DEFAULT NULL COMMENT '奖励比例',
  `coin_amount` varchar(45) DEFAULT '0' COMMENT '奖励额度',
  `coin_type` varchar(6) DEFAULT NULL COMMENT '币种类型',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户抽奖';
