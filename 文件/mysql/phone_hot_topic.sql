/*
Navicat MySQL Data Transfer

Source Server         : 192.168.0.139
Source Server Version : 50641
Source Host           : 192.168.0.139:3306
Source Database       : phone_mining

Target Server Type    : MYSQL
Target Server Version : 50641
File Encoding         : 65001

Date: 2018-09-10 17:44:35
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for phone_hot_topic
-- ----------------------------
DROP TABLE IF EXISTS `phone_hot_topic`;
CREATE TABLE `phone_hot_topic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL COMMENT '动态标题',
  `content` text NOT NULL COMMENT '动态内容',
  `date` varchar(10) NOT NULL COMMENT '日期',
  `show_order` int(11) NOT NULL DEFAULT '0' COMMENT '展示顺序，按数字倒序',
  `flag` tinyint(4) NOT NULL DEFAULT '0' COMMENT '状态：0 不是热点 1 热点 2 已删除',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
