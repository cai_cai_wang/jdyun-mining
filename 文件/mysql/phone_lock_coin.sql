/*
Navicat MySQL Data Transfer

Source Server         : 192.168.0.139
Source Server Version : 50641
Source Host           : 192.168.0.139:3306
Source Database       : phone_mining

Target Server Type    : MYSQL
Target Server Version : 50641
File Encoding         : 65001

Date: 2018-09-10 17:44:47
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for phone_lock_coin
-- ----------------------------
DROP TABLE IF EXISTS `phone_lock_coin`;
CREATE TABLE `phone_lock_coin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(8) NOT NULL COMMENT '用户id',
  `init_lock_coin_amount` decimal(28,8) NOT NULL DEFAULT '0.00000000' COMMENT '初始挖矿数量',
  `lock_coin_amount` decimal(28,8) NOT NULL DEFAULT '0.00000000' COMMENT '锁仓（算力）',
  `income` decimal(28,8) NOT NULL DEFAULT '0.00000000' COMMENT '锁仓累计收益',
  `coin_type` varchar(16) NOT NULL COMMENT '币种',
  `sequence_id` varchar(45) NOT NULL COMMENT '流水号',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NOT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=548025 DEFAULT CHARSET=utf8 COMMENT='锁仓后30天释放Tex';
