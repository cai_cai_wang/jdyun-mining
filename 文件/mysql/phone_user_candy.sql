/*
Navicat MySQL Data Transfer

Source Server         : 192.168.0.139
Source Server Version : 50641
Source Host           : 192.168.0.139:3306
Source Database       : phone_mining

Target Server Type    : MYSQL
Target Server Version : 50641
File Encoding         : 65001

Date: 2018-09-10 17:47:18
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for phone_user_candy
-- ----------------------------
DROP TABLE IF EXISTS `phone_user_candy`;
CREATE TABLE `phone_user_candy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(8) NOT NULL COMMENT '用户id',
  `reward_all` decimal(28,8) NOT NULL COMMENT '获取的所有的糖果',
  `reward_invite_all` decimal(28,8) NOT NULL COMMENT '获取的所有的推荐奖励糖果',
  `free_all` decimal(28,8) NOT NULL COMMENT '已释放的所有糖果',
  `free_invite_all` decimal(28,8) NOT NULL COMMENT '已释放的所有推荐奖励的糖果',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
