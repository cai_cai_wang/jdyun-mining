package cn.com.jdyun.util;

import cn.com.jdyun.exception.BdexGatewayException;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisShardInfo;
import redis.clients.jedis.ShardedJedis;
import redis.clients.jedis.ShardedJedisPool;
import redis.clients.jedis.exceptions.JedisConnectionException;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
public class RedisUtils {

    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(RedisUtil.class);

    @Autowired
    private JedisPool jedisPool;

    //    public Jedis getResource() {
//        return jedisPool.getResource();
//    }
    public ShardedJedis getResource() {
        //配置对象
        GenericObjectPoolConfig config = new GenericObjectPoolConfig();
        config.setMaxTotal(50);    //最大链接数

        //节点链接信息
        List<JedisShardInfo> shardsList = new ArrayList<JedisShardInfo>();
        JedisShardInfo info1 = new JedisShardInfo("192.168.0.162", 6379);
        shardsList.add(info1);
        JedisShardInfo info2 = new JedisShardInfo("192.168.0.162", 6380);
        shardsList.add(info2);
        JedisShardInfo info3 = new JedisShardInfo("192.168.0.162", 6381);
        shardsList.add(info3);

        //创建jedis池
        ShardedJedisPool pool = new ShardedJedisPool(config, shardsList);
        return pool.getResource();
    }

    public void returnResource(ShardedJedis jedis) {
        if (jedis != null) {
            jedis.close();
        }
    }

    public void set(String key, String value, int timeout) throws BdexGatewayException {
        ShardedJedis jedis = null;
        try {
            jedis = getResource();
            jedis.setex(key, timeout, value);
//            logger.info("Redis set success - " + key + ", value:" + value);
        } catch (JedisConnectionException e) {
            logger.error("Redis set error: " + e.getMessage() + " - " + key + ", value:" + value);
            throw new BdexGatewayException("redis连接异常");
        } catch (Exception e) {
            logger.error("Redis set error: " + e.getMessage() + " - " + key + ", value:" + value);
            throw e;
        } finally {
            returnResource(jedis);
        }
    }

    public void set(String key, String value) throws BdexGatewayException {
        ShardedJedis jedis = null;
        try {
            jedis = getResource();
            jedis.setex(key, Integer.valueOf(7200), value); // 默认保存3个小时
//            logger.info("Redis set success - " + key + ", value:" + value);
        } catch (JedisConnectionException e) {
            logger.error("Redis set error: " + e.getMessage() + " - " + key + ", value:" + value);
            throw new BdexGatewayException("redis连接异常");
        } catch (Exception e) {
            logger.error("Redis set error: " + e.getMessage() + " - " + key + ", value:" + value);
            throw e;
        } finally {
            returnResource(jedis);
        }
    }

    public void hset(String key1, String key2, String value) throws BdexGatewayException {
        ShardedJedis jedis = null;
        try {
            jedis = getResource();
            jedis.hset(key1, key2, value);
            jedis.expire(key1, Integer.valueOf(7200));
        } catch (JedisConnectionException e) {
            logger.error("Redis hset error: " + e.getMessage() + " - " + key1 + " - " + key2 + ", value:" + value);
            throw new BdexGatewayException("redis连接异常");
        } catch (Exception e) {
            logger.error("Redis hset error: " + e.getMessage() + " - " + key1 + " - " + key2 + ", value:" + value);
            throw e;
        } finally {
            returnResource(jedis);
        }
    }

    public void hset(String key1, String key2, String value, int timeout) throws BdexGatewayException {
        ShardedJedis jedis = null;
        try {
            jedis = getResource();
            jedis.hset(key1, key2, value);
            jedis.expire(key1, timeout);
        } catch (JedisConnectionException e) {
            logger.error("Redis hset error: " + e.getMessage() + " - " + key1 + " - " + key2 + ", value:" + value);
            throw new BdexGatewayException("redis连接异常");
        } catch (Exception e) {
            logger.error("Redis hset error: " + e.getMessage() + " - " + key1 + " - " + key2 + ", value:" + value);
            throw e;
        } finally {
            returnResource(jedis);
        }
    }

    public void hmset(String key1, Map value, int timeout) throws BdexGatewayException {
        ShardedJedis jedis = null;
        try {
            jedis = getResource();
            jedis.hmset(key1, value);
            jedis.expire(key1, timeout);
        } catch (JedisConnectionException e) {
            logger.error("Redis hset error: " + e.getMessage() + " - " + key1 + ", value:" + value);
            throw new BdexGatewayException("redis连接异常");
        } catch (Exception e) {
            logger.error("Redis hset error: " + e.getMessage() + " - " + key1 + ", value:" + value);
            throw e;
        } finally {
            returnResource(jedis);
        }
    }

    public boolean hdel(String key1, String key2) throws BdexGatewayException {
        ShardedJedis jedis = null;
        try {
            jedis = getResource();
            long reLong = jedis.hdel(key1, key2);
            return reLong > 0 ? true : false;
        } catch (JedisConnectionException e) {
            logger.error("Redis hdel error: " + e.getMessage() + " - " + key1 + " - " + key2);
            throw new BdexGatewayException("redis连接异常");
        } catch (Exception e) {
            logger.error("Redis hdel error: " + e.getMessage() + " - " + key1 + " - " + key2);
            throw e;
        } finally {
            returnResource(jedis);
        }
    }

    public boolean del(String key1) throws BdexGatewayException {
        ShardedJedis jedis = null;
        try {
            jedis = getResource();
            long reLong = jedis.del(key1);
            return reLong > 0 ? true : false;
        } catch (JedisConnectionException e) {
            logger.error("Redis del error: " + e.getMessage() + " - " + key1);
            throw new BdexGatewayException("redis连接异常");
        } catch (Exception e) {
            logger.error("Redis del error: " + e.getMessage() + " - " + key1);
            throw e;
        } finally {
            returnResource(jedis);
        }
    }

    public String hget(String key1, String key2) throws BdexGatewayException {
        String result = null;
        ShardedJedis jedis = null;
        try {
            jedis = getResource();
            result = jedis.hget(key1, key2);
        } catch (JedisConnectionException e) {
            logger.error("Redis hget error: " + e.getMessage() + " - " + key1 + " - " + key2);
            throw new BdexGatewayException("redis连接异常");
        } catch (Exception e) {
            logger.error("Redis hget error: " + e.getMessage() + " - " + key1 + " - " + key2);
            throw e;
        } finally {
            returnResource(jedis);
        }
        return result;
    }

    /**
     * 检查key值是否存在
     *
     * @param key
     * @return
     * @throws BdexGatewayException
     */
    public boolean exists(String key) throws BdexGatewayException {
        ShardedJedis jedis = null;
        try {
            jedis = getResource();
            return jedis.exists(key);
        } catch (JedisConnectionException e) {
            logger.error("Redis exists error: " + e.getMessage() + " - " + key);
            throw new BdexGatewayException("redis连接异常");
        } catch (Exception e) {
            logger.error("Redis exists error: " + e.getMessage() + " - " + key);
            throw e;
        } finally {
            returnResource(jedis);
        }
    }

    public Map<String, String> hgetAll(String key) throws BdexGatewayException {
        Map<String, String> result = null;
        ShardedJedis jedis = null;
        try {
            jedis = getResource();
            result = jedis.hgetAll(key);
        } catch (JedisConnectionException e) {
            logger.error("Redis hgetAll error: " + e.getMessage() + " - " + key);
            throw new BdexGatewayException("redis连接异常");
        } catch (Exception e) {
            logger.error("Redis hgetAll error: " + e.getMessage() + " - " + key);
            throw e;
        } finally {
            returnResource(jedis);
        }
        return result;
    }


    public String get(String key) throws BdexGatewayException {
        String result = null;
        ShardedJedis jedis = null;
        try {
            jedis = getResource();
            result = jedis.get(key);
        } catch (JedisConnectionException e) {
            logger.error("Redis get error: " + e.getMessage() + " - " + key + ", value:" + result);
            throw new BdexGatewayException("redis连接异常");
        } catch (Exception e) {
            logger.error("Redis get error: " + e.getMessage() + " - " + key + ", value:" + result);
//            throw e;
        } finally {
            returnResource(jedis);
        }
        return result;
    }

}