package cn.com.jdyun.util.reponse;


import cn.com.jdyun.dto.ANS_ALERT;
import cn.com.jdyun.exception.BdexGatewayException;
import cn.com.jdyun.exception.SuccessAlertException;
import cn.com.jdyun.exception.WarningException;

public class Result {

    private int code;
    private String msg;
    private Object data;
    private String alert;
    private String redirectURL;
    private boolean status;

    public Result() {
        this.alert = ANS_ALERT.NORMAL;
        this.status = true;
        this.code = 200;
        this.msg = "success";
    }

    public void setTimeout() {
        this.alert = ANS_ALERT.TIMEOUT;
        this.status = false;
        this.code = 401;
        this.msg = "操作超时，请重新登录！";
    }

    public static Result success(Object data) {
        Result result = new Result();
        result.setCode(Status.OK.getCode());
        result.setMsg("success");
        result.setData(data);
        return result;
    }

    public void setError(String msg) {
        this.alert = ANS_ALERT.ERROR;
        this.status = false;
        this.code = 500;
        this.msg = msg;
    }

    public void setWarning(String msg) {
        this.alert = ANS_ALERT.WARNING;
        this.status = false;
        this.code = 200;
        this.msg = msg;
    }

    public void setWarning(WarningException e) {
        this.alert = ANS_ALERT.WARNING;
        this.status = false;
        this.code = 200;
        this.msg = e.getMessage();
    }

    public void setTraderWarning(WarningException e) {
        this.alert = ANS_ALERT.WARNING;
        this.status = false;
        this.code = 500;
        this.msg = e.getMessage();
    }

    public void setSuccess(SuccessAlertException msg) {
        this.alert = ANS_ALERT.SUCCESS;
        this.status = true;
        this.msg = msg.getMessage();
        this.code = 200;
    }

    public void setError(BdexGatewayException msg) {
        this.alert = ANS_ALERT.ERROR;
        this.status = false;
        if (msg.getCode() == null) {
            this.code = 500;
        } else {
            this.code = msg.getCode();
        }
        this.msg = msg.getMessage();
    }

    public void setError(Exception msg) {
        this.alert = ANS_ALERT.ERROR;
        this.status = false;
        this.code = 500;
        this.msg = msg.getMessage();
    }

    public void setErrorNull(Exception msg) {
        this.alert = ANS_ALERT.ERROR;
        this.status = false;
        this.code = 501;
        this.msg = msg.getMessage();
    }


    public static Result fail(String msg) {
        Result result = new Result();
        result.setCode(Status.UNDEFINED_ERROR.getCode());
        result.setMsg(msg);
        result.setData(null);
        return result;
    }

    public void setAlert(String alert) {
        this.alert = alert;
    }

    public static Result create(Status status, String msg, Object data) {
        Result result = new Result();
        result.setCode(status.getCode());
        result.setMsg(msg);
        result.setData(data);
        return result;
    }

    public static Result create(Status status, Object data) {
        Result result = new Result();
        result.setCode(status.getCode());
        result.setMsg(status.toString());
        result.setData(data);
        return result;
    }

    //setter and getter
    public void setCode(int code) {
        this.code = code;
    }

    public int getCode() {
        return this.code;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getMsg() {
        return this.msg;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public Object getData() {
        return this.data;
    }

    public String getAlert() {
        return alert;
    }

    public String getRedirectURL() {
        return redirectURL;
    }

    public void setRedirectURL(String redirectURL) {
        this.redirectURL = redirectURL;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Result{" +
                "code=" + code +
                ", msg='" + msg + '\'' +
                ", data=" + data +
                ", alert='" + alert + '\'' +
                ", redirectURL='" + redirectURL + '\'' +
                ", status=" + status +
                '}';
    }
}
