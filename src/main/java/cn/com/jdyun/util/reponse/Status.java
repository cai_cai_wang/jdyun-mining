/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cn.com.jdyun.util.reponse;

import java.util.ArrayList;


public enum Status {

    //Common Definition

    OK,
    UNDEFINED_ERROR,
    ACCESS_DENIED,
    SERVICE_BUSY,
    FREQUENT_REQUEST,
    ILLEGAL_PARAMETER_VALUE,
    DATABASE_ERROR,

    SMS_SEND_FAILED,

    //AccountService Definition
    ACCOUNT_EXISTS,
    ACCOUNT_NOT_EXISTS,
    ACCOUNT_LOCKED,
    ACCOUNT_FOBIDDEN,
    ACCOUNT_LOGIN_FAIL,
    CODE_ERROR,
    CODE_EXPIRED,
    REFEREE_CODE_ERROR,
    CAPTCHA_CODE_ERROR,
    ACCOUNT_NAME_ERROR,
    ACCOUNT_PWD_ERROR,
    PHONE_CODE_SEND_LIMIT,
    ACCOUNT_SEC_PWD_EMPTY,
    ACCOUNT_SEC_PWD_ERROR,
    ACCOUNT_SEC_ID_ERROR,
    ACCOUNT_SEC_ID_APPLY,
    ACCOUNT_SEC_ID_REFUSED,
    ACCOUNT_SEC_ID_INVALID,
    ACCOUNT_SEC_ID_APPLY_REPEAT;


    public int getCode() {
        return this.ordinal() + 200;
    }


    @Override
    public String toString() {

        return "$ACCOUNT_Mining-SERVICE > " + super.toString();
    }

    public Object[] status() {
        ArrayList<String> statList = new ArrayList<>();
        Status[] statusList = Status.values();
        for (Status s : statusList) {
            statList.add(s.getCode() + " -- " + s.toString());
        }
        return statList.toArray();
    }

}
