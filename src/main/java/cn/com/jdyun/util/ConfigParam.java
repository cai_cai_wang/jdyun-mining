package cn.com.jdyun.util;

public enum ConfigParam {
    //普通节点的奖励
    COMMON_NODE_REWARD("普通节点的奖励"),
    COMMON_NODE_REWARD_TEX("普通节点Tex的奖励"),
    //超级节点奖励
    SUPER_NODE_REWARD("超级节点奖励"),
    SUPER_NODE_REWARD_TEX("超级节点TEX奖励"),
    //初始化时矿池总量
    MINER_POOL_AMOUNT_INIT("矿池总量初始值"),
    MINER_POOL_AMOUNT_INIT_TEX("矿池总量TEX初始值"),
    //矿池总量
    MINER_POOL_AMOUNT("矿池总量"),
    MINER_POOL_AMOUNT_TEX("矿池TEX总量");

    private String codeName;
    private ConfigParam(String codeName) {
        this.codeName = codeName;
    }
    public String getCodeName() {
        return codeName;
    }
    public String toString() {
        return this.getClass().getSimpleName() + "." + this.name();
    }
}
