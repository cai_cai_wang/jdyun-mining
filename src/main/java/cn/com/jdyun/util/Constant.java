package cn.com.jdyun.util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Random;

/**
 * @author yzping
 * @date Created in 上午 10:35 2018/8/21 0021
 */
public interface Constant {

    public static final String REGIST_CODE = "REGIST";//注册发送短信
    public static final String FINDPWD_CODE = "FINDPWD";//找回密码发送短信
    public static final String FINDTRADEPWD_CODE = "FINDTRADEPWD";//找回交易密码发送短信


    public static final String CAPTCHA = "CAPTCHA"; // 图片验证码
    public static final String PHONE_NUMBER = "PHONE_NUMBER"; //手机号码
    public static final String EMAIL = "EMAIL"; //邮箱
    public static final String PHONE_NUMBER_CODE = "PHONE_NUMBER_CODE"; // 手机验证码
    public static final String EMAIL_CODE = "EMAIL_CODE"; // 邮箱验证码
    public static final String CODE_SEND_TIME = "CODE_SEND_TIME"; //验证码发送时间
    public static final long CODE_HOLD_TIME = 10 * 60 * 1000; //验证码有效期10分钟

    public static final String ACCOUNT_Mining = "ACCOUNT_Mining"; // 用户登录信息

    public static final String[] COINRRAY = {"ETH", "SSSP","TEX", "CANDY"}; //定义的币种列表

    public static final String COINTYPE_SSSP = "SSSP"; //系统默认币种
    public static final String COINTYPE_TEX = "TEX"; //系统默认币种

    public static final String CANDY = "CANDY"; //糖果
    public static final String REGEST_CANDY = "REGEST_CANDY"; //注册送糖果
    public static final String INVITE_CANDY = "INVITE_CANDY"; //邀请注册送糖果
    public static final String DAY_FREE_CANDY = "DAY_FREE_CANDY"; //每天释放糖果

    public static final String serviceCharge = "serviceCharge";//内部转账手续费
    public static final String serviceTEXCharge = "serviceTEXCharge";//内部转账手续费

    public static final String releaseCoinPercentage = "releaseCoinPercentage";//每日释放5‰
    public static final String releaseTexCoinPercentage = "releaseTexCoinPercentage";//每日释放5‰
    public static final String releaseReferee1AvailableBalance = "releaseReferee1AvailableBalance";//邀请好友参与挖矿，自己提前释放10%
    public static final String releaseReferee1TexAvailableBalance = "releaseReferee1TexAvailableBalance";//邀请好友参与挖矿，自己提前释放10%
    public static final String releaseReferee2AvailableBalance = "releaseReferee2AvailableBalance";//邀请好友参与挖矿，自己提前释放10%
    public static final String releaseReferee2TexAvailableBalance = "releaseReferee2TexAvailableBalance";//邀请好友参与挖矿，自己提前释放10%
    public static final String tinyMiningLowestLockCoinNum = "tinyMiningLowestLockCoinNum";//小矿机最少存入的SSSP
    public static final String tinyMiningLowestLockCoinTexNum = "tinyMiningLowestLockCoinTexNum";//小矿机最少存入的Tex
    public static final String smallMiner = "smallMiner";//小矿机最少存入的SSSP
    public static final String texSmallMiner = "texSmallMiner";//小矿机最少存入的Tex
    public static final String largeMiningMachine = "largeMiningMachine";//大矿机最少存入的sssp
    public static final String texLargeMiningMachine = "texLargeMiningMachine";//大矿机最少存入的Tex

    public static final String tinyMiningRewardPercent = "tinyMiningRewardPercent";//小矿机	赠送存币总量的5%
    public static final String texTinyMiningRewardPercent = "texTinyMiningRewardPercent";//小矿机	赠送存币总量的5%

    public static final String strongMiningRewardPercent = "strongMiningRewardPercent";//大矿机	赠送存币总量的5%
    public static final String texStrongMiningRewardPercent = "texStrongMiningRewardPercent";//大矿机	赠送存币总量的5%


    public static final String releaseRefereeAvailableBalance = "releaseRefereeAvailableBalance";//推荐人节点加速释放10%。
    public static final String texReleaseRefereeAvailableBalance = "texReleaseRefereeAvailableBalance";//推荐人节点加速释放10%。

    public static final String referee1RewardPercent = "referee1RewardPercent";//邀请节点1奖励参与挖矿算力10%。
    public static final String texReferee1RewardPercent = "texReferee1RewardPercent";//邀请节点1奖励参与挖矿算力10%。

    public static final String referee2RewardPercent = "referee2RewardPercent";//邀请节点2奖励参与挖矿算力5%。
    public static final String texReferee2RewardPercent = "texReferee2RewardPercent";//邀请节点2奖励参与挖矿算力5%。

    public static final String communityRewardPercent = "communityRewardPercent";//合伙人社区挖矿奖励的百分比10%。
    public static final String texCommunityRewardPercent = "texCommunityRewardPercent";//合伙人社区挖矿奖励的百分比10%。
    public static final String communityPartnerRewardPercent = "communityPartnerRewardPercent";//合伙人社区中奖励其下合伙人的30%。
    public static final String texCommunityPartnerRewardPercent = "texCommunityPartnerRewardPercent";//合伙人社区中奖励其下合伙人的30%。


    public static final String LOTTERY_LIMIT = "LOTTERY_LIMIT";//充值达到5000,获得一次抽奖机会
    public static final String TEX_LOTTERY_LIMIT = "TEX_LOTTERY_LIMIT";//充值达到5000,获得一次抽奖机会
    public static final String[] LOTTERY_CONFIG = new String[]{"1:50", "2:15", "3:7.5", "4:5", "5:4.5", "6:4", "7:3.5", "8:3", "9:2.5", "10:2", "15:1.5", "20:1", "30:0.5"};//获得比例:中奖概率

    public static final String STRONG_NUM_LIMIT = "STRONG_NUM_LIMIT"; //成为合伙人的条件之一：有3个强挖矿推荐
    public static final String TEX_STRONG_NUM_LIMIT = "TEX_STRONG_NUM_LIMIT"; //成为合伙人的条件之一：有3个强挖矿推荐
    public static final String SUPERNODE_LIMIT = "SUPERNODE_LIMIT"; //成为超级超级节点的下限（一级二级推荐人锁仓总额达到该下限即可）
    public static final String TEX_SUPERNODE_LIMIT = "TEX_SUPERNODE_LIMIT"; //成为超级超级节点的下限（一级二级推荐人锁仓总额达到该下限即可）

    /**
     * 获取微挖矿的随机奖励比例
     * 0.001~0.003之间
     *
     * @return
     */
    public static BigDecimal gainsMini() {
        Random random = new Random();
        double rate = 1 + random.nextInt(2);
        rate += random.nextDouble();
        rate = rate / 1000;
        return new BigDecimal(rate).setScale(5, RoundingMode.HALF_UP);
//        return new BigDecimal("0");
    }

    /**
     * 获取强挖矿的随机奖励比例
     * 0.003~0.005之间
     *
     * @return
     */
    public static BigDecimal gainsStrong() {
        Random random = new Random();
        double rate = 3 + random.nextInt(2);
        rate += random.nextDouble();
        rate = rate / 1000;
        return new BigDecimal(rate).setScale(5, RoundingMode.HALF_UP);
//        return new BigDecimal("0");
    }

    /**
     * 生成四位推荐码
     * 由于四位字符序列可随机的范围有限，所以不能采取完全随机的策略
     * 这里生成四组随机顺序数组，让生成的推荐码看起来是随机的。
     *
     * @return
     */
//    public static String generateReffereeCode(String id) {
//        String[][] chars = new String[][]{
//                {"g", "9", "x", "H", "X", "K", "l", "a", "f", "G", "Q", "Y", "F", "M", "w", "r", "k", "d", "y", "1", "e", "Z", "L", "2", "t", "S", "p", "J", "4", "o", "8", "6", "3", "U", "7", "0", "R", "b", "n", "i", "V", "u", "s", "P", "B", "C", "D", "W", "v", "m", "T", "O", "A", "N", "5", "c", "E", "j", "h", "q", "I", "z"},
//                {"A", "h", "c", "X", "e", "C", "m", "O", "1", "u", "k", "T", "l", "0", "2", "G", "J", "j", "4", "6", "v", "a", "d", "g", "S", "x", "M", "D", "3", "w", "H", "8", "F", "5", "R", "I", "n", "i", "7", "Q", "E", "o", "U", "Z", "9", "K", "b", "z", "W", "Y", "q", "B", "p", "r", "V", "P", "y", "t", "s", "N", "L", "f"},
//                {"m", "1", "2", "r", "U", "c", "M", "A", "S", "V", "I", "H", "E", "D", "t", "s", "a", "7", "q", "d", "X", "Z", "k", "6", "y", "W", "B", "Y", "N", "K", "h", "J", "z", "3", "G", "u", "Q", "F", "x", "p", "n", "w", "P", "L", "v", "R", "j", "8", "C", "l", "T", "o", "O", "0", "i", "g", "9", "5", "b", "4", "f", "e"},
//                {"r", "x", "3", "2", "k", "9", "H", "T", "X", "Z", "D", "M", "E", "N", "A", "i", "u", "I", "O", "S", "o", "1", "f", "8", "7", "j", "y", "J", "F", "z", "P", "V", "s", "d", "v", "R", "4", "K", "0", "Y", "q", "a", "g", "e", "n", "c", "h", "b", "C", "5", "W", "6", "L", "w", "B", "Q", "l", "t", "m", "p", "U", "G"}
//        };
//        StringBuffer shortBuffer = new StringBuffer();
//        Long key = Long.valueOf(id) - 10000000;
//        Long index = 0l;
//        for (int i = 0; i < 4; i++) {
//            Double dou = Math.pow(chars[0].length, i + 1);
//            index = (long) (key % dou);
//            key = (long) (key / dou);
//            if (key > 0) {
//                key = key - 1;
//            }
//            shortBuffer.append(chars[i][index.intValue()]);
//        }
//
//        return shortBuffer.reverse().toString();
//    }

    //生成四位不重复的验证码
    public static String generateReffereeCode(){
        char[] codeSequence ={'A','B','C','D','E','F','G','H','I','J','K',
                'L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z',
                'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s',
                't','u','v','w','x','y','z','0','1','2','3','4','5','6','7','8','9'};
        Random random = new Random();
        //动态字符串
        StringBuilder sb = new StringBuilder();
        int count = 0;
        while(true){
            //随机产生一个下标，通过下标取出字符数组中内容
            char c = codeSequence[random.nextInt(codeSequence.length)];
            //假设取出来的字符在动态字符串中不存在，代表没有重复的
            if(sb.indexOf(c+" ") == -1){
                //追加到动态字符串中
                sb.append(c);
                count++;
                if(count == 4){
                    break;
                }
            }
        }
        return sb.toString();
    }

    public static void main(String[] args) {


    }
}
