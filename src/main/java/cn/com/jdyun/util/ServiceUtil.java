package cn.com.jdyun.util;

import java.util.UUID;

public class ServiceUtil {

    /**
     * 生成主键
     *
     * @return
     */
    public static String generateKey() {
        String key = UUID.randomUUID().toString().replaceAll("-", "").toLowerCase();
        return key;
    }
}
