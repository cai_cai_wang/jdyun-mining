package cn.com.jdyun.controller.admin;

import cn.com.jdyun.dto.UserCredit;
import cn.com.jdyun.exception.BdexGatewayException;
import cn.com.jdyun.exception.WarningException;
import cn.com.jdyun.interceptor.Auth;
import cn.com.jdyun.pojo.User;
import cn.com.jdyun.pojo.UserSec;
import cn.com.jdyun.pojo.UserWallet;
import cn.com.jdyun.service.TransferService;
import cn.com.jdyun.service.UserSecService;
import cn.com.jdyun.service.UserService;
import cn.com.jdyun.util.Constant;
import cn.com.jdyun.util.RedisUtil;
import cn.com.jdyun.util.reponse.Result;
import cn.com.jdyun.util.reponse.Status;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

@RestController("AdminUserController")
public class UserController {

    @Autowired
    private UserService userService;
    @Autowired
    private UserSecService userSecService;
    @Autowired
    private TransferService transferService;
    @Autowired
    private RedisUtil redisUtil;

	/*//新增用户
	@Auth(true)
	@PostMapping("/admin/User/add")
	public Result add(@RequestParam(required=false)String phoneNumber, @RequestParam(required=false)String email, String refereeCode, String password, String repeat) throws Exception {
		if(StringUtils.isBlank(phoneNumber) && StringUtils.isBlank(email)) {
			return Result.create(Status.ILLEGAL_PARAMETER_VALUE, "", "");
		}
		User account = new User();
		if(email != null && WebConstants.validEmail(email)) {
			account.setEmail(email);
			if(userService.existUsername(email)) {
				return Result.create(Status.ACCOUNT_EXISTS, email);
			}
		}
		if(phoneNumber != null && WebConstants.validPhoneNumber(phoneNumber)) {
			account.setPhone(phoneNumber);
			if(userService.existUsername(phoneNumber)) {
				return Result.create(Status.ACCOUNT_EXISTS, phoneNumber);
			}
		}
		if(account.getEmail() == null && account.getPhone() == null) {
			return Result.create(Status.ILLEGAL_PARAMETER_VALUE, "", "");
		}
		if(StringUtils.isBlank(refereeCode)) {
			return Result.create(Status.ILLEGAL_PARAMETER_VALUE, "", "");
		}
		if(!password.equals(repeat)) {
			return Result.create(Status.ILLEGAL_PARAMETER_VALUE, "", "");
		}
		account.setReferee(refereeCode);
		account.setPassword(password);
		userService.save(account);
		account.setPassword(null);
		
		return Result.success(account);
	}
	
	//修改用户信息
	@Auth(true)
	@PostMapping("/admin/User/update")
	public Result update(User account, HttpSession session) throws Exception {
		if(account.getId() == null) {
			return Result.create(Status.ILLEGAL_PARAMETER_VALUE, "", "");
		}
		account.setPassword(null);
		userService.saveOrUpdate(account);
		
		return Result.success(account);
	}*/

    /**
     * 充值
     * @param session
     * @return
     * @throws Exception
     */
    @Auth(true)
    @PostMapping("/admin/rechange")
    public Result rechange(String userId, String amount, String coinType, String telphone, HttpSession session) throws Exception {
        UserCredit userCredit = (UserCredit) session.getAttribute(Constant.ACCOUNT_Mining);
        if(StringUtils.isEmpty(coinType)){
            throw new BdexGatewayException("传入的代币种类不能为空!");
        }
        //查询充值对象的比特宝信息
        UserWallet refereeWallet = transferService.inqueryLockedCoinAmount(userId, coinType);
        transferService.rechangeCoin(userCredit.getId(), refereeWallet, new BigDecimal(amount), telphone, coinType);
        Result r = new Result();
        r.setMsg("充值成功!");
        return r;
    }

    //重置密码
    @Auth(true)
    @PostMapping("/admin/User/reset")
    public Result reset(String userId, String password, HttpSession session) throws Exception {
        if (userId == null || password == null) {
            return Result.create(Status.ILLEGAL_PARAMETER_VALUE, "", "");
        }
        if (password.length() < 6 || password.length() > 12) {
            return Result.create(Status.ILLEGAL_PARAMETER_VALUE, "", "");
        }
        User account = new User();
        account.setId(userId);
        account.setPassword(password);
        userService.saveOrUpdate(account);

        return Result.success(account);
    }

    //获取用户分页信息
    @Auth(true)
    @PostMapping("/admin/User/pageData")
    public Result pageData(@RequestParam Map<String, Object> condition, Integer pageNumber, Integer pageSize) throws Exception {

        PageHelper.startPage(pageNumber, pageSize);

        Page<User> page = userService.pageData(condition);

        return Result.success(page.toPageInfo());
    }

    //账户空投
    @Auth(true)
    @PostMapping("/admin/User/airdrop")
    public Result airdrop(String userId, String balance, HttpSession session) throws Exception {

//		//空投转账
//		transferService.transferFrom(Constant.mainAccountId, userId, Constant.COINTYPE_SSSP, balance,Constant.mainAccountId,session);

        return Result.success(userId);
    }

    //获取用户实名认证审批列表
    @Auth(true)
    @PostMapping("/admin/User/pageSecData")
    public Result pageSecData(@RequestParam Map<String, Object> condition, @RequestParam(defaultValue = "0") Integer pageNumber, @RequestParam(defaultValue = "5") Integer pageSize) throws Exception {

        PageHelper.startPage(pageNumber, pageSize);
        Page<UserSec> page = userSecService.pageSecData(condition);
        return Result.success(page.toPageInfo());
    }

    //用户身份审核：通过/拒绝
    @Auth(true)
    @PostMapping("/admin/User/approve")
    public Result approve(String userId, Byte flag, HttpSession session) throws Exception {

        if (StringUtils.isBlank(userId) || flag == null) {
            return Result.fail("userId and flag is required");
        }
        userSecService.approve(userId, flag);

        return Result.success(userId);
    }

    //用户身份认证信息
    @Auth(true)
    @PostMapping("/admin/User/userSec")
    public Result userSec(String userId, HttpSession session) throws Exception {

        if (StringUtils.isBlank(userId)) {
            return Result.fail("userId is required");
        }
        UserSec userSec = userSecService.queryByUserId(userId);

        return Result.success(userSec);
    }

    //查询所有锁定的用户
    @Auth(true)
    @PostMapping("/admin/User/allRelieveLock")
    public Result allRelieveLock(HttpSession session) throws WarningException {
        List<User> userList = userService.allRelieveLock();
        return Result.success(userList);
    }
    //解除用户锁定状态
    @Auth(true)
    @PostMapping("/admin/User/relieveLock")
    public Result relieveLock(String userId, HttpSession session) throws Exception {
        Result r = new Result();
        userService.updateRelieveLock(userId);
        if (StringUtils.isNotEmpty(redisUtil.get("ERRORINFO:" + userId))) {
            redisUtil.del("ERRORINFO:" + userId);
        }
        r.setMsg(userId + "用户状态恢复正常!");
        return r;
    }

}
