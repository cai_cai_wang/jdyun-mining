package cn.com.jdyun.controller.admin;

import cn.com.jdyun.dto.UserCredit;
import cn.com.jdyun.interceptor.Auth;
import cn.com.jdyun.pojo.Message;
import cn.com.jdyun.service.MessageService;
import cn.com.jdyun.util.Constant;
import cn.com.jdyun.util.reponse.Result;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author admin
 * @date Created in 上午 10:33 2018/8/25 0025
 */
@RestController
public class MessageController {

    @Autowired
    MessageService messageService;

    /**
     * 新增公告消息
     *
     * @param title   主题
     * @param message 消息内容
     * @param session
     * @return
     * @throws Exception
     */
    @Auth(true)
    @PostMapping("/admin/Message/addMessage")
    public Result addMessage(String title, String message, HttpSession session) throws Exception {
        Result r = new Result();
        UserCredit userCredit = (UserCredit) session.getAttribute(Constant.ACCOUNT_Mining);
        messageService.insertCommonMessage(userCredit.getId(), title, message,null);
        r.setMsg("消息新增成功!");
        return r;
    }

    /**
     * 更新公告消息内容
     *
     * @param id
     * @param title
     * @param message
     * @param session
     * @return
     * @throws Exception
     */
    @Auth(true)
    @PostMapping("/admin/Message/updateMessage")
    public Result updateMessage(Integer id, String title, String message, HttpSession session) throws Exception {
        Result r = new Result();
        messageService.updateCommonMessage(id, title, message);
        r.setMsg("消息修改成功!");
        return r;
    }

    /**
     * 发布公告
     *
     * @param id
     * @param session
     * @return
     * @throws Exception
     */
    @Auth(true)
    @PostMapping("/admin/Message/releaseMessage")
    public Result releaseMessage(Integer id, HttpSession session) throws Exception {
        Result r = new Result();
        messageService.releaseMessage(id);
        r.setMsg("消息发布成功!");
        return r;
    }

    /**
     * 获取消息
     *
     * @param publisher
     * @param messageType
     * @param createStart
     * @param createEnd
     * @param pageNumber
     * @param pageSize
     * @return
     * @throws Exception
     */
    @Auth(true)
    @PostMapping("/admin/Message/pageData")
    public Result pageData(@RequestParam(required = false) String publisher, @RequestParam(required = false) Byte messageType, @RequestParam(required = false) Byte flag, @RequestParam(required = false) Date createStart, @RequestParam(required = false) Date createEnd, Integer pageNumber, Integer pageSize) throws Exception {
        PageHelper.startPage(pageNumber, pageSize);
        Map<String, Object> condition = new HashMap<String, Object>();
        if (StringUtils.isNotBlank(publisher)) {
            condition.put("publisher", publisher);
        }
        if (messageType != null) {
            condition.put("messageType", messageType);
        }
        if (flag != null) {
            condition.put("flag", flag);
        }
        if (createStart != null) {
            condition.put("createStart", createStart);
        }
        if (createEnd != null) {
            condition.put("createEnd", createEnd);
        }

        Page<Message> page = messageService.pageManagerData(condition);

        return Result.success(page.toPageInfo());
    }

}
