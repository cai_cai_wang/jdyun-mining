package cn.com.jdyun.controller.admin;

import cn.com.jdyun.dto.UserCredit;
import cn.com.jdyun.exception.BdexGatewayException;
import cn.com.jdyun.interceptor.Auth;
import cn.com.jdyun.pojo.SysConfig;
import cn.com.jdyun.service.SysConfigService;
import cn.com.jdyun.util.ConfigParam;
import cn.com.jdyun.util.Constant;
import cn.com.jdyun.util.reponse.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.List;

@RestController
public class SysConfigController {

    @Autowired
    private SysConfigService sysConfigService;

    /**
     * 設置普通节点的任务奖励
     *
     * @param initValue
     * @param session
     * @return
     * @throws BdexGatewayException
     */
    @Auth(true)
    @PostMapping("/admin/SysConfig/setCommonNodeReward")
    public Result setCommonNodeReward(String initValue, HttpSession session) throws BdexGatewayException {
        UserCredit userCredit = (UserCredit) session.getAttribute(Constant.ACCOUNT_Mining);
        String value = sysConfigService.changeConfigValue(ConfigParam.COMMON_NODE_REWARD, initValue, userCredit.getId());
        return Result.success(value);
    }

    /**
     * 设置超级节点任务奖励
     *
     * @param initValue
     * @param session
     * @return
     * @throws BdexGatewayException
     */
    @Auth(true)
    @PostMapping("/admin/SysConfig/setSuperNodeReward")
    public Result setSuperNodeReward(String initValue, HttpSession session) throws BdexGatewayException {
        UserCredit userCredit = (UserCredit) session.getAttribute(Constant.ACCOUNT_Mining);
        String value = sysConfigService.changeConfigValue(ConfigParam.SUPER_NODE_REWARD, initValue, userCredit.getId());
        return Result.success(value);
    }

    /**
     * 设置各参数值
     *
     * @param session
     * @return
     * @throws BdexGatewayException
     */
    @Auth(true)
    @PostMapping("/admin/SysConfig/setParameterValues")
    public Result setParameterValues(String code,String codeName,String initValue, HttpSession session) throws BdexGatewayException {
        UserCredit userCredit = (UserCredit) session.getAttribute(Constant.ACCOUNT_Mining);
        sysConfigService.initParameterValues(code, codeName,initValue,userCredit.getId());
        Result r = new Result();
        r.setMsg("success");
        return r;
    }
    /**
     * 更新参数值
     * @param session
     * @return
     * @throws BdexGatewayException
     */
    @Auth(true)
    @PostMapping("/admin/SysConfig/updateParameterValues")
    public Result updateParameterValues(SysConfig sysConfig, HttpSession session) throws BdexGatewayException {
        UserCredit userCredit = (UserCredit) session.getAttribute(Constant.ACCOUNT_Mining);
        sysConfig.setOperatorId(userCredit.getId());
        sysConfigService.updateParameterValues(sysConfig);
        Result r = new Result();
        r.setMsg("success");
        return r;
    }


    /**
     * 设置矿池总量
     *
     * @param initValue
     * @param session
     * @return
     * @throws BdexGatewayException
     */
    @Auth(true)
    @PostMapping("/admin/SysConfig/setMinerPoolAmount")
    public Result setMinerPoolAmount(String initValue, HttpSession session) throws BdexGatewayException {
        UserCredit userCredit = (UserCredit) session.getAttribute(Constant.ACCOUNT_Mining);
        String value = sysConfigService.initConfigValue(initValue, userCredit.getId());
        return Result.success(value);
    }

    /**
     * 获取所有的配置参数
     *
     * @return
     * @throws BdexGatewayException
     */
    @Auth(true)
    @PostMapping("/admin/SysConfig/getConfigParams")
    public Result getConfigParams() throws BdexGatewayException {
        List<SysConfig> values = sysConfigService.getConfigParams();
        return Result.success(values);
    }

    /**
     *
     * 查询矿池总量
     * @return
     * @throws BdexGatewayException
     */
    @Auth(true)
    @PostMapping("/admin/SysConfig/getMinerPoolAmount")
    public Result getMinerPoolAmount(String coinType) throws BdexGatewayException {
        Result r = new Result();
        String value = null;
        if(Constant.COINTYPE_SSSP.equals(coinType)){
            value = sysConfigService.queryConfigInitValue(ConfigParam.MINER_POOL_AMOUNT);
        }else if(Constant.COINTYPE_TEX.equals(coinType)){
            value = sysConfigService.queryConfigInitValue(ConfigParam.MINER_POOL_AMOUNT_TEX);
        }else{
            throw new BdexGatewayException("币种类型错误!");
        }
        r.setData("矿池总量为" + value);
        return r;
    }
}
