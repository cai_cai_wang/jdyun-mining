package cn.com.jdyun.controller.admin;

import cn.com.jdyun.pojo.HotTopic;
import cn.com.jdyun.service.HotTopicService;
import cn.com.jdyun.util.reponse.Result;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class SysHotTopicController {

    @Autowired
    private HotTopicService hotTopicService;

    @PostMapping("/admin/SysHotTopic/pageData")
    public Result more(Map<String, Object> condition, Integer pageNumber, Integer pageSize) throws Exception {

        PageHelper.startPage(pageNumber, pageSize);

        Page<HotTopic> page = hotTopicService.pageData(condition);

        return Result.success(page.toPageInfo());
    }
}
