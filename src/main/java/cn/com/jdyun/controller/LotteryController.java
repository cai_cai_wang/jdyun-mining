package cn.com.jdyun.controller;

import cn.com.jdyun.dto.UserCredit;
import cn.com.jdyun.exception.BdexGatewayException;
import cn.com.jdyun.interceptor.Auth;
import cn.com.jdyun.pojo.Lottery;
import cn.com.jdyun.pojo.RewardPoints;
import cn.com.jdyun.service.LotteryService;
import cn.com.jdyun.util.Constant;
import cn.com.jdyun.util.reponse.Result;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;

@RestController
public class LotteryController {

    @Autowired
    private LotteryService lotteryService;

    //查询充值积分情况
    @Auth
    @PostMapping("/Lottery/rewardPoints")
    public Result rewardPoints(String coinType, HttpSession session) throws BdexGatewayException {
        UserCredit userCredit = (UserCredit) session.getAttribute(Constant.ACCOUNT_Mining);

        if (StringUtils.isBlank(coinType)) {
           throw new BdexGatewayException("代币种类不能为空!");
        }

        RewardPoints rewardPoints = lotteryService.queryUserRewardPoints(userCredit.getId(), coinType);

        return Result.success(rewardPoints);
    }

    //抽奖
    @Auth
    @PostMapping("/Lottery/draw")
    public Result draw(@RequestParam(required = false) String coinType, HttpSession session) throws BdexGatewayException {
        UserCredit userCredit = (UserCredit) session.getAttribute(Constant.ACCOUNT_Mining);

        if (StringUtils.isBlank(coinType)) {
            coinType = Constant.COINTYPE_SSSP;
        }

        Lottery lottery = lotteryService.userLottery(userCredit.getId(), coinType);

        return Result.success(lottery);
    }

    //抽奖记录
    @Auth
    @PostMapping("/Lottery/pageData")
    public Result pageData(@RequestParam(required = false) String coinType, Integer pageNumber, Integer pageSize, HttpSession session) throws BdexGatewayException {
        UserCredit userCredit = (UserCredit) session.getAttribute(Constant.ACCOUNT_Mining);

        if (StringUtils.isBlank(coinType)) {
            coinType = Constant.COINTYPE_SSSP;
        }

        PageHelper.startPage(pageNumber, pageSize);

        Page<Lottery> page = lotteryService.pageData(userCredit.getId(), coinType);

        return Result.success(page.toPageInfo());
    }
}
