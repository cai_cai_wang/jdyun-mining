package cn.com.jdyun.controller;

import cn.com.jdyun.pojo.HotTopic;
import cn.com.jdyun.service.HotTopicService;
import cn.com.jdyun.util.reponse.Result;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.List;

@RestController
public class HotTopicController {

    @Autowired
    private HotTopicService hotTopicService;

    @SuppressWarnings("unchecked")
    @PostMapping("/HotTopic/index")
    public Result index(@RequestParam(required = false, defaultValue = "4") Integer number) throws Exception {

        List<HotTopic> result = hotTopicService.lastHotTopic(number);
        if (result == null) {
            result = Collections.EMPTY_LIST;
        }
        return Result.success(result);
    }
}
