package cn.com.jdyun.job;

import cn.com.jdyun.exception.BdexGatewayException;
import cn.com.jdyun.mapper.UserWalletMapper;
import cn.com.jdyun.pojo.UserWallet;
import cn.com.jdyun.service.GainsService;
import cn.com.jdyun.service.UserWalletService;
import cn.com.jdyun.util.Constant;
import cn.com.jdyun.util.WebConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * @author admin
 * @date Created in 下午 2:25 2018/9/25 0025
 */
@Component
@EnableScheduling
public class MiningGainsJob {
    @Autowired
    private GainsService gainsService;
    @Autowired
    private UserWalletService userWalletService;
    @Autowired
    private UserWalletMapper userWalletMapper;

    public void miningEveryDay() throws BdexGatewayException {
        // 获取所有用戶
        String[] coinTypes = {Constant.COINTYPE_SSSP,Constant.COINTYPE_TEX};
        List<UserWallet> wallets = userWalletService.queryAllExistsLockedCoinWallet(coinTypes);
        if (wallets == null || wallets.size() == 0) {
            return;
        }
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.HOUR, -23);
        String date = WebConstants.dateFormat.format(calendar.getTime());
        for (UserWallet userWallet : wallets) {
            //挖矿处理
            gainsService.mining(date, userWallet);
        }

        List<String>  userWalletList = userWalletMapper.selectByTexIdNot();
        for(String str :userWalletList){
            UserWallet userWallet = new UserWallet();
            userWallet.setUserId(str);
            userWallet.setAsset("0");
            userWallet.setMingCoin("0");
            userWallet.setBalance("0");
            userWallet.setCreateTime(new Date());
            userWallet.setUpdateTime(new Date());
            userWallet.setLockedCoin(new BigDecimal("0"));
            userWallet.setFreezeCoin("0");
            userWallet.setCoinType("TEX");
            userWallet.setTaskCoin("0");
            userWalletMapper.insertTexRecord(userWallet);
        }

    }
}
