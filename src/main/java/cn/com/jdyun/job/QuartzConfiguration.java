package cn.com.jdyun.job;

import org.quartz.JobDetail;
import org.quartz.Trigger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.quartz.CronTriggerFactoryBean;
import org.springframework.scheduling.quartz.MethodInvokingJobDetailFactoryBean;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;

/**
 * @author admin
 * @date Created in 下午 2:12 2018/8/26 0026
 */
@Configuration
public class QuartzConfiguration {

    // 配置定时任务1
    @Bean(name = "firstJobDetail")
    public MethodInvokingJobDetailFactoryBean firstJobDetail(ReleaseCoinJob firstJob) {
        MethodInvokingJobDetailFactoryBean jobDetail = new MethodInvokingJobDetailFactoryBean();
        // 是否并发执行
        jobDetail.setConcurrent(false);
        // 为需要执行的实体类对应的对象
        jobDetail.setTargetObject(firstJob);
        // 需要执行的方法
        jobDetail.setTargetMethod("releaseCoin");
        return jobDetail;
    }

    // 配置触发器1
    @Bean(name = "firstTrigger")
    public CronTriggerFactoryBean firstTrigger(JobDetail firstJobDetail) {
        CronTriggerFactoryBean trigger = new CronTriggerFactoryBean();
        trigger.setJobDetail(firstJobDetail);
        // cron表达式
//        trigger.setCronExpression("0 32 15 * * ?");
        trigger.setCronExpression("0 00 04 * * ?");
        return trigger;
    }
    // 配置定时任务2
    @Bean(name = "secondJobDetail")
        public MethodInvokingJobDetailFactoryBean secondJobDetail(MiningGainsJob secondJob) {
        MethodInvokingJobDetailFactoryBean jobDetail = new MethodInvokingJobDetailFactoryBean();
        // 是否并发执行
        jobDetail.setConcurrent(false);
        // 为需要执行的实体类对应的对象
        jobDetail.setTargetObject(secondJob);
        // 需要执行的方法
        jobDetail.setTargetMethod("miningEveryDay");
        return jobDetail;
    }
    // 配置触发器2
    @Bean(name = "secondTrigger")
    public CronTriggerFactoryBean secondTrigger(JobDetail secondJobDetail) {
        CronTriggerFactoryBean trigger = new CronTriggerFactoryBean();
        trigger.setJobDetail(secondJobDetail);
        // cron表达式
        trigger.setCronExpression("0 55 23 * * ?");
//        trigger.setCronExpression("0 05 17 * * ?");
//        trigger.setCronExpression("0/5 * * * * ?");
        return trigger;
    }

    // 配置定时任务3
    @Bean(name = "thirdJobDetail")
    public MethodInvokingJobDetailFactoryBean thirdJobDetail(CheckIsSuperPartner thirdJob) {
        MethodInvokingJobDetailFactoryBean jobDetail = new MethodInvokingJobDetailFactoryBean();
        // 是否并发执行
        jobDetail.setConcurrent(false);
        // 为需要执行的实体类对应的对象
        jobDetail.setTargetObject(thirdJob);
        // 需要执行的方法
        jobDetail.setTargetMethod("isSuperPartnerOrNot");
        return jobDetail;
    }
    // 配置触发器3
    @Bean(name = "thirdTrigger")
    public CronTriggerFactoryBean thirdTrigger(JobDetail thirdJobDetail) {
        CronTriggerFactoryBean trigger = new CronTriggerFactoryBean();
        trigger.setJobDetail(thirdJobDetail);
        // cron表达式
        trigger.setCronExpression("0 50 04 * * ?");
//        trigger.setCronExpression("0/5 * * * * ?");
        return trigger;
    }

    // 配置Scheduler
    @Bean(name = "scheduler")
    public SchedulerFactoryBean schedulerFactory(Trigger firstTrigger, Trigger secondTrigger,Trigger thirdTrigger) {
        SchedulerFactoryBean bean = new SchedulerFactoryBean();
        // 延时启动，应用启动1秒后
        bean.setStartupDelay(1);
        // 注册触发器
        bean.setTriggers(firstTrigger,secondTrigger,thirdTrigger);
        return bean;
    }
}
