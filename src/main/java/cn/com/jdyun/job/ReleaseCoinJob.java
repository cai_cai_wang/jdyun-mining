package cn.com.jdyun.job;

import cn.com.jdyun.exception.BdexGatewayException;
import cn.com.jdyun.mapper.ext.ExtSysConfigMapper;
import cn.com.jdyun.mapper.ext.ExtUserWalletMapper;
import cn.com.jdyun.pojo.LockCoin;
import cn.com.jdyun.pojo.SysConfig;
import cn.com.jdyun.pojo.UserWallet;
import cn.com.jdyun.service.GainsService;
import cn.com.jdyun.service.LockCoinService;
import cn.com.jdyun.service.RefereeService;
import cn.com.jdyun.service.TransferService;
import cn.com.jdyun.service.UserWalletService;
import cn.com.jdyun.util.Constant;
import cn.com.jdyun.util.RedisUtil;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Iterator;
import java.util.List;

/**
 * @author admin
 * @date Created in 下午 2:20 2018/8/26 0026
 */
@Component
@EnableScheduling
public class ReleaseCoinJob {
    private static final org.slf4j.Logger log = LoggerFactory.getLogger(ReleaseCoinJob.class);

    @Autowired
    LockCoinService lockCoinService;
    @Autowired
    TransferService transferService;
    @Autowired
    RefereeService refereeService;
    @Autowired
    UserWalletService userWalletService;
    @Autowired
    RedisUtil redisUtil;
    @Autowired
    GainsService gainsService;
    @Autowired
    ExtUserWalletMapper walletMapper;
    @Autowired
    ExtSysConfigMapper sysConfigMapper;

    //存入后每天释放千分之五到可用SSSP数量中
    public void releaseCoin() throws BdexGatewayException {

        //查询所有锁仓大于0的记录
        String[] coinTypes = {Constant.COINTYPE_SSSP,Constant.COINTYPE_TEX};
        List<LockCoin> lockCoinList = lockCoinService.selectAllLockCoin(coinTypes);
        Iterator iterator = lockCoinList.iterator();
        while (iterator.hasNext()) {
            LockCoin lockCoin = (LockCoin) iterator.next();
            //查询比特宝信息
            UserWallet userWallet = new UserWallet();
            //用户ID
            String userId = lockCoin.getUserId();
            if(lockCoin.getCoinType().equals(Constant.COINTYPE_SSSP)){
                userWallet = transferService.inqueryLockedCoinAmount(userId, Constant.COINTYPE_SSSP);
            }else if(lockCoin.getCoinType().equals(Constant.COINTYPE_TEX)){
                userWallet = transferService.inqueryLockedCoinAmount(userId, Constant.COINTYPE_TEX);
            }else{
                throw new BdexGatewayException("代币类型错误!");
            }
            //锁定的代币
            BigDecimal lockCoinAmount = lockCoin.getLockCoinAmount();
            //释放的代币
            BigDecimal releaseCoinNum;
            if (lockCoinAmount.compareTo(new BigDecimal("10")) > 0) {
                if(lockCoin.getCoinType().equals(Constant.COINTYPE_SSSP)){
                    SysConfig releaseCoinPercentageConfig = sysConfigMapper.selectByCode(Constant.releaseCoinPercentage);
                    releaseCoinNum = lockCoinAmount.multiply(new BigDecimal(releaseCoinPercentageConfig.getInitValue())).setScale(6, BigDecimal.ROUND_HALF_DOWN);
                }else if(lockCoin.getCoinType().equals(Constant.COINTYPE_TEX)){
                    SysConfig releaseTexCoinPercentageConfig = sysConfigMapper.selectByCode(Constant.releaseTexCoinPercentage);
                    releaseCoinNum = lockCoinAmount.multiply(new BigDecimal(releaseTexCoinPercentageConfig.getInitValue())).setScale(6, BigDecimal.ROUND_HALF_DOWN);
                }else{
                    throw new BdexGatewayException("代币类型错误");
                }
            } else {
                releaseCoinNum = lockCoinAmount;
            }
            //释放代币
            userWalletService.releaseCoinTypeReward(userWallet, releaseCoinNum, lockCoin);

        }
    }
}


