//package cn.com.jdyun.job;
//
//import java.util.Calendar;
//import java.util.HashSet;
//import java.util.List;
//import java.util.Set;
//import java.util.concurrent.ExecutorService;
//import java.util.concurrent.LinkedBlockingDeque;
//import java.util.concurrent.ThreadPoolExecutor;
//import java.util.concurrent.TimeUnit;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.scheduling.annotation.Scheduled;
//import org.springframework.stereotype.Component;
//
//import com.google.common.util.concurrent.ThreadFactoryBuilder;
//
//import cn.com.jdyun.pojo.User;
//import cn.com.jdyun.pojo.UserWallet;
//import cn.com.jdyun.service.GainsService;
//import cn.com.jdyun.service.UserService;
//import cn.com.jdyun.service.UserWalletService;
//import cn.com.jdyun.util.Constant;
//import cn.com.jdyun.util.WebConstants;
//
///**
// * 凌晨发放当日用户未主动获取的矿（定时任务），查询当天挖矿所得
// *
// * @author Administrator
// */
//@Component
//public class GainsJob {
//
//    @Autowired
//    private GainsService gainsService;
//    @Autowired
//    private UserWalletService userWalletService;
//
//    private static final Logger logger = LoggerFactory.getLogger(GainsJob.class);
//
//    // 当前主机cpu个数：用于初始化线程池
//    private static final int nThreads = Runtime.getRuntime().availableProcessors();
//    // 初始线程池,最大线程数,程池的工作线程空闲后保持存活的时间:0,时间单位：毫秒,用来储存等待执行任务的队列,线程工厂，当队列和线程池都满了时拒绝任务的策略
//    private static final ExecutorService pool = new ThreadPoolExecutor(nThreads, nThreads * 2, 0L,
//            TimeUnit.MILLISECONDS, new LinkedBlockingDeque<Runnable>(256),
//            new ThreadFactoryBuilder().setNameFormat("thread-%d").build(), new ThreadPoolExecutor.AbortPolicy());
//    private static final Set<String> keys = new HashSet<String>();
//
//    @Scheduled(cron = "0 21 14 * * ?") //每天凌晨执行
//    //@Scheduled(initialDelay=30, fixedDelay=60*60*1000) //启动后执行，间隔一小时再执行
//    public void timerToNow() throws Exception {
//        // 获取所有用戶
//        List<UserWallet> wallets = userWalletService.queryAllExistsLockedCoinWallet(Constant.COINTYPE);
//        if (wallets == null || wallets.size() == 0) {
//            return;
//        }
//        int i = 0;
//        Calendar calendar = Calendar.getInstance();
//        calendar.add(Calendar.HOUR, -23);
//        String date = WebConstants.dateFormat.format(calendar.getTime());
//        while (wallets.size() > i) {
//            try {
//                doActions(wallets.get(i), date);
//            } catch (Exception e) {
//                logger.error(e.getMessage());
//            } finally {
//                i++;
//                if (i % 10 == 9) {
//                    //每十个处理，停顿1s
//                    Thread.sleep(1000);
//                }
//            }
//        }
//    }
//
//    private void doActions(final UserWallet wallet, final String date) throws InterruptedException {
//        //如果队列线程数超过200，则有可能超出线最大队列数限制
//        while (keys.size() >= 200) {
//            Thread.sleep(1000);
//        }
//        keys.add(wallet.getUserId());
//        pool.execute(() -> {
//            try {
//                //挖矿处理
//                gainsService.mining(date, wallet);
//            } catch (Exception e) {
//                logger.error(e.getMessage());
//            } finally {
//                keys.remove(wallet.getUserId());
//            }
//        });
//    }
//}
