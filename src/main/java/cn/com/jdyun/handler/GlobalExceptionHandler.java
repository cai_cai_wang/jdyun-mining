package cn.com.jdyun.handler;

import cn.com.jdyun.exception.AuthException;
import cn.com.jdyun.exception.BdexGatewayException;
import cn.com.jdyun.exception.WarningException;
import cn.com.jdyun.util.reponse.Result;
import cn.com.jdyun.util.reponse.Status;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.net.URLDecoder;

/**
 * 全局异常捕获
 *
 * @author Administrator
 */
@ControllerAdvice
@ResponseBody
public class GlobalExceptionHandler {
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    @ExceptionHandler(value = Exception.class)
    public Result defaultErrorHandler(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {
        response.setContentType(MediaType.APPLICATION_JSON_UTF8_VALUE);

        if (ex instanceof AuthException) {
            return ((AuthException) ex).getResult();
        }

        ex.printStackTrace();

        return Result.create(Status.UNDEFINED_ERROR, null);
    }

    /**
     * 系统异常处理，比如：404,500
     *
     * @param req
     * @param e
     * @return
     * @throws BdexGatewayException
     */
    @ExceptionHandler(value = BdexGatewayException.class)
    @ResponseBody
    public Result defaultErrorHandler(HttpServletRequest req, BdexGatewayException e) throws Exception {

        String uri = req.getRequestURI();
        String queryString = req.getQueryString();
        String logStr = "";
        if (StringUtils.isNotEmpty(queryString)) {
            logStr = uri + ", params:" + URLDecoder.decode(queryString);
        } else {
            logStr = uri + ", params is empty.";
        }

        logger.error("BdexGatewayException：" + logStr + " ERROR: " + e.getMessage());


        Result r = new Result();
        r.setError(e);
        return r;
    }

    /**
     * 警告
     *
     * @param req
     * @param e
     * @return
     * @throws Exception
     */
    @ExceptionHandler(value = WarningException.class)
    @ResponseBody
    public Result warningHandler(HttpServletRequest req, WarningException e) throws Exception {

        String uri = req.getRequestURI();
        String queryString = req.getQueryString();
        String logStr = "";
        if (StringUtils.isNotEmpty(queryString)) {
            logStr = uri + ", params:" + URLDecoder.decode(queryString);
        } else {
            logStr = uri + ", params is empty.";
        }
        logger.info("WarningException：" + logStr + " ERROR: " + e.getMessage());

        Result r = new Result();
        if (StringUtils.isNotEmpty(e.getRedirectURL())) {
            r.setRedirectURL(e.getRedirectURL());
        }
        r.setTraderWarning(e);
        return r;
    }
}
