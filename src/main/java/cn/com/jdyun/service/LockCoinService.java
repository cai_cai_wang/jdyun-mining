package cn.com.jdyun.service;

import cn.com.jdyun.exception.BdexGatewayException;
import cn.com.jdyun.exception.WarningException;
import cn.com.jdyun.pojo.LockCoin;
import cn.com.jdyun.pojo.LockCoinReleaseLog;
import com.github.pagehelper.Page;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author yzping
 * @date Created in 下午 1:03 2018/8/26 0026
 */
public interface LockCoinService {
    /**
     * 新增一条锁仓代币变化的记录
     *
     * @param lockCoinEntity
     * @return
     */
    void addNewLockCoinRecode(LockCoin lockCoinEntity) throws BdexGatewayException;

    /**
     * 查询所有锁仓记录
     *
     * @param coinTypes
     * @return
     * @throws WarningException
     */
    List<LockCoin> selectAllLockCoin(String[] coinTypes) throws WarningException;

    /**
     * 更新锁仓记录
     *
     * @param lockCoin
     */
    void updateByPrimaryKeySelective(LockCoin lockCoin) throws BdexGatewayException;

    /**
     * 获取用户的锁仓的信息（除去锁仓数量为0的记录）
     *
     * @param userId
     * @param cointype
     * @return
     * @throws BdexGatewayException
     */
    List<LockCoin> selectUserLockCoin(String userId, String cointype) throws BdexGatewayException;

    /**
     * 获取用户的锁仓信息（除去锁仓数量为0的记录）
     *
     * @param userId
     * @param cointype
     * @return
     * @throws BdexGatewayException
     */
    List<LockCoin> selectUserLockCoin1(String userId, String cointype) throws BdexGatewayException;

    /**
     * 释放锁仓，兵新增锁仓记录：释放类型：0,好友锁仓释放,1，每日定量释放，2，每日获得好友释放
     *
     * @param userId
     * @param lockCoinId
     * @param lockCoin    当前锁仓数量
     * @param amount      释放量
     * @param coinType
     * @param releaseType
     * @throws BdexGatewayException
     */
    void releaseLockCoin(String userId, Integer lockCoinId, BigDecimal lockCoin, BigDecimal amount, String coinType, Byte releaseType) throws BdexGatewayException;

    /**
     * 新增锁仓释放记录
     * @param lcrl
     */
    void insertIntoReleaseLog(LockCoinReleaseLog lcrl) throws BdexGatewayException;

    /**
     * 查询用户的锁仓记录
     *
     * @param userId
     * @param coinType
     * @return
     * @throws BdexGatewayException
     */
    Page<LockCoin> pageData(String userId, String coinType) throws BdexGatewayException;

    /**
     * 添加新的推荐人挖矿奖励记录
     *
     * @param userId
     * @param date
     * @param toLockedCoin
     * @param toBalance
     * @param coinType
     * @throws BdexGatewayException
     */
    void addNewRefereeIncome(String userId, String date, BigDecimal toLockedCoin, BigDecimal toBalance, String coinType) throws BdexGatewayException;

    /**
     * 查询所有可以释放的锁仓记录
     *
     * @param coinType
     * @return
     */
    List<LockCoin> selectAllEnableUnLockCoin(String coinType) throws BdexGatewayException;
    
    /**
     * 查询用户可以释放的全部锁仓记录
     * 
     * @param userId
     * @param coinType
     * @return
     * @throws BdexGatewayException
     */
    List<LockCoin> queryUserEnableUnlockedCoin(String userId, String coinType) throws BdexGatewayException;
    
    /**
     * 查询所有可以释放的锁仓总额
     * 
     * @param userId
     * @param coinType
     * @return
     * @throws BdexGatewayException
     */
	BigDecimal queryUserEnableUnlockedAmount(String userId, String coinType) throws BdexGatewayException;
}
