package cn.com.jdyun.service;

import cn.com.jdyun.dto.GainsDTO;
import cn.com.jdyun.exception.BdexGatewayException;
import cn.com.jdyun.pojo.Gains;
import cn.com.jdyun.pojo.UserWallet;

import com.github.pagehelper.Page;

import java.util.List;

public interface GainsService {
    /**
     * 查询用户当前已经获取的奖励
     *
     * @param date
     * @param userId
     * @return
     * @throws BdexGatewayException
     */
    Gains queryCurBalance(String date, String userId,String coinType) throws BdexGatewayException;

    /**
     * 挖矿，保存奖励
     *
     * @param date
     * @param wallet
     * @return
     * @throws BdexGatewayException
     */
    Gains mining(String date, UserWallet wallet) throws BdexGatewayException;

    /**
     * 用户的挖矿记录
     *
     * @param userId
     * @return
     * @throws BdexGatewayException
     */
    Page<Gains> pageData(String userId) throws BdexGatewayException;

    /**
     * 查询合伙人下的所有节点用户的挖矿收益
     *
     * @param userId
     * @param coinType
     * @param date
     * @return
     */
    List<GainsDTO> queryPartnerRelation(String userId, String coinType, String date);
}
