package cn.com.jdyun.service;

import cn.com.jdyun.dto.BdexResult;
import cn.com.jdyun.exception.BdexGatewayException;
import cn.com.jdyun.pojo.Message;
import com.github.pagehelper.Page;

import java.util.Map;

public interface MessageService {
    /**
     * 新增发布一条要的消息
     *
     * @param userId
     * @param title       主题
     * @param messageInfo 发送的消息
     * @return
     */
    void insertCommonMessage(String userId, String title, String messageInfo,String status) throws BdexGatewayException;

    /**
     * 更新公告信息
     *
     * @param id
     * @param title
     * @throws BdexGatewayException
     */
    void updateCommonMessage(Integer id, String title, String messageInfo) throws BdexGatewayException;

    /**
     * 发布公告信息
     *
     * @param id
     * @throws BdexGatewayException
     */
    void releaseMessage(Integer id) throws BdexGatewayException;

    /**
     * 查询消息
     *
     * @param pageNumber
     * @param pageSize
     * @return
     */
    BdexResult queryMessageList(Integer pageNumber, Integer pageSize) throws BdexGatewayException;

    /**
     * 分页查询用户消息
     *
     * @param condition
     * @return
     * @throws BdexGatewayException
     */
    Page<Message> pageManagerData(Map<String, Object> condition) throws BdexGatewayException;

    /**
     * 用户个人信息
     *
     * @param userId
     * @param title       主题
     * @param messageInfo 发送的消息
     * @param amount      奖励数额
     * @param coinType    币种类型
     * @return
     * @throws BdexGatewayException
     */
    void insertUserMessage(String userId, String title, String messageInfo, String amount, String coinType,String status) throws BdexGatewayException;

    /**
     * 分页查询用户消息
     *
     * @param userId
     * @param flag
     * @return
     * @throws BdexGatewayException
     */
    Page<Message> pageData(String userId, Byte flag) throws BdexGatewayException;

    /**
     * 设为已读
     *
     * @param userId
     * @param messageId
     * @throws BdexGatewayException
     */
    void readMessage(String userId, String messageId) throws BdexGatewayException;

    /**
     * 删除消息
     *
     * @param userId
     * @param messageId
     * @throws BdexGatewayException
     */
    void delMessage(String userId, String messageId) throws BdexGatewayException;

    /**
     * 全部设为已读
     *
     * @param id
     * @throws BdexGatewayException
     */
    void readAllMessage(String id) throws BdexGatewayException;

    /**
     * 删除全部消息
     *
     * @param userId
     * @throws BdexGatewayException
     */
    void delAllMessage(String userId) throws BdexGatewayException;

}
