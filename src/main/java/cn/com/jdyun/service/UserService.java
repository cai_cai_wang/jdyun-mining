package cn.com.jdyun.service;

import cn.com.jdyun.dto.UserCredit;
import cn.com.jdyun.exception.BdexGatewayException;
import cn.com.jdyun.exception.WarningException;
import cn.com.jdyun.pojo.User;
import com.github.pagehelper.Page;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface UserService {

    /**
     * 根据id获取记录信息
     *
     * @param id
     * @return
     * @throws BdexGatewayException
     */
    User getById(String id) throws BdexGatewayException;

    /**
     * 新增添加记录信息
     *
     * @param user
     * @throws BdexGatewayException
     */
    void save(User user) throws BdexGatewayException;

    /**
     * 用户注册
     * 1，新增用户
     * 2，创建用户钱包
     * 3，赠送糖果:1000
     * 4，若有推荐人，赠送推荐人糖果：100
     * 5，糖果每天凌晨定时解锁5个至可用余额，直至为0
     *
     * @param user
     * @throws BdexGatewayException
     */
    void regist(User user) throws BdexGatewayException;

    /**
     * 新增或修改记录信息
     *
     * @param user
     * @throws BdexGatewayException
     */
    void saveOrUpdate(User user) throws BdexGatewayException;

    /**
     * 修改密码信息
     *
     * @param user
     * @throws BdexGatewayException
     */
    void updateUserPwdEmail(User user) throws BdexGatewayException;

    /**
     * 修改密码信息
     *
     * @param user
     * @throws BdexGatewayException
     */
    void updateUserPwdPhone(User user) throws BdexGatewayException;

    /**
     * 根据id删除记录
     *
     * @param id
     * @throws BdexGatewayException
     */
    void deleteById(String id) throws BdexGatewayException;

    /**
     * 判断记录是否可以删除
     *
     * @param id
     * @return
     * @throws BdexGatewayException
     */
    boolean deleteEnable(String id) throws BdexGatewayException;

    /**
     * 查询所有
     *
     * @return
     * @throws BdexGatewayException
     */
    List<User> selectAll() throws BdexGatewayException;

    /**
     * 根据条件查询记录信息
     *
     * @param user
     * @return
     * @throws BdexGatewayException
     */
    List<User> selectBySelective(User user);

    /**
     * 查询客户的ID
     *
     * @param phone
     * @return
     */
    String inqueryUserId(String phone);

    /**
     * 分页获取信息
     *
     * @param condition
     * @return
     * @throws BdexGatewayException
     */
    Page<User> pageData(Map<String, Object> condition) throws BdexGatewayException;

    /**
     * 更新用户状态
     *
     * @param userId
     * @param flag
     */
    void updateUserFlag(String userId, Byte flag) throws BdexGatewayException;

    /**
     * 登陆
     *
     * @param username
     * @param password
     * @return
     * @throws BdexGatewayException
     */
    UserCredit login(String username, String password) throws BdexGatewayException;

    /**
     * 判断用户名是否存在
     *
     * @param username
     * @return
     * @throws BdexGatewayException
     */
    boolean existUsername(String username) throws BdexGatewayException;

    /**
     * 判断推荐码是否存在
     *
     * @param refereeCode
     * @return
     * @throws Exception
     */
    String checkRefereeCode(String refereeCode) throws Exception;

    /**
     * 发送短信到邮箱
     *
     * @param receiverEmail 接收者邮箱
     * @param code          随机生成的验证码
     * @throws BdexGatewayException
     */
    void sendVerificationCodeToEmail(String receiverEmail, String code) throws BdexGatewayException;

    /**
     * 发送短信到手机
     *
     * @param content  短信内容
     * @param telphone 手机号
     * @param dstime   定时时间 ，为空时表示立即发送
     * @param msgid    客户自定义消息ID
     * @param ext      用户自定义扩展
     * @param msgfmt   提交消息编码格式（UTF-8/GBK）置空时默认是UTF-8
     * @return
     * @throws BdexGatewayException
     */
    Long sendVerificationCodeToTelphone(String content, String message, String telphone, String dstime, String msgid, String ext, String msgfmt) throws BdexGatewayException;


    /**
     * 查询所有被锁定的用户
     *
     * @return
     */
    List<User> allRelieveLock() throws WarningException;

    /**
     * 修复用户锁定状态
     *
     * @param userId
     * @throws BdexGatewayException
     */
    void updateRelieveLock(String userId) throws BdexGatewayException;
    
    /**
     * 用户等级:0 未设置，1，注册会员，2，小矿机，3，大矿机，4，超级节点，5合伙人
     * 
     * @param level
     * @return
     * @throws BdexGatewayException
     */
	Set<String> queryUserIdByLevel(int level) throws BdexGatewayException;
    /**
     * 用户等级:0 未设置，1，注册会员，2，小矿机，3，大矿机，4，超级节点，5合伙人
     *
     * @param level
     * @return
     * @throws BdexGatewayException
     */
	Set<String> queryUserIdByTexLevel(int level) throws BdexGatewayException;


}
