package cn.com.jdyun.service;


import cn.com.jdyun.exception.BdexGatewayException;

/**
 * @author yzping
 * @date Created in 下午 5:03 2018/8/17 0017
 */
public interface MinerService {

    /**
     * 根据代币币种查询用户的代币数量
     *
     * @param userId
     * @param coinType 代币种类
     * @return
     * @throws BdexGatewayException
     */
    String inqueryCoinAmount(String userId, String coinType) throws BdexGatewayException;

    /**
     * 签到后相应的减少矿池中Tex
     *
     * @param userId
     * @param superNode 是否是超级节点
     * @return
     * @throws BdexGatewayException
     */
    String checkMineralPoolTex(String userId, Boolean superNode) throws BdexGatewayException;


    void minerGains(String userId);


}
