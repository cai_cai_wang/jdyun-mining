package cn.com.jdyun.service.impl;

import java.math.BigDecimal;
import java.util.Date;

import cn.com.jdyun.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.com.jdyun.exception.BdexGatewayException;
import cn.com.jdyun.mapper.ExchangeMapper;
import cn.com.jdyun.pojo.Exchange;
import cn.com.jdyun.pojo.UserWallet;
import cn.com.jdyun.service.ExchangeService;
import cn.com.jdyun.service.UserWalletService;
import cn.com.jdyun.util.Constant;

@Service
public class ExchangeServiceImpl implements ExchangeService {
	
	@Autowired
	private ExchangeMapper exchangeMapper;
	@Autowired
	private UserWalletService userWalletService;

	@Autowired
	MessageService messageService;
	
	@Transactional(rollbackFor=Exception.class)
	@Override
	public void exchangeCoin(String userId, String fromCoinType, BigDecimal fromAmount, String toCoinType,
			BigDecimal rate, boolean isRealease) throws BdexGatewayException {
		if(fromAmount == null || fromAmount.doubleValue() <= 0) {
			return;
		}
		UserWallet fromWallet = userWalletService.queryByUserIdAndConiType(userId, fromCoinType);
		if(fromWallet == null) {
			return;
		}
		Date cur = new Date();
		//判断额度是否充足，兵更新源币种钱包币数额
		if(isRealease) {
			if(fromWallet.getLockedCoin().subtract(fromAmount).doubleValue() < 0) {
				return;
			}
			fromWallet.setLockedCoin(fromWallet.getLockedCoin().subtract(fromAmount));
		}else {
			if(new BigDecimal(fromWallet.getBalance()).subtract(fromAmount).doubleValue() < 0) {
				return;
			}
			fromWallet.setBalance(new BigDecimal(fromWallet.getBalance()).subtract(fromAmount).toString());
		}
		fromWallet.setAsset(new BigDecimal(fromWallet.getAsset()).subtract(fromAmount).toString());
		fromWallet.setUpdateTime(cur);
		userWalletService.updateWallet(fromWallet);
		
		//修改目标钱包币数额
		BigDecimal toAmount = fromAmount.multiply(rate);
		UserWallet toWallet = userWalletService.queryByUserIdAndConiType(userId, toCoinType);
		if(toWallet == null) {
			toWallet = userWalletService.createWallet(userId, toCoinType, "0");
		}
		toWallet.setBalance(new BigDecimal(toWallet.getBalance()).add(toAmount).toString());
		toWallet.setAsset(new BigDecimal(toWallet.getAsset()).add(toAmount).toString());
		toWallet.setUpdateTime(cur);
		userWalletService.updateWallet(toWallet);
		
		//保存用户的兑换记录
		Exchange exchange = new Exchange();
		exchange.setFromAmount(fromAmount);
		exchange.setFromCoinType(fromCoinType);
		exchange.setToAmount(toAmount);
		exchange.setToCoinType(toCoinType);
		exchange.setUserId(userId);
		exchange.setRate(rate);
		exchange.setCreateTime(cur);
		exchangeMapper.insert(exchange);
		//新增一条发送锁仓释放消息
		messageService.insertUserMessage(userId, "糖果释放", "糖果释放转化为"+toCoinType, fromAmount.toString(), Constant.CANDY,"1");
	}
	
	@Transactional(rollbackFor=Exception.class)
	@Override
	public void exchangeCandyToSSSP(String userId, BigDecimal fromAmount,String coinType, boolean isRealease) throws BdexGatewayException {
		exchangeCoin(userId, Constant.CANDY, fromAmount,coinType, BigDecimal.valueOf(1), isRealease);
	}

}
