package cn.com.jdyun.service.impl;

import cn.com.jdyun.exception.BdexGatewayException;
import cn.com.jdyun.mapper.ext.ExtTaskMapper;
import cn.com.jdyun.mapper.ext.ExtUserWalletMapper;
import cn.com.jdyun.pojo.Task;
import cn.com.jdyun.service.MinerService;
import cn.com.jdyun.service.SysConfigService;
import cn.com.jdyun.service.UserService;
import cn.com.jdyun.service.UserWalletService;
import cn.com.jdyun.util.ConfigParam;
import cn.com.jdyun.util.Constant;
import cn.com.jdyun.util.RedisUtil;
import cn.com.jdyun.util.WebConstants;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.DateFormat;
import java.util.Date;

/**
 * @author yzping
 * @date Created in 下午 5:05 2018/8/17 0017
 */
@Service
public class MinerServiceImpl implements MinerService {

    @Autowired
    UserService userService;
    @Autowired
    RedisUtil redisUtil;
    @Autowired
    ExtUserWalletMapper userWalletMapper;
    @Autowired
    ExtTaskMapper extTaskMapper;
    @Autowired
    private SysConfigService sysConfigService;
    @Autowired
    private UserWalletService userWalletService;

    /**
     * 查询用户代币数量
     *
     * @param userId
     * @param coinType
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public String inqueryCoinAmount(String userId, String coinType) throws BdexGatewayException {
        String coinAmount = userWalletMapper.inqueryCoinAmount(userId, coinType);
        redisUtil.set(userId + ":" + coinType + " amount", coinAmount);
        return coinAmount;
    }


    /**
     * 签到后相应的减少矿池中Tex
     *
     * @param userId
     * @param superNode 是否是超级节点
     * @return
     * @throws BdexGatewayException
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public String checkMineralPoolTex(String userId, Boolean superNode) throws BdexGatewayException {

        //每天只能签到一次
        Date now = new Date();
        DateFormat d1 = DateFormat.getDateInstance(); //默认语言（汉语）下的默认风格（MEDIUM风格，比如：2008-6-16 20:54:53）
        String dateNow = d1.format(now);
        String date = redisUtil.get("TODAYDATE");
        if (StringUtils.isNotEmpty(date)) {
            if (date.equalsIgnoreCase(dateNow)) {
                throw new BdexGatewayException("您当日已签到了！");
            } else {
                redisUtil.set("TODAYDATE", dateNow);
            }
        }

        String mineralPoolTex = redisUtil.get("MINERALPOOLTEX");
        Double mineralPoolTexValue = null;
        if (StringUtils.isEmpty(mineralPoolTex)) {
            mineralPoolTex = sysConfigService.queryConfigInitValue(ConfigParam.MINER_POOL_AMOUNT);
        }
        mineralPoolTexValue = Double.valueOf(mineralPoolTex);
        String dailyReward = "";
        if (superNode) {
            dailyReward = sysConfigService.queryConfigInitValue(ConfigParam.SUPER_NODE_REWARD);
        } else {
            dailyReward = sysConfigService.queryConfigInitValue(ConfigParam.COMMON_NODE_REWARD);
        }
        Double dailyRewardValue = null;
        dailyRewardValue = Double.valueOf(dailyReward);
        //判断矿池中Tex是否足够
        if (mineralPoolTexValue >= dailyRewardValue) {
            //从矿池中减去相应的Tex
            //mineralPoolTexValue = mineralPoolTexValue - dailyRewardValue;
            sysConfigService.changeConfigValue(ConfigParam.MINER_POOL_AMOUNT, String.valueOf(-dailyRewardValue), userId);

            //保存签到记录
            Task task = new Task();
            task.setUserId(userId);
            task.setReward(dailyReward);
            task.setCreateTime(now);
            task.setDate(WebConstants.dateFormat.format(now));
            extTaskMapper.insert(task);

            //修改用户签到所得 
            userWalletService.updateSignInReward(userId, Constant.COINTYPE_SSSP, dailyReward);

        } else {
            throw new BdexGatewayException("矿池中TEX不足，请联系管理员！");
        }

        //标记已签到
        redisUtil.set("TODAYDATE", dateNow);

        return dailyReward;
    }

    //用户挖矿
    @Override
    public void minerGains(String userId) {

    }

    //普通挖矿
    public void commonMinner() {

    }

    //微挖矿
    public void miniMinner() {

    }

    //强挖矿
    public void strongMinner() {

    }

}
