package cn.com.jdyun.service.impl;

import cn.com.jdyun.exception.BdexGatewayException;
import cn.com.jdyun.mapper.HotTopicMapper;
import cn.com.jdyun.pojo.HotTopic;
import cn.com.jdyun.service.HotTopicService;
import cn.com.jdyun.util.WebConstants;
import cn.com.jdyun.util.reponse.Status;
import com.github.pagehelper.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class HotTopicServiceImpl implements HotTopicService {

    @Autowired
    private HotTopicMapper hotTopicMapper;

    @Transactional(readOnly = true)
    @Override
    public Page<HotTopic> pageData(Map<String, Object> condition) throws BdexGatewayException {
        return hotTopicMapper.pageData(condition);
    }

    @Transactional(readOnly = true)
    @Override
    public Page<HotTopic> pageDataForIndex() throws BdexGatewayException {
        return hotTopicMapper.pageDataForIndex();
    }

    @Transactional(readOnly = true)
    @Override
    public List<HotTopic> lastHotTopic(int number) throws BdexGatewayException {
        return hotTopicMapper.lastHotTopic(number);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void top(Integer id) throws BdexGatewayException {
        HotTopic old = hotTopicMapper.selectByPrimaryKey(id);
        HotTopic record = new HotTopic();
        Date cur = new Date();
        //查询当前最顶端的消息
        int topIncex = hotTopicMapper.selectTopIndex();
        record.setUpdateTime(cur);
        record.setShowOrder(topIncex + 1);

        hotTopicMapper.updateByPrimaryKeySelective(record);
    }

    @Transactional(readOnly = true)
    @Override
    public HotTopic selectById(Integer id) throws BdexGatewayException {
        return hotTopicMapper.selectByPrimaryKey(id);
    }

}
