package cn.com.jdyun.service.impl;

import cn.com.jdyun.exception.BdexGatewayException;
import cn.com.jdyun.mapper.UserSecMapper;
import cn.com.jdyun.pojo.UserSec;
import cn.com.jdyun.service.UserSecService;
import cn.com.jdyun.util.MD5Util;
import cn.com.jdyun.util.reponse.Status;
import com.github.pagehelper.Page;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.Map;

@Service
public class UserSecServiceImpl implements UserSecService {

    @Autowired
    private UserSecMapper userSecMapper;

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void certification(UserSec userSec) throws BdexGatewayException {
        //禁止此处修改密码
        userSec.setSecPassword(null);
        if (StringUtils.isBlank(userSec.getUserId())) {
            return;
        }
        Date cur = new Date();
        UserSec old = userSecMapper.queryByUserId(userSec.getUserId());
        if(old != null && old.getFlag() == 1 && old.getFlag() == 2) {
        	throw new BdexGatewayException("身份认证审核中或已通过，不能重复提交", Status.ACCOUNT_SEC_ID_APPLY_REPEAT.getCode());
        }
        //修改后，需要重新审核
        userSec.setFlag(Byte.valueOf("1"));
        if (old != null) {
            userSec.setId(old.getId());
            userSecMapper.updateByPrimaryKeySelective(userSec);
        } else {
            userSec.setCreateTime(cur);
            userSec.setUpdateTime(cur);
            userSecMapper.insertSelective(userSec);
        }

    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateSecPwd(String userId, String pwd) throws BdexGatewayException {
        if (StringUtils.isBlank(userId)) {
            return;
        }
        UserSec userSec = userSecMapper.queryByUserId(userId);
        Date cur = new Date();
        if (userSec == null) {
            userSec = new UserSec();
            userSec.setFlag(Byte.valueOf("0"));
            userSec.setUserId(userId);
            userSec.setSecPassword(MD5Util.encode(pwd));
            userSec.setUpdateTime(cur);
            userSec.setCreateTime(cur);
            userSecMapper.insertSelective(userSec);
        } else {
            userSec.setSecPassword(MD5Util.encode(pwd));
            userSec.setUpdateTime(cur);
            userSecMapper.updateByPrimaryKeySelective(userSec);
        }
    }

    @Transactional(readOnly = true)
    @Override
    public boolean checkSecPwd(String userId, String pwd) throws BdexGatewayException {
        String dbPwd = userSecMapper.selectSecPwd(userId);
        //如果支付密码为空，提示设置支付密码
        if (StringUtils.isBlank(dbPwd)) {
            throw new BdexGatewayException("请先设置支付密码", Status.ACCOUNT_SEC_PWD_EMPTY.getCode());
        }
        return dbPwd.equals(MD5Util.encode(pwd));
    }

    @Transactional(readOnly = true)
    @Override
    public boolean isAuthentication(String userId) throws BdexGatewayException {
        return userSecMapper.checkCertification(userId);
    }

    @Transactional(readOnly = true)
    @Override
    public Page<UserSec> pageSecData(Map<String, Object> condition) throws BdexGatewayException {
        return userSecMapper.pageData(condition);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void approve(String userId, Byte flag) throws BdexGatewayException {
        if (flag != 2 && flag != 3) {
            throw new BdexGatewayException("操作无效");
        }
        UserSec userSec = userSecMapper.queryByUserId(userId);
        if (userSec.getFlag() == flag || userSec.getFlag() != 1) {
            throw new BdexGatewayException("操作无效");
        }
        userSec.setFlag(flag);
        userSec.setUpdateTime(new Date());
        userSecMapper.updateByPrimaryKeySelective(userSec);
    }

    @Override
    public UserSec queryByUserId(String userId) throws BdexGatewayException {
        return userSecMapper.queryByUserId(userId);
    }


}
