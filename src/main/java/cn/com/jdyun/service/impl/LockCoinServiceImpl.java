package cn.com.jdyun.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;

import cn.com.jdyun.exception.BdexGatewayException;
import cn.com.jdyun.exception.WarningException;
import cn.com.jdyun.mapper.LockCoinMapper;
import cn.com.jdyun.mapper.LockCoinReleaseLogMapper;
import cn.com.jdyun.mapper.RefereeIncomeMapper;
import cn.com.jdyun.pojo.LockCoin;
import cn.com.jdyun.pojo.LockCoinReleaseLog;
import cn.com.jdyun.pojo.RefereeIncome;
import cn.com.jdyun.service.LockCoinService;

/**
 * @author yzping
 * @date Created in 下午 1:03 2018/8/26 0026
 */
@Service
public class LockCoinServiceImpl implements LockCoinService {

    @Autowired
    private LockCoinMapper lockCoinMapper;
    @Autowired
    private LockCoinReleaseLogMapper lockCoinReleaseLogMapper;
    @Autowired
    private RefereeIncomeMapper refereeIncomeMapper;
    @Value("${spring.profiles.active}")
    private String env;

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void addNewLockCoinRecode(LockCoin lockCoinEntity) throws BdexGatewayException {
    	if(env.equals("dev")) {
    		//修改锁仓时间为30天前
        	Calendar calendar = Calendar.getInstance();
    		calendar.add(Calendar.DAY_OF_MONTH, -30);
    		Date date = calendar.getTime();
    		lockCoinEntity.setCreateTime(date);
    	}
    	lockCoinEntity.setIncome(new BigDecimal("0"));
        int result = lockCoinMapper.setNewLockCoinRecode(lockCoinEntity);
        if (result <= 0) {
            throw new BdexGatewayException("新增锁仓记录失败!");
        }
    }

    @Transactional(readOnly = true)
    @Override
    public List<LockCoin> selectAllLockCoin(String[] coinTypes) throws WarningException {
        List<LockCoin> lockCoinList = new ArrayList<>();
        for(String str : coinTypes){
            List<LockCoin> lockCoinLists = lockCoinMapper.selectAllLockCoin(str);
            lockCoinList.addAll(lockCoinLists);
        }
        if (lockCoinList == null || lockCoinList.size() <= 0) {
            throw new WarningException("没有查询到锁仓数据!");
        }
        return lockCoinList;
    }

    /*@Transactional(rollbackFor = Exception.class)
    @Override
    public void updateLockCoin(LockCoin lockCoinEntity) throws BdexGatewayException {
        int result = lockCoinMapper.updateLockCoin(lockCoinEntity);
        if(result<=0){
            throw new BdexGatewayException("更新锁仓失败!");
        }
    }*/
    @Transactional(readOnly = true)
    @Override
    public List<LockCoin> selectUserLockCoin(String userId, String cointype) throws BdexGatewayException {
        return lockCoinMapper.selectUserLockCoin(userId, cointype);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateByPrimaryKeySelective(LockCoin lockCoin) throws BdexGatewayException {
        int result = lockCoinMapper.updateByPrimaryKeySelective(lockCoin);
        if (result <= 0) {
            throw new BdexGatewayException("更新锁仓信息失败!");
        }
    }

    @Transactional(readOnly = true)
    @Override
    public List<LockCoin> selectUserLockCoin1(String userId, String cointype) throws BdexGatewayException {
        return lockCoinMapper.selectUserLockCoin1(userId, cointype);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void releaseLockCoin(String userId, Integer lockCoinId, BigDecimal lockCoin, BigDecimal amount, String coinType, Byte releaseType)
            throws BdexGatewayException {
        //如果锁定的余额不大于0，则表明为无效处理
        if (lockCoin == null || lockCoin.doubleValue() <= 0) {
            return;
        }
        Date cur = new Date();
        LockCoin lc = new LockCoin();
        lc.setId(lockCoinId);
        lc.setLockCoinAmount(lockCoin.subtract(amount));
        if (lc.getLockCoinAmount().doubleValue() < 0) {
            lc.setLockCoinAmount(BigDecimal.valueOf(0));
            amount = lockCoin;
        }
        lc.setUpdateTime(cur);
        lockCoinMapper.updateByPrimaryKeySelective(lc);
        //新增一条释放记录
        LockCoinReleaseLog lcrl = new LockCoinReleaseLog();
        lcrl.setAmount(amount);
        lcrl.setLockedCoin(lockCoin);
        lcrl.setLockCoinId(lockCoinId);
        lcrl.setCoinType(coinType);
        lcrl.setReleaseType(releaseType);
        lcrl.setUserId(userId);
        lcrl.setCreateTime(cur);

        lockCoinReleaseLogMapper.insert(lcrl);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void insertIntoReleaseLog(LockCoinReleaseLog lcrl) throws BdexGatewayException {
        int result = lockCoinReleaseLogMapper.insert(lcrl);
        if(result<=0){
            throw new BdexGatewayException("释放锁仓记录失败!");
        }
    }



    @Transactional(readOnly = true)
    @Override
    public Page<LockCoin> pageData(String userId, String coinType) throws BdexGatewayException {
        return lockCoinMapper.pageData(userId, coinType);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void addNewRefereeIncome(String userId, String date, BigDecimal toLockedCoin, BigDecimal toBalance, String coinType)
            throws BdexGatewayException {
        Date cur = new Date();
        RefereeIncome ri = new RefereeIncome();
        ri.setUserId(userId);
        ri.setDate(date);
        ri.setToLockedCoin(toLockedCoin);
        ri.setToBalance(toBalance);
        ri.setCreateTime(cur);
        ri.setUpdateTime(cur);
        refereeIncomeMapper.insert(ri);
    }

    @Transactional(readOnly = true)
    @Override
    public List<LockCoin> selectAllEnableUnLockCoin(String coinType) throws BdexGatewayException{
        return lockCoinMapper.selectAllEnableUnLockCoin(coinType);
    }
    
    @Transactional(readOnly = true)
	@Override
	public List<LockCoin> queryUserEnableUnlockedCoin(String userId, String coinType) throws BdexGatewayException {
		return lockCoinMapper.queryUserEnableUnlockedCoin(userId, coinType);
	}
    
    @Transactional(readOnly = true)
	@Override
	public BigDecimal queryUserEnableUnlockedAmount(String userId, String coinType) throws BdexGatewayException {
    	BigDecimal result = lockCoinMapper.queryUserEnableUnlockedAmount(userId, coinType);
    	if(result == null) {
    		result = BigDecimal.valueOf(0);
    	}
    	return result;
	}
}
