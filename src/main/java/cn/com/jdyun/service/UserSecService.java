package cn.com.jdyun.service;

import cn.com.jdyun.exception.BdexGatewayException;
import cn.com.jdyun.pojo.UserSec;
import com.github.pagehelper.Page;

import java.util.Map;

public interface UserSecService {

    /**
     * 实名认证
     *
     * @param userSec
     * @throws BdexGatewayException
     */
    void certification(UserSec userSec) throws BdexGatewayException;

    /**
     * 修改用户的支付密码
     *
     * @param userId
     * @param pwd
     * @throws BdexGatewayException
     */
    void updateSecPwd(String userId, String pwd) throws BdexGatewayException;

    /**
     * 验证用户的支付密码
     *
     * @param userId
     * @param pwd
     * @throws BdexGatewayException
     */
    boolean checkSecPwd(String userId, String pwd) throws BdexGatewayException;

    /**
     * 判断用户是否已经实名认证
     *
     * @param userId
     * @return
     * @throws BdexGatewayException
     */
    boolean isAuthentication(String userId) throws BdexGatewayException;

    /**
     * 查询身份认证审批信息
     *
     * @param condition
     * @return
     * @throws BdexGatewayException
     */
    Page<UserSec> pageSecData(Map<String, Object> condition) throws BdexGatewayException;

    /**
     * 审批：0 待审批 1审批通过 2不通过
     *
     * @param userId
     * @param flag
     * @throws BdexGatewayException
     */
    void approve(String userId, Byte flag) throws BdexGatewayException;

    /**
     * 查看用户的身份认证信息
     *
     * @param userId
     * @return
     * @throws BdexGatewayException
     */
    UserSec queryByUserId(String userId) throws BdexGatewayException;


}
