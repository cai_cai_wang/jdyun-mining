package cn.com.jdyun.service;


import cn.com.jdyun.exception.BdexGatewayException;
import cn.com.jdyun.pojo.User;
import cn.com.jdyun.pojo.UserWallet;

import java.util.List;
import java.util.Set;

public interface RefereeService {

    /**
     * 建立推荐关系
     *
     * @param userId
     * @param refereeId
     * @throws BdexGatewayException
     */
    void add(String userId, String refereeId) throws BdexGatewayException;

    /**
     * 查询用户推荐的用户信息列表
     *
     * @param userId
     * @return
     * @throws BdexGatewayException
     */
    List<User> userReferees(String userId) throws BdexGatewayException;

    /**
     * 推荐该用户的用户信息
     *
     * @param userId
     * @return
     * @throws BdexGatewayException
     */
    User userReferee(String userId) throws BdexGatewayException;

    /**
     * 查找当前账户是否可以成为合伙人
     *
     * @param userId
     * @return
     * @throws BdexGatewayException
     */
    boolean checkPartner(String userId) throws BdexGatewayException;
    
    /**
     * 查找当前账户是否可以成为超级节点
     *
     * @param userId
     * @return
     * @throws BdexGatewayException
     */
    boolean checkSuperNode(String userId,String coinType) throws BdexGatewayException;

    /**
     * 查询用户下二级用户信息
     *
     * @param userId
     * @return
     */
    List<UserWallet> queryAllUserWallet(String userId) throws BdexGatewayException;

    /**
     * 查询所有超级节点
     * 1，超级节点
     * 1.1 自己至少属于小矿机
     * 1.2 一级推荐人中至少有三个小矿机
     * 1.3 一二级推荐人锁仓总量达到了20万
     *
     * @return
     * @throws BdexGatewayException
     */
    Set<String> queryAllSuperNodeId() throws BdexGatewayException;
    /**
     * 查询所有Tex超级节点
     * 1，超级节点
     * 1.1 自己至少属于小矿机
     * 1.2 一级推荐人中至少有三个小矿机
     * 1.3 一二级推荐人锁仓总量达到了20万
     *
     * @return
     * @throws BdexGatewayException
     */
    Set<String> queryAllTexSuperNodeId() throws BdexGatewayException;

    /**
     * 查询所有合伙人
     * 2，合伙人
     * 2.1 自己是大矿机
     * 2.2 邀请至少三个超级节点
     * @param superNodeIds
     * @return
     * @throws BdexGatewayException
     */
    Set<String> queryAllPartnerId(Set<String> superNodeIds) throws BdexGatewayException;
    /**
     * 查询所有Tex合伙人
     * 2，合伙人
     * 2.1 自己是大矿机
     * 2.2 邀请至少三个超级节点
     * @param superTexNodeIds
     * @return
     * @throws BdexGatewayException
     */
    Set<String> queryAllTexPartnerId(Set<String> superTexNodeIds) throws BdexGatewayException;

    /**
     * 查询用户的一级推荐人个数
     *
     * @param userId
     * @return
     * @throws BdexGatewayException
     */
    int selectLevel1Amount(String userId) throws BdexGatewayException;

    /**
     * 查询用户二级推荐人个数
     *
     * @param userId
     * @return
     * @throws BdexGatewayException
     */
    int selectLevel2Amount(String userId) throws BdexGatewayException;

}
