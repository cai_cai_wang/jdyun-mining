package cn.com.jdyun.service;

import cn.com.jdyun.exception.BdexGatewayException;
import cn.com.jdyun.pojo.Lottery;
import cn.com.jdyun.pojo.RewardPoints;
import com.github.pagehelper.Page;

public interface LotteryService {

    /**
     * 充值积累积分（记录充值总额，并记录可抽奖次数）
     *
     * @param userId   用户id
     * @param amount   充值数量
     * @param coinType 充值币种
     * @throws BdexGatewayException
     */
    void updatePoint(String userId, String amount, String coinType) throws BdexGatewayException;

    /**
     * 查询用户的积分表
     *
     * @param userId
     * @return
     * @throws BdexGatewayException
     */
    RewardPoints queryUserRewardPoints(String userId, String coinType) throws BdexGatewayException;

    /**
     * 用户抽奖
     *
     * @param userId
     * @return
     * @throws BdexGatewayException
     */
    Lottery userLottery(String userId, String coinType) throws BdexGatewayException;

    /**
     * 查询用户的抽奖信息列表
     *
     * @param userId
     * @return
     * @throws BdexGatewayException
     */
    Page<Lottery> pageData(String userId, String coinType) throws BdexGatewayException;

}
