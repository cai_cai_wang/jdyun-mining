package cn.com.jdyun.service;


import cn.com.jdyun.exception.BdexGatewayException;
import cn.com.jdyun.pojo.LockCoin;
import cn.com.jdyun.pojo.PartnerIncome;
import cn.com.jdyun.pojo.User;
import cn.com.jdyun.pojo.UserCandy;
import cn.com.jdyun.pojo.UserWallet;
import cn.com.jdyun.pojo.Withdraw;

import javax.servlet.http.HttpSession;
import java.math.BigDecimal;
import java.util.List;

public interface UserWalletService {

    /**
     * 根据用户的id和币种查询比特宝
     *
     * @param userId
     * @param coinType
     * @return
     * @throws BdexGatewayException
     */
    UserWallet queryByUserIdAndConiType(String userId, String coinType) throws BdexGatewayException;

    /**
     * 创建比特宝
     *
     * @param userId
     * @param coinType 币种
     * @param amount   初始化值（若为null或空，默认作0）
     * @return
     * @throws BdexGatewayException
     */
    UserWallet createWallet(String userId, String coinType, String amount) throws BdexGatewayException;

    /**
     * 查询合伙人信息
     *
     * @param userId
     * @param date
     * @return
     */
    PartnerIncome inqueryByUserId(String userId, String date);

    /**
     * 根据用户的id和币种查询比特宝
     *
     * @param userId
     * @return
     */
    UserWallet selectByPrimaryKey(String userId);


    /**
     * 修改比特宝信息
     *
     * @param wallet
     * @throws BdexGatewayException
     */
    void updateWallet(UserWallet wallet) throws BdexGatewayException;

    /**
     * 查询锁仓金额大于amount的记录
     *
     * @param coinType
     * @return
     */
    List<UserWallet> findSuperNodes(String coinType, BigDecimal amount) throws BdexGatewayException;

    /**
     * 提币
     *
     * @param userId   用户id
     * @param amount   提币数量
     * @param coinType 提取币种
     * @param fee      费用
     * @return
     * @throws BdexGatewayException
     */
    Withdraw withdraw(String userId, String amount, String coinType, String fee, HttpSession session) throws BdexGatewayException;

    /**
     * 用户签到奖励
     *
     * @param userId
     * @param coinType
     * @param reward
     * @return
     * @throws BdexGatewayException
     */
    UserWallet updateSignInReward(String userId, String coinType, String reward) throws BdexGatewayException;

    /**
     * 用户挖矿所得
     *
     * @param userId
     * @param coinType
     * @param reward
     * @return
     * @throws BdexGatewayException
     */
    UserWallet updateMiningReward(String userId, String coinType, String reward) throws BdexGatewayException;

    /**
     * 赠送货币
     *
     * @param userId   用户id
     * @param amount   数量
     * @param coinType 币种
     * @param isInvite 是否是邀请所得
     * @return
     * @throws BdexGatewayException
     */
    LockCoin presentCoin(String userId, String amount, String coinType, boolean isInvite) throws BdexGatewayException;

    /**
     * 查询币种的所有用户钱包，锁仓值大于0
     *
     * @param coinType
     * @return
     */
    List<UserWallet> findLockedCoinWallet(String coinType) throws BdexGatewayException;

    /**
     * 释放糖果
     *
     * @param wallet
     * @param amount
     * @throws BdexGatewayException
     */
    void freeCandyCoin(UserWallet wallet, String amount,String coinType) throws BdexGatewayException;

    /**
     * 查看当前用户是否满足成为合伙人条件1
     *
     * @param userId
     * @param strongLockCoinNum
     * @return
     */
    Boolean queryStrongAmount(String userId, String coinType, BigDecimal strongLockCoinNum);

    /**
     * 更新比特宝
     *
     * @param userWallet
     */
    void updateWalletCoinAmount(UserWallet userWallet) throws BdexGatewayException;

    /**
     * 推荐人奖励
     *
     * @param user
     * @param refereeReward
     * @param date
     * @throws BdexGatewayException
     */
    void releaseCoinTypeRefereeReward(User user, BigDecimal refereeReward, String date) throws BdexGatewayException;

    /**
     * 锁仓后推荐人释放SSP
     *
     * @param lockAmount
     * @throws BdexGatewayException
     */
    void releaseSSPRefereeReward(User referee, BigDecimal lockAmount,String coinType) throws BdexGatewayException;

    /**
     * 锁仓
     *
     * @param wallet
     * @param lockAmount
     * @param giftAmount
     */
    void lockSSPReward(UserWallet wallet, BigDecimal lockAmount, BigDecimal giftAmount,String coinType) throws BdexGatewayException;

    /**
     * SSSP释放
     *
     * @param userWallet
     * @throws BdexGatewayException
     */
    void releaseCoinTypeReward(UserWallet userWallet, BigDecimal releaseCoinNum, LockCoin lockCoin) throws BdexGatewayException;

    /**
     * 查询用户的所有钱包
     *
     * @param userId
     * @return
     * @throws BdexGatewayException
     */
    List<UserWallet> queryUserWallets(String userId) throws BdexGatewayException;

    /**
     * 查询用户的排名
     *
     * @param amount
     * @param cointype
     * @return
     */
    int queryRankByAmountAndConiType(BigDecimal amount, String cointype) throws BdexGatewayException;

    /**
     * 更新用户作为合伙人的收益：从矿池中获取，存入用户余额
     *
     * @param userId
     * @param curUserIncome
     * @param cointype
     * @param date
     */
    void updatePartnerIncome(String userId, BigDecimal curUserIncome, String cointype, String date) throws BdexGatewayException;

    /**
     * 获取用户的糖果相关信息：总数和释放数
     *
     * @param userId
     * @return
     * @throws BdexGatewayException
     */
    UserCandy queryUserCandyInfo(String userId) throws BdexGatewayException;
    
    /**
     * 查询所有存在锁仓的账户钱包
     * @param cointype
     * @return
     * @throws BdexGatewayException
     */
	List<UserWallet> queryAllExistsLockedCoinWallet(String[] cointype) throws BdexGatewayException;

    /**
     * 推荐人得到锁仓的奖励
     */
    void setBalanceOfReward();
}
