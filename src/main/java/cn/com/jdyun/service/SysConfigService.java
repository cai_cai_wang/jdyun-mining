package cn.com.jdyun.service;


import cn.com.jdyun.exception.BdexGatewayException;
import cn.com.jdyun.pojo.SysConfig;
import cn.com.jdyun.util.ConfigParam;

import java.math.BigDecimal;
import java.util.List;

public interface SysConfigService {

    /**
     * 查询配置属性的值(在高并发情况下，这个方法获取的值并不一定是最新的值)
     *
     * @param code
     * @return
     * @throws BdexGatewayException
     */
    String queryConfigInitValue(ConfigParam code) throws BdexGatewayException;

    /**
     * 获取所有的配置参数
     *
     * @return
     */
    List<SysConfig> getConfigParams();

    /**
     * 改变改资金池
     *
     * @param code   参数名称
     * @param amount 变动的金额：增加为整数，减少为负数
     * @param userId
     * @return
     * @throws BdexGatewayException
     */
    String changeConfigValue(ConfigParam code, String amount, String userId) throws BdexGatewayException;

    /**
     * 设置参数值
     * @param code  参数code
     * @param codeName 参数名称
     * @param initValue 初始化值
     * @param operatorId 操作人
     * @return
     */
    void initParameterValues(String code,String codeName,String initValue,String operatorId) throws BdexGatewayException;
    /**
     * 初始化矿池
     *
     * @param initValue
     * @param userId
     * @return
     * @throws BdexGatewayException
     */
    String initConfigValue(String initValue, String userId) throws BdexGatewayException;

    /**
     * 更新参数值
     * @param sysConfig
     */
    void updateParameterValues(SysConfig sysConfig) throws BdexGatewayException;
}
