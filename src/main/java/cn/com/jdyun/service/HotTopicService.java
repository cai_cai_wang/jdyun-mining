package cn.com.jdyun.service;

import cn.com.jdyun.exception.BdexGatewayException;
import cn.com.jdyun.pojo.HotTopic;
import com.github.pagehelper.Page;

import java.util.List;
import java.util.Map;

/**
 * 热点动态
 *
 * @author Administrator
 */
public interface HotTopicService {
    /**
     * 分页查询
     *
     * @param condition
     * @return
     * @throws BdexGatewayException
     */
    Page<HotTopic> pageData(Map<String, Object> condition) throws BdexGatewayException;

    /**
     * 分页查询,首页更多动态
     *
     * @return
     * @throws BdexGatewayException
     */
    Page<HotTopic> pageDataForIndex() throws BdexGatewayException;

    /**
     * 首页展示最新动态
     *
     * @param number 条数
     * @return
     * @throws BdexGatewayException
     */
    List<HotTopic> lastHotTopic(int number) throws BdexGatewayException;

    /**
     * 置顶
     *
     * @param id
     * @throws BdexGatewayException
     */
    void top(Integer id) throws BdexGatewayException;

    /**
     * 根据id获取热点动态详情
     *
     * @param id
     * @return
     * @throws BdexGatewayException
     */
    HotTopic selectById(Integer id) throws BdexGatewayException;
}
