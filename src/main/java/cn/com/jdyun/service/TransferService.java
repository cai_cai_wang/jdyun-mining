package cn.com.jdyun.service;

import cn.com.jdyun.dto.TransferRecordsDTO;
import cn.com.jdyun.exception.BdexGatewayException;
import cn.com.jdyun.pojo.TransferRecords;
import cn.com.jdyun.pojo.UserWallet;
import com.github.pagehelper.Page;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.util.Map;

/**
 * @author yzping
 * @date Created in 下午 2:15 2018/8/25 0025
 */
public interface TransferService {
    /**
     * 转账
     *
     * @param coinType
     * @param amount
     * @param from
     * @param to
     * @return
     */
    TransferRecords transferFrom(String from, String to, String coinType, String amount, String secPassword, String receivePhone, String remarks) throws BdexGatewayException, UnsupportedEncodingException;

    /**
     * 锁仓
     *
     * @param userId      用户ID
     * @param lockAmount  锁仓的Tex数量
     * @param secPassword 交易密码
     */
    Map<String, String> lockPosition(String userId, String coinType,String lockAmount, String secPassword) throws BdexGatewayException;

    /**
     * 查询比特宝钱包
     *
     * @param userId
     * @param coinType
     * @return
     */
    UserWallet inqueryLockedCoinAmount(String userId, String coinType) throws BdexGatewayException;

    /**
     * 充值
     * @param adminId
     * @param refereeWallet
     * @param rechangeNum
     * @param coinType
     */
    void rechangeCoin(String adminId, UserWallet refereeWallet, BigDecimal rechangeNum,String telphone,String coinType) throws BdexGatewayException, UnsupportedEncodingException;
    /**
     * 查询用户的转账记录
     *
     * @param userId
     * @param coinType
     * @return
     * @throws BdexGatewayException
     */
    Page<TransferRecords> pageData(String userId, String coinType) throws BdexGatewayException;

    /**
     * 根据交易id查询交易详情
     *
     * @param transactionId
     * @return
     * @throws BdexGatewayException
     */
    TransferRecordsDTO queryTransferDetail(Integer transactionId) throws BdexGatewayException;
}
