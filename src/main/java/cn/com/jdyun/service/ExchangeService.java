package cn.com.jdyun.service;

import java.math.BigDecimal;

import cn.com.jdyun.exception.BdexGatewayException;

public interface ExchangeService {
	
	/**
	 * 币币兑换
	 * @param userId 用户id
	 * @param fromCoinType 转换币种类型
	 * @param fromAmount 转换数额
	 * @param toCoinType 转换目标币种类型
	 * @param rate 汇率
	 * @param isRealease 是否是释放锁仓
	 * @throws BdexGatewayException
	 */
	void exchangeCoin(String userId, String fromCoinType, BigDecimal fromAmount, String toCoinType, BigDecimal rate, boolean isRealease) throws BdexGatewayException;
	
	/**
	 * 转化糖果到SSSP
	 * @param userId 用户id
	 * @param fromAmount 转换数额
	 * @param isRealease 是否是释放锁仓
	 * @throws BdexGatewayException
	 */
	void exchangeCandyToSSSP(String userId, BigDecimal fromAmount, String coinType,boolean isRealease) throws BdexGatewayException;
	
}
