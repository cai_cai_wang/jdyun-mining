package cn.com.jdyun.mapper;

import cn.com.jdyun.pojo.PoolLog;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.annotations.UpdateProvider;
import org.apache.ibatis.type.JdbcType;

public interface PoolLogMapper {
    @Delete({
            "delete from phone_pool_log",
            "where id = #{id,jdbcType=INTEGER}"
    })
    int deleteByPrimaryKey(Integer id);

    @Insert({
            "insert into phone_pool_log (pool_balance, coin_amount, ",
            "coin_type, user_id, ",
            "memo, create_time)",
            "values (#{poolBalance,jdbcType=VARCHAR}, #{coinAmount,jdbcType=VARCHAR}, ",
            "#{coinType,jdbcType=VARCHAR}, #{userId,jdbcType=VARCHAR}, ",
            "#{memo,jdbcType=VARCHAR}, #{createTime,jdbcType=TIMESTAMP})"
    })
    @SelectKey(statement = "SELECT LAST_INSERT_ID()", keyProperty = "id", before = false, resultType = Integer.class)
    int insert(PoolLog record);

    @InsertProvider(type = PoolLogSqlProvider.class, method = "insertSelective")
    @SelectKey(statement = "SELECT LAST_INSERT_ID()", keyProperty = "id", before = false, resultType = Integer.class)
    int insertSelective(PoolLog record);

    @Select({
            "select",
            "id, pool_balance, coin_amount, coin_type, user_id, memo, create_time",
            "from phone_pool_log",
            "where id = #{id,jdbcType=INTEGER}"
    })
    @Results({
            @Result(column = "id", property = "id", jdbcType = JdbcType.INTEGER, id = true),
            @Result(column = "pool_balance", property = "poolBalance", jdbcType = JdbcType.VARCHAR),
            @Result(column = "coin_amount", property = "coinAmount", jdbcType = JdbcType.VARCHAR),
            @Result(column = "coin_type", property = "coinType", jdbcType = JdbcType.VARCHAR),
            @Result(column = "user_id", property = "userId", jdbcType = JdbcType.VARCHAR),
            @Result(column = "memo", property = "memo", jdbcType = JdbcType.VARCHAR),
            @Result(column = "create_time", property = "createTime", jdbcType = JdbcType.TIMESTAMP)
    })
    PoolLog selectByPrimaryKey(Integer id);

    @UpdateProvider(type = PoolLogSqlProvider.class, method = "updateByPrimaryKeySelective")
    int updateByPrimaryKeySelective(PoolLog record);

    @Update({
            "update phone_pool_log",
            "set pool_balance = #{poolBalance,jdbcType=VARCHAR},",
            "coin_amount = #{coinAmount,jdbcType=VARCHAR},",
            "coin_type = #{coinType,jdbcType=VARCHAR},",
            "user_id = #{userId,jdbcType=VARCHAR},",
            "memo = #{memo,jdbcType=VARCHAR},",
            "create_time = #{createTime,jdbcType=TIMESTAMP}",
            "where id = #{id,jdbcType=INTEGER}"
    })
    int updateByPrimaryKey(PoolLog record);
}