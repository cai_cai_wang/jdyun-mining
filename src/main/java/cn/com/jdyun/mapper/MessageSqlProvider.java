package cn.com.jdyun.mapper;

import cn.com.jdyun.pojo.Message;
import org.apache.ibatis.jdbc.SQL;

import java.util.Map;

public class MessageSqlProvider {

    public String insertSelective(Message record) {
        SQL sql = new SQL();
        sql.INSERT_INTO("phone_message");

        if (record.getTitle() != null) {
            sql.VALUES("title", "#{title,jdbcType=VARCHAR}");
        }

        if (record.getMessage() != null) {
            sql.VALUES("message", "#{message,jdbcType=VARCHAR}");
        }

        if (record.getAmount() != null) {
            sql.VALUES("amount", "#{amount,jdbcType=VARCHAR}");
        }

        if (record.getCoinType() != null) {
            sql.VALUES("coin_type", "#{coinType,jdbcType=VARCHAR}");
        }

        if (record.getMessageType() != null) {
            sql.VALUES("message_type", "#{messageType,jdbcType=TINYINT}");
        }

        if (record.getPublisher() != null) {
            sql.VALUES("publisher", "#{publisher,jdbcType=VARCHAR}");
        }

        if (record.getFlag() != null) {
            sql.VALUES("flag", "#{flag,jdbcType=TINYINT}");
        }
        if (record.getStatus() != null) {
            sql.VALUES("status", "#{status,jdbcType=TINYINT}");
        }

        if (record.getCreateTime() != null) {
            sql.VALUES("create_time", "#{createTime,jdbcType=TIMESTAMP}");
        }

        if (record.getUpdateTime() != null) {
            sql.VALUES("update_time", "#{updateTime,jdbcType=TIMESTAMP}");
        }

        return sql.toString();
    }

    public String updateByPrimaryKeySelective(Message record) {
        SQL sql = new SQL();
        sql.UPDATE("phone_message");

        if (record.getTitle() != null) {
            sql.SET("title = #{title,jdbcType=VARCHAR}");
        }

        if (record.getMessage() != null) {
            sql.SET("message = #{message,jdbcType=VARCHAR}");
        }

        if (record.getAmount() != null) {
            sql.SET("amount = #{amount,jdbcType=VARCHAR}");
        }

        if (record.getCoinType() != null) {
            sql.SET("coin_type = #{coinType,jdbcType=VARCHAR}");
        }

        if (record.getMessageType() != null) {
            sql.SET("message_type = #{messageType,jdbcType=TINYINT}");
        }

        if (record.getPublisher() != null) {
            sql.SET("publisher = #{publisher,jdbcType=VARCHAR}");
        }

        if (record.getFlag() != null) {
            sql.SET("flag = #{flag,jdbcType=TINYINT}");
        }

        if (record.getStatus() != null) {
            sql.SET("status = #{status,jdbcType=TINYINT}");
        }

        if (record.getCreateTime() != null) {
            sql.SET("create_time = #{createTime,jdbcType=TIMESTAMP}");
        }

        if (record.getUpdateTime() != null) {
            sql.SET("update_time = #{updateTime,jdbcType=TIMESTAMP}");
        }

        sql.WHERE("id = #{id,jdbcType=INTEGER}");

        return sql.toString();
    }

    public String pageManagerData(Map<String, Object> condition) {

        SQL sql = new SQL();
        sql.SELECT("id, title, message, amount, coin_type, message_type, publisher, flag, status,create_time, update_time");
        sql.FROM("phone_message");

        StringBuffer where = new StringBuffer(" 1 = 1");
        if (condition != null) {
            if (condition.containsKey("publisher") && condition.get("publisher") != null) {
                where.append(" and publisher = #{publisher, jdbcType=VARCHAR}");
            }
            if (condition.containsKey("messageType") && condition.get("messageType") != null) {
                where.append(" and message_type = #{messageType,jdbcType=TINYINT}");
            }
            if (condition.containsKey("flag") && condition.get("flag") != null) {
                where.append(" and flag = #{flag,jdbcType=TINYINT}");
            }
            if (condition.containsKey("status") && condition.get("status") != null) {
                where.append(" and status = #{status,jdbcType=TINYINT}");
            }
            if (condition.containsKey("dateStart") && condition.get("dateStart") != null) {
                where.append(" and create_time >= #{dateStart,jdbcType=TIMESTAMP}");
            }
            if (condition.containsKey("dateEnd") && condition.get("dateEnd") != null) {
                where.append(" and create_time < #{dateEnd,jdbcType=TIMESTAMP}");
            }
        }
        sql.WHERE(where.toString());
        sql.ORDER_BY("create_time desc");
        return sql.toString();
    }
}