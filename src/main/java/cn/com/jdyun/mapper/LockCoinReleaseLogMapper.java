package cn.com.jdyun.mapper;

import cn.com.jdyun.pojo.LockCoinReleaseLog;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.annotations.UpdateProvider;
import org.apache.ibatis.type.JdbcType;

public interface LockCoinReleaseLogMapper {
    @Delete({
            "delete from phone_lock_coin_release_log",
            "where id = #{id,jdbcType=INTEGER}"
    })
    int deleteByPrimaryKey(Integer id);

    @Insert({
            "insert into phone_lock_coin_release_log (user_id, lock_coin_id, ",
            "coin_type, locked_coin, ",
            "amount, release_type, ",
            "create_time)",
            "values (#{userId,jdbcType=VARCHAR}, #{lockCoinId,jdbcType=INTEGER}, ",
            "#{coinType,jdbcType=VARCHAR}, #{lockedCoin,jdbcType=DECIMAL}, ",
            "#{amount,jdbcType=DECIMAL}, #{releaseType,jdbcType=TINYINT}, ",
            "#{createTime,jdbcType=TIMESTAMP})"
    })
    @SelectKey(statement = "SELECT LAST_INSERT_ID()", keyProperty = "id", before = false, resultType = Integer.class)
    int insert(LockCoinReleaseLog record);

    @InsertProvider(type = LockCoinReleaseLogSqlProvider.class, method = "insertSelective")
    @SelectKey(statement = "SELECT LAST_INSERT_ID()", keyProperty = "id", before = false, resultType = Integer.class)
    int insertSelective(LockCoinReleaseLog record);

    @Select({
            "select",
            "id, user_id, lock_coin_id, coin_type, locked_coin, amount, release_type, create_time",
            "from phone_lock_coin_release_log",
            "where id = #{id,jdbcType=INTEGER}"
    })
    @Results({
            @Result(column = "id", property = "id", jdbcType = JdbcType.INTEGER, id = true),
            @Result(column = "user_id", property = "userId", jdbcType = JdbcType.VARCHAR),
            @Result(column = "lock_coin_id", property = "lockCoinId", jdbcType = JdbcType.INTEGER),
            @Result(column = "coin_type", property = "coinType", jdbcType = JdbcType.VARCHAR),
            @Result(column = "locked_coin", property = "lockedCoin", jdbcType = JdbcType.DECIMAL),
            @Result(column = "amount", property = "amount", jdbcType = JdbcType.DECIMAL),
            @Result(column = "release_type", property = "releaseType", jdbcType = JdbcType.TINYINT),
            @Result(column = "create_time", property = "createTime", jdbcType = JdbcType.TIMESTAMP)
    })
    LockCoinReleaseLog selectByPrimaryKey(Integer id);

    @UpdateProvider(type = LockCoinReleaseLogSqlProvider.class, method = "updateByPrimaryKeySelective")
    int updateByPrimaryKeySelective(LockCoinReleaseLog record);

    @Update({
            "update phone_lock_coin_release_log",
            "set user_id = #{userId,jdbcType=VARCHAR},",
            "lock_coin_id = #{lockCoinId,jdbcType=INTEGER},",
            "coin_type = #{coinType,jdbcType=VARCHAR},",
            "locked_coin = #{lockedCoin,jdbcType=DECIMAL},",
            "amount = #{amount,jdbcType=DECIMAL},",
            "release_type = #{releaseType,jdbcType=TINYINT},",
            "create_time = #{createTime,jdbcType=TIMESTAMP}",
            "where id = #{id,jdbcType=INTEGER}"
    })
    int updateByPrimaryKey(LockCoinReleaseLog record);
}