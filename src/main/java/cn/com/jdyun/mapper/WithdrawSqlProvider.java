package cn.com.jdyun.mapper;

import cn.com.jdyun.pojo.Withdraw;
import org.apache.ibatis.jdbc.SQL;

public class WithdrawSqlProvider {

    public String insertSelective(Withdraw record) {
        SQL sql = new SQL();
        sql.INSERT_INTO("phone_withdraw");

        if (record.getUserId() != null) {
            sql.VALUES("user_id", "#{userId,jdbcType=VARCHAR}");
        }

        if (record.getCoinAmount() != null) {
            sql.VALUES("coin_amount", "#{coinAmount,jdbcType=VARCHAR}");
        }

        if (record.getCoinType() != null) {
            sql.VALUES("coin_type", "#{coinType,jdbcType=VARCHAR}");
        }

        if (record.getFlag() != null) {
            sql.VALUES("flag", "#{flag,jdbcType=TINYINT}");
        }

        if (record.getCreateTime() != null) {
            sql.VALUES("create_time", "#{createTime,jdbcType=TIMESTAMP}");
        }

        if (record.getUpdateTime() != null) {
            sql.VALUES("update_time", "#{updateTime,jdbcType=TIMESTAMP}");
        }

        return sql.toString();
    }

    public String updateByPrimaryKeySelective(Withdraw record) {
        SQL sql = new SQL();
        sql.UPDATE("phone_withdraw");

        if (record.getUserId() != null) {
            sql.SET("user_id = #{userId,jdbcType=VARCHAR}");
        }

        if (record.getCoinAmount() != null) {
            sql.SET("coin_amount = #{coinAmount,jdbcType=VARCHAR}");
        }

        if (record.getCoinType() != null) {
            sql.SET("coin_type = #{coinType,jdbcType=VARCHAR}");
        }

        if (record.getFlag() != null) {
            sql.SET("flag = #{flag,jdbcType=TINYINT}");
        }

        if (record.getCreateTime() != null) {
            sql.SET("create_time = #{createTime,jdbcType=TIMESTAMP}");
        }

        if (record.getUpdateTime() != null) {
            sql.SET("update_time = #{updateTime,jdbcType=TIMESTAMP}");
        }

        sql.WHERE("id = #{id,jdbcType=INTEGER}");

        return sql.toString();
    }
}