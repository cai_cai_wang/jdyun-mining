package cn.com.jdyun.mapper;

import cn.com.jdyun.pojo.RewardPoints;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.annotations.UpdateProvider;
import org.apache.ibatis.type.JdbcType;

public interface RewardPointsMapper {
    @Delete({
            "delete from phone_reward_points",
            "where id = #{id,jdbcType=INTEGER}"
    })
    int deleteByPrimaryKey(Integer id);

    @Insert({
            "insert into phone_reward_points (user_id, coin_amount, ",
            "coin_type, times, ",
            "leaves, leaves_amount, ",
            "create_time, update_time)",
            "values (#{userId,jdbcType=VARCHAR}, #{coinAmount,jdbcType=VARCHAR}, ",
            "#{coinType,jdbcType=VARCHAR}, #{times,jdbcType=INTEGER}, ",
            "#{leaves,jdbcType=INTEGER}, #{leavesAmount,jdbcType=VARCHAR}, ",
            "#{createTime,jdbcType=TIMESTAMP}, #{updateTime,jdbcType=TIMESTAMP})"
    })
    @SelectKey(statement = "SELECT LAST_INSERT_ID()", keyProperty = "id", before = false, resultType = Integer.class)
    int insert(RewardPoints record);

    @InsertProvider(type = RewardPointsSqlProvider.class, method = "insertSelective")
    @SelectKey(statement = "SELECT LAST_INSERT_ID()", keyProperty = "id", before = false, resultType = Integer.class)
    int insertSelective(RewardPoints record);

    @Select({
            "select",
            "id, user_id, coin_amount, coin_type, times, leaves, leaves_amount, create_time, ",
            "update_time",
            "from phone_reward_points",
            "where id = #{id,jdbcType=INTEGER}"
    })
    @Results({
            @Result(column = "id", property = "id", jdbcType = JdbcType.INTEGER, id = true),
            @Result(column = "user_id", property = "userId", jdbcType = JdbcType.VARCHAR),
            @Result(column = "coin_amount", property = "coinAmount", jdbcType = JdbcType.VARCHAR),
            @Result(column = "coin_type", property = "coinType", jdbcType = JdbcType.VARCHAR),
            @Result(column = "times", property = "times", jdbcType = JdbcType.INTEGER),
            @Result(column = "leaves", property = "leaves", jdbcType = JdbcType.INTEGER),
            @Result(column = "leaves_amount", property = "leavesAmount", jdbcType = JdbcType.VARCHAR),
            @Result(column = "create_time", property = "createTime", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "update_time", property = "updateTime", jdbcType = JdbcType.TIMESTAMP)
    })
    RewardPoints selectByPrimaryKey(Integer id);

    @UpdateProvider(type = RewardPointsSqlProvider.class, method = "updateByPrimaryKeySelective")
    int updateByPrimaryKeySelective(RewardPoints record);

    @Update({
            "update phone_reward_points",
            "set user_id = #{userId,jdbcType=VARCHAR},",
            "coin_amount = #{coinAmount,jdbcType=VARCHAR},",
            "coin_type = #{coinType,jdbcType=VARCHAR},",
            "times = #{times,jdbcType=INTEGER},",
            "leaves = #{leaves,jdbcType=INTEGER},",
            "leaves_amount = #{leavesAmount,jdbcType=VARCHAR},",
            "create_time = #{createTime,jdbcType=TIMESTAMP},",
            "update_time = #{updateTime,jdbcType=TIMESTAMP}",
            "where id = #{id,jdbcType=INTEGER}"
    })
    int updateByPrimaryKey(RewardPoints record);

    @Select({
            "select",
            "id, user_id, coin_amount, coin_type, times, leaves, leaves_amount, create_time, ",
            "update_time",
            "from phone_reward_points",
            "where user_id = #{userId,jdbcType=INTEGER} and coin_type = #{coinType,jdbcType=INTEGER}"
    })
    @Results({
            @Result(column = "id", property = "id", jdbcType = JdbcType.INTEGER, id = true),
            @Result(column = "user_id", property = "userId", jdbcType = JdbcType.VARCHAR),
            @Result(column = "coin_amount", property = "coinAmount", jdbcType = JdbcType.VARCHAR),
            @Result(column = "coin_type", property = "coinType", jdbcType = JdbcType.VARCHAR),
            @Result(column = "times", property = "times", jdbcType = JdbcType.INTEGER),
            @Result(column = "leaves", property = "leaves", jdbcType = JdbcType.INTEGER),
            @Result(column = "leaves_amount", property = "leavesAmount", jdbcType = JdbcType.VARCHAR),
            @Result(column = "create_time", property = "createTime", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "update_time", property = "updateTime", jdbcType = JdbcType.TIMESTAMP)
    })
    RewardPoints queryUserRewardPoints(@Param("userId") String userId, @Param("coinType") String coinType);
}