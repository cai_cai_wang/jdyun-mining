package cn.com.jdyun.mapper;

import cn.com.jdyun.pojo.UserCandy;
import org.apache.ibatis.jdbc.SQL;

public class UserCandySqlProvider {

    public String insertSelective(UserCandy record) {
        SQL sql = new SQL();
        sql.INSERT_INTO("phone_user_candy");

        if (record.getUserId() != null) {
            sql.VALUES("user_id", "#{userId,jdbcType=VARCHAR}");
        }

        if (record.getRewardAll() != null) {
            sql.VALUES("reward_all", "#{rewardAll,jdbcType=DECIMAL}");
        }

        if (record.getRewardInviteAll() != null) {
            sql.VALUES("reward_invite_all", "#{rewardInviteAll,jdbcType=DECIMAL}");
        }

        if (record.getFreeAll() != null) {
            sql.VALUES("free_all", "#{freeAll,jdbcType=DECIMAL}");
        }

        if (record.getFreeInviteAll() != null) {
            sql.VALUES("free_invite_all", "#{freeInviteAll,jdbcType=DECIMAL}");
        }

        if (record.getCreateTime() != null) {
            sql.VALUES("create_time", "#{createTime,jdbcType=TIMESTAMP}");
        }

        if (record.getUpdateTime() != null) {
            sql.VALUES("update_time", "#{updateTime,jdbcType=TIMESTAMP}");
        }

        return sql.toString();
    }

    public String updateByPrimaryKeySelective(UserCandy record) {
        SQL sql = new SQL();
        sql.UPDATE("phone_user_candy");

        if (record.getUserId() != null) {
            sql.SET("user_id = #{userId,jdbcType=VARCHAR}");
        }

        if (record.getRewardAll() != null) {
            sql.SET("reward_all = #{rewardAll,jdbcType=DECIMAL}");
        }

        if (record.getRewardInviteAll() != null) {
            sql.SET("reward_invite_all = #{rewardInviteAll,jdbcType=DECIMAL}");
        }

        if (record.getFreeAll() != null) {
            sql.SET("free_all = #{freeAll,jdbcType=DECIMAL}");
        }

        if (record.getFreeInviteAll() != null) {
            sql.SET("free_invite_all = #{freeInviteAll,jdbcType=DECIMAL}");
        }

        if (record.getCreateTime() != null) {
            sql.SET("create_time = #{createTime,jdbcType=TIMESTAMP}");
        }

        if (record.getUpdateTime() != null) {
            sql.SET("update_time = #{updateTime,jdbcType=TIMESTAMP}");
        }

        sql.WHERE("id = #{id,jdbcType=INTEGER}");

        return sql.toString();
    }
}