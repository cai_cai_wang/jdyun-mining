package cn.com.jdyun.mapper;

import cn.com.jdyun.pojo.LockCoin;
import com.github.pagehelper.Page;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.annotations.UpdateProvider;
import org.apache.ibatis.type.JdbcType;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author yzping
 * @date Created in 下午 1:18 2018/8/26 0026
 */
public interface LockCoinMapper {

    @Delete({
            "delete from phone_lock_coin ",
            "where coin_type=#{coinType} and lock_coin_amount = #{lockCoinAmount} "
    })
    int deleteLockCoin(@Param("coinType") String coinType, @Param("lockCoinAmount") BigDecimal lockCoinAmount);

    @Insert({
            "insert into phone_lock_coin (user_id, init_lock_coin_amount, ",
            "lock_coin_amount, income, ",
            "coin_type, sequence_id, ",
            "create_time, update_time)",
            "values (#{userId,jdbcType=VARCHAR}, #{initLockCoinAmount,jdbcType=DECIMAL}, ",
            "#{lockCoinAmount,jdbcType=DECIMAL}, #{income,jdbcType=DECIMAL}, ",
            "#{coinType,jdbcType=VARCHAR}, #{sequenceId,jdbcType=VARCHAR}, ",
            "#{createTime,jdbcType=TIMESTAMP}, #{updateTime,jdbcType=TIMESTAMP})"
    })
    @SelectKey(statement = "SELECT LAST_INSERT_ID()", keyProperty = "id", before = false, resultType = Integer.class)
    int insert(LockCoin record);
    //新增一条SSP锁仓数量变化的记录
    @Insert({
            "insert into phone_lock_coin (user_id, init_lock_coin_amount, lock_coin_amount, ",
            "income,coin_type, sequence_id, ",
            "create_time, update_time)",
            "values (#{userId,jdbcType=VARCHAR}, #{initLockCoinAmount,jdbcType=DECIMAL}, #{lockCoinAmount,jdbcType=DECIMAL},#{income,jdbcType=DECIMAL}, ",
            "#{coinType,jdbcType=VARCHAR}, ",
            "#{sequenceId,jdbcType=VARCHAR}, ",
            "#{createTime,jdbcType=TIMESTAMP}, #{updateTime,jdbcType=TIMESTAMP})"
    })
    @SelectKey(statement = "SELECT LAST_INSERT_ID()", keyProperty = "id", before = false, resultType = Integer.class)
    int setNewLockCoinRecode(LockCoin lockCoinEntity);

    @InsertProvider(type = LockCoinSqlProvider.class, method = "insertSelective")
    @SelectKey(statement = "SELECT LAST_INSERT_ID()", keyProperty = "id", before = false, resultType = Integer.class)
    int insertSelective(LockCoin record);

    @Select({
            "select",
            "id, user_id, init_lock_coin_amount, lock_coin_amount, income, coin_type, sequence_id, ",
            "create_time, update_time",
            "from phone_lock_coin",
            "where id = #{id,jdbcType=INTEGER}"
    })
    @Results({
            @Result(column = "id", property = "id", jdbcType = JdbcType.INTEGER, id = true),
            @Result(column = "user_id", property = "userId", jdbcType = JdbcType.VARCHAR),
            @Result(column = "init_lock_coin_amount", property = "initLockCoinAmount", jdbcType = JdbcType.DECIMAL),
            @Result(column = "lock_coin_amount", property = "lockCoinAmount", jdbcType = JdbcType.DECIMAL),
            @Result(column = "income", property = "income", jdbcType = JdbcType.DECIMAL),
            @Result(column = "coin_type", property = "coinType", jdbcType = JdbcType.VARCHAR),
            @Result(column = "sequence_id", property = "sequenceId", jdbcType = JdbcType.VARCHAR),
            @Result(column = "create_time", property = "createTime", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "update_time", property = "updateTime", jdbcType = JdbcType.TIMESTAMP)
    })
    LockCoin selectByPrimaryKey(Integer id);

    @UpdateProvider(type = LockCoinSqlProvider.class, method = "updateByPrimaryKeySelective")
    int updateByPrimaryKeySelective(LockCoin record);

    @Update({
            "update phone_lock_coin",
            "set user_id = #{userId,jdbcType=VARCHAR},",
            "init_lock_coin_amount = #{initLockCoinAmount,jdbcType=DECIMAL},",
            "lock_coin_amount = #{lockCoinAmount,jdbcType=DECIMAL},",
            "income = #{income,jdbcType=DECIMAL},",
            "coin_type = #{coinType,jdbcType=VARCHAR},",
            "sequence_id = #{sequenceId,jdbcType=VARCHAR},",
            "create_time = #{createTime,jdbcType=TIMESTAMP},",
            "update_time = #{updateTime,jdbcType=TIMESTAMP}",
            "where id = #{id,jdbcType=INTEGER}"
    })
    int updateByPrimaryKey(LockCoin record);



    /*@Update({
            "update phone_lock_coin",
            "set update_time = #{updateTime,jdbcType=TIMESTAMP},",
            "lock_coin_amount = #{lockCoinAmount,jdbcType=DECIMAL}",
            "where user_id = #{userId,jdbcType=VARCHAR} "
    })
    int updateLockCoin(LockCoin lockCoinEntity);*/

    /*@Select({
            "select",
            "user_id, lock_coin_amount, coin_type, sequence_id, create_time",
            "from phone_lock_coin",
            "where user_id = #{userId,jdbcType=VARCHAR}"
    })
    @Results({
            @Result(column = "user_id", property = "userId", jdbcType = JdbcType.VARCHAR),
            @Result(column = "lock_coin_amount", property = "lockCoinAmount", jdbcType = JdbcType.DECIMAL),
            @Result(column = "coin_type", property = "coinType", jdbcType = JdbcType.VARCHAR),
            @Result(column = "sequence_id", property = "sequenceId", jdbcType = JdbcType.VARCHAR),
            @Result(column="create_time", property="createTime", jdbcType=JdbcType.TINYINT),
    })
    LockCoin selectLockCoin(String userId);*/

    @Select({
            "select",
            "id, user_id, init_lock_coin_amount, lock_coin_amount, income, coin_type, sequence_id, create_time, update_time",
            "from phone_lock_coin",
            "where coin_type = #{coinType,jdbcType=VARCHAR} AND lock_coin_amount>0 order by create_time "
    })
    @Results({
            @Result(column = "id", property = "id", jdbcType = JdbcType.INTEGER, id = true),
            @Result(column = "user_id", property = "userId", jdbcType = JdbcType.VARCHAR),
            @Result(column = "init_lock_coin_amount", property = "initLockCoinAmount", jdbcType = JdbcType.DECIMAL),
            @Result(column = "lock_coin_amount", property = "lockCoinAmount", jdbcType = JdbcType.DECIMAL),
            @Result(column = "income", property = "income", jdbcType = JdbcType.DECIMAL),
            @Result(column = "coin_type", property = "coinType", jdbcType = JdbcType.VARCHAR),
            @Result(column = "sequence_id", property = "sequenceId", jdbcType = JdbcType.VARCHAR),
            @Result(column = "create_time", property = "createTime", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "update_time", property = "updateTime", jdbcType = JdbcType.TIMESTAMP)
    })
    List<LockCoin> selectAllLockCoin(String coinType);

    @Select({
            "select",
            "id, user_id, init_lock_coin_amount, lock_coin_amount, income, coin_type, sequence_id, create_time, update_time",
            "from phone_lock_coin",
            "where lock_coin_amount > 0 and user_id = #{userId,jdbcType=VARCHAR} and coin_type = #{cointype,jdbcType=VARCHAR} order by create_time"
    })
    @Results({
            @Result(column = "id", property = "id", jdbcType = JdbcType.INTEGER, id = true),
            @Result(column = "user_id", property = "userId", jdbcType = JdbcType.VARCHAR),
            @Result(column = "init_lock_coin_amount", property = "initLockCoinAmount", jdbcType = JdbcType.DECIMAL),
            @Result(column = "lock_coin_amount", property = "lockCoinAmount", jdbcType = JdbcType.DECIMAL),
            @Result(column = "income", property = "income", jdbcType = JdbcType.DECIMAL),
            @Result(column = "coin_type", property = "coinType", jdbcType = JdbcType.VARCHAR),
            @Result(column = "sequence_id", property = "sequenceId", jdbcType = JdbcType.VARCHAR),
            @Result(column = "create_time", property = "createTime", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "update_time", property = "updateTime", jdbcType = JdbcType.TIMESTAMP)
    })
    List<LockCoin> selectUserLockCoin(@Param("userId") String userId, @Param("cointype") String cointype);

    @Select({
            "select",
            "id, user_id, init_lock_coin_amount, lock_coin_amount, income, coin_type, sequence_id, create_time, update_time",
            "from phone_lock_coin",
            "where lock_coin_amount > 0 and user_id = #{userId,jdbcType=VARCHAR} and coin_type = #{cointype,jdbcType=VARCHAR} order by create_time"
    })
    @Results({
            @Result(column = "id", property = "id", jdbcType = JdbcType.INTEGER, id = true),
            @Result(column = "user_id", property = "userId", jdbcType = JdbcType.VARCHAR),
            @Result(column = "init_lock_coin_amount", property = "initLockCoinAmount", jdbcType = JdbcType.DECIMAL),
            @Result(column = "lock_coin_amount", property = "lockCoinAmount", jdbcType = JdbcType.DECIMAL),
            @Result(column = "income", property = "income", jdbcType = JdbcType.DECIMAL),
            @Result(column = "coin_type", property = "coinType", jdbcType = JdbcType.VARCHAR),
            @Result(column = "sequence_id", property = "sequenceId", jdbcType = JdbcType.VARCHAR),
            @Result(column = "create_time", property = "createTime", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "update_time", property = "updateTime", jdbcType = JdbcType.TIMESTAMP)
    })
    List<LockCoin> selectUserLockCoin1(@Param("userId") String userId, @Param("cointype") String cointype); //注：这个方法不要加30天限制，如需查30天限制的，请使用：selectUserLockCoin

    @Select({
            "select",
            "id, user_id, init_lock_coin_amount, lock_coin_amount, income, coin_type, sequence_id, create_time, update_time",
            "from phone_lock_coin",
            "where user_id = #{userId,jdbcType=VARCHAR} and coin_type = #{coinType,jdbcType=VARCHAR}"
    })
    @Results({
            @Result(column = "id", property = "id", jdbcType = JdbcType.INTEGER, id = true),
            @Result(column = "user_id", property = "userId", jdbcType = JdbcType.VARCHAR),
            @Result(column = "init_lock_coin_amount", property = "initLockCoinAmount", jdbcType = JdbcType.DECIMAL),
            @Result(column = "lock_coin_amount", property = "lockCoinAmount", jdbcType = JdbcType.DECIMAL),
            @Result(column = "income", property = "income", jdbcType = JdbcType.DECIMAL),
            @Result(column = "coin_type", property = "coinType", jdbcType = JdbcType.VARCHAR),
            @Result(column = "sequence_id", property = "sequenceId", jdbcType = JdbcType.VARCHAR),
            @Result(column = "create_time", property = "createTime", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "update_time", property = "updateTime", jdbcType = JdbcType.TIMESTAMP)
    })
    Page<LockCoin> pageData(@Param("userId") String userId, @Param("coinType") String coinType);

    @Select({
            "select",
            "id, user_id, init_lock_coin_amount, lock_coin_amount, income, coin_type, sequence_id, create_time, update_time",
            "from phone_lock_coin",
            "where lock_coin_amount > 0 and coin_type = #{cointype,jdbcType=VARCHAR}  order by create_time"
    })
    @Results({
            @Result(column = "id", property = "id", jdbcType = JdbcType.INTEGER, id = true),
            @Result(column = "user_id", property = "userId", jdbcType = JdbcType.VARCHAR),
            @Result(column = "init_lock_coin_amount", property = "initLockCoinAmount", jdbcType = JdbcType.DECIMAL),
            @Result(column = "lock_coin_amount", property = "lockCoinAmount", jdbcType = JdbcType.DECIMAL),
            @Result(column = "income", property = "income", jdbcType = JdbcType.DECIMAL),
            @Result(column = "coin_type", property = "coinType", jdbcType = JdbcType.VARCHAR),
            @Result(column = "sequence_id", property = "sequenceId", jdbcType = JdbcType.VARCHAR),
            @Result(column = "create_time", property = "createTime", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "update_time", property = "updateTime", jdbcType = JdbcType.TIMESTAMP)
    })
    List<LockCoin> selectAllEnableUnLockCoin(@Param("coinType")String coinType);
    
    @Select({
        "select",
        "id, user_id, init_lock_coin_amount, lock_coin_amount, income, coin_type, sequence_id, create_time, update_time",
        "from phone_lock_coin",
        "where user_id = #{userId,jdbcType=VARCHAR} and lock_coin_amount > 0 and coin_type = #{coinType,jdbcType=VARCHAR} order by create_time"
	})
	@Results({
	        @Result(column = "id", property = "id", jdbcType = JdbcType.INTEGER, id = true),
	        @Result(column = "user_id", property = "userId", jdbcType = JdbcType.VARCHAR),
	        @Result(column = "init_lock_coin_amount", property = "initLockCoinAmount", jdbcType = JdbcType.DECIMAL),
	        @Result(column = "lock_coin_amount", property = "lockCoinAmount", jdbcType = JdbcType.DECIMAL),
	        @Result(column = "income", property = "income", jdbcType = JdbcType.DECIMAL),
	        @Result(column = "coin_type", property = "coinType", jdbcType = JdbcType.VARCHAR),
	        @Result(column = "sequence_id", property = "sequenceId", jdbcType = JdbcType.VARCHAR),
	        @Result(column = "create_time", property = "createTime", jdbcType = JdbcType.TIMESTAMP),
	        @Result(column = "update_time", property = "updateTime", jdbcType = JdbcType.TIMESTAMP)
	})
    List<LockCoin> queryUserEnableUnlockedCoin(@Param("userId") String userId, @Param("coinType") String coinType);
	
	@Select({
		"select",
		"locked_coin",
		"from phone_user_wallet",
		"where user_id = #{userId,jdbcType=VARCHAR} and coin_type = #{coinType,jdbcType=VARCHAR}"
	})
	BigDecimal queryUserEnableUnlockedAmount(@Param("userId") String userId, @Param("coinType") String coinType);
}
