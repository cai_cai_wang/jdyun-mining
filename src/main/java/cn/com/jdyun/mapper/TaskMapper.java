package cn.com.jdyun.mapper;

import cn.com.jdyun.pojo.Task;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.annotations.UpdateProvider;
import org.apache.ibatis.type.JdbcType;

public interface TaskMapper {
    @Delete({
            "delete from phone_task",
            "where id = #{id,jdbcType=INTEGER}"
    })
    int deleteByPrimaryKey(Integer id);

    @Insert({
            "insert into phone_task (user_id, date, ",
            "reward, create_time)",
            "values (#{userId,jdbcType=VARCHAR}, #{date,jdbcType=VARCHAR}, ",
            "#{reward,jdbcType=VARCHAR}, #{createTime,jdbcType=TIMESTAMP})"
    })
    @SelectKey(statement = "SELECT LAST_INSERT_ID()", keyProperty = "id", before = false, resultType = Integer.class)
    int insert(Task record);

    @InsertProvider(type = TaskSqlProvider.class, method = "insertSelective")
    @SelectKey(statement = "SELECT LAST_INSERT_ID()", keyProperty = "id", before = false, resultType = Integer.class)
    int insertSelective(Task record);

    @Select({
            "select",
            "id, user_id, date, reward, create_time",
            "from phone_task",
            "where id = #{id,jdbcType=INTEGER}"
    })
    @Results({
            @Result(column = "id", property = "id", jdbcType = JdbcType.INTEGER, id = true),
            @Result(column = "user_id", property = "userId", jdbcType = JdbcType.VARCHAR),
            @Result(column = "date", property = "date", jdbcType = JdbcType.VARCHAR),
            @Result(column = "reward", property = "reward", jdbcType = JdbcType.VARCHAR),
            @Result(column = "create_time", property = "createTime", jdbcType = JdbcType.TIMESTAMP)
    })
    Task selectByPrimaryKey(Integer id);

    @UpdateProvider(type = TaskSqlProvider.class, method = "updateByPrimaryKeySelective")
    int updateByPrimaryKeySelective(Task record);

    @Update({
            "update phone_task",
            "set user_id = #{userId,jdbcType=VARCHAR},",
            "date = #{date,jdbcType=VARCHAR},",
            "reward = #{reward,jdbcType=VARCHAR},",
            "create_time = #{createTime,jdbcType=TIMESTAMP}",
            "where id = #{id,jdbcType=INTEGER}"
    })
    int updateByPrimaryKey(Task record);
}