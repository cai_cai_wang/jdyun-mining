package cn.com.jdyun.mapper;

import cn.com.jdyun.pojo.Referee;
import org.apache.ibatis.jdbc.SQL;

public class RefereeSqlProvider {

    public String insertSelective(Referee record) {
        SQL sql = new SQL();
        sql.INSERT_INTO("phone_referee");

        if (record.getUserId() != null) {
            sql.VALUES("user_id", "#{userId,jdbcType=VARCHAR}");
        }

        if (record.getRefereeId() != null) {
            sql.VALUES("referee_id", "#{refereeId,jdbcType=VARCHAR}");
        }

        if (record.getRefereeParentId() != null) {
            sql.VALUES("referee_parent_id", "#{refereeParentId,jdbcType=VARCHAR}");
        }

        if (record.getContext() != null) {
            sql.VALUES("context", "#{context,jdbcType=VARCHAR}");
        }

        if (record.getCreateTime() != null) {
            sql.VALUES("create_time", "#{createTime,jdbcType=TIMESTAMP}");
        }

        if (record.getUpdateTime() != null) {
            sql.VALUES("update_time", "#{updateTime,jdbcType=TIMESTAMP}");
        }

        return sql.toString();
    }

    public String updateByPrimaryKeySelective(Referee record) {
        SQL sql = new SQL();
        sql.UPDATE("phone_referee");

        if (record.getUserId() != null) {
            sql.SET("user_id = #{userId,jdbcType=VARCHAR}");
        }

        if (record.getRefereeId() != null) {
            sql.SET("referee_id = #{refereeId,jdbcType=VARCHAR}");
        }

        if (record.getRefereeParentId() != null) {
            sql.SET("referee_parent_id = #{refereeParentId,jdbcType=VARCHAR}");
        }

        if (record.getContext() != null) {
            sql.SET("context = #{context,jdbcType=VARCHAR}");
        }

        if (record.getCreateTime() != null) {
            sql.SET("create_time = #{createTime,jdbcType=TIMESTAMP}");
        }

        if (record.getUpdateTime() != null) {
            sql.SET("update_time = #{updateTime,jdbcType=TIMESTAMP}");
        }

        sql.WHERE("id = #{id,jdbcType=INTEGER}");

        return sql.toString();
    }
}