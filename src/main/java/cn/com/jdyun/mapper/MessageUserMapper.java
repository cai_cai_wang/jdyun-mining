package cn.com.jdyun.mapper;

import cn.com.jdyun.pojo.MessageUser;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.annotations.UpdateProvider;
import org.apache.ibatis.type.JdbcType;

import java.util.Date;

public interface MessageUserMapper {
    @Delete({
            "delete from phone_message_user",
            "where id = #{id,jdbcType=INTEGER}"
    })
    int deleteByPrimaryKey(Integer id);

    @Insert({
            "insert into phone_message_user (user_id, message_id, ",
            "flag, create_time, ",
            "update_time)",
            "values (#{userId,jdbcType=VARCHAR}, #{messageId,jdbcType=INTEGER}, ",
            "#{flag,jdbcType=TINYINT}, #{createTime,jdbcType=TIMESTAMP}, ",
            "#{updateTime,jdbcType=TIMESTAMP})"
    })
    @SelectKey(statement = "SELECT LAST_INSERT_ID()", keyProperty = "id", before = false, resultType = Integer.class)
    int insert(MessageUser record);

    @InsertProvider(type = MessageUserSqlProvider.class, method = "insertSelective")
    @SelectKey(statement = "SELECT LAST_INSERT_ID()", keyProperty = "id", before = false, resultType = Integer.class)
    int insertSelective(MessageUser record);

    @Select({
            "select",
            "id, user_id, message_id, flag, create_time, update_time",
            "from phone_message_user",
            "where id = #{id,jdbcType=INTEGER}"
    })
    @Results({
            @Result(column = "id", property = "id", jdbcType = JdbcType.INTEGER, id = true),
            @Result(column = "user_id", property = "userId", jdbcType = JdbcType.VARCHAR),
            @Result(column = "message_id", property = "messageId", jdbcType = JdbcType.INTEGER),
            @Result(column = "flag", property = "flag", jdbcType = JdbcType.TINYINT),
            @Result(column = "create_time", property = "createTime", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "update_time", property = "updateTime", jdbcType = JdbcType.TIMESTAMP)
    })
    MessageUser selectByPrimaryKey(Integer id);

    @UpdateProvider(type = MessageUserSqlProvider.class, method = "updateByPrimaryKeySelective")
    int updateByPrimaryKeySelective(MessageUser record);

    @Update({
            "update phone_message_user",
            "set user_id = #{userId,jdbcType=VARCHAR},",
            "message_id = #{messageId,jdbcType=INTEGER},",
            "flag = #{flag,jdbcType=TINYINT},",
            "create_time = #{createTime,jdbcType=TIMESTAMP},",
            "update_time = #{updateTime,jdbcType=TIMESTAMP}",
            "where id = #{id,jdbcType=INTEGER}"
    })
    int updateByPrimaryKey(MessageUser record);

    @Update({
            "update phone_message_user",
            "set flag = #{flag,jdbcType=TINYINT},",
            "update_time = #{updateTime,jdbcType=TIMESTAMP}",
            "where user_id = #{userId,jdbcType=VARCHAR} and message_id = #{messageId,jdbcType=VARCHAR} and flag != 2"
    })
    void updateFlag(@Param("userId") String userId, @Param("messageId") String messageId, @Param("flag") Byte flag, @Param("updateTime") Date updateTime);

    @Update({
            "update phone_message_user",
            "set flag = #{flag,jdbcType=TINYINT},",
            "update_time = #{updateTime,jdbcType=TIMESTAMP}",
            "where user_id = #{userId,jdbcType=VARCHAR} and flag != 2"
    })
    void updateUserAllFlag(@Param("userId") String userId, @Param("flag") Byte flag, @Param("updateTime") Date updateTime);

}