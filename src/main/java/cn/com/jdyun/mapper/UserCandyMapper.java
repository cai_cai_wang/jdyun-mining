package cn.com.jdyun.mapper;

import cn.com.jdyun.pojo.UserCandy;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.annotations.UpdateProvider;
import org.apache.ibatis.type.JdbcType;

public interface UserCandyMapper {
    @Delete({
            "delete from phone_user_candy",
            "where id = #{id,jdbcType=INTEGER}"
    })
    int deleteByPrimaryKey(Integer id);

    @Insert({
            "insert into phone_user_candy (user_id, reward_all, ",
            "reward_invite_all, free_all, ",
            "free_invite_all, create_time, ",
            "update_time)",
            "values (#{userId,jdbcType=VARCHAR}, #{rewardAll,jdbcType=DECIMAL}, ",
            "#{rewardInviteAll,jdbcType=DECIMAL}, #{freeAll,jdbcType=DECIMAL}, ",
            "#{freeInviteAll,jdbcType=DECIMAL}, #{createTime,jdbcType=TIMESTAMP}, ",
            "#{updateTime,jdbcType=TIMESTAMP})"
    })
    @SelectKey(statement = "SELECT LAST_INSERT_ID()", keyProperty = "id", before = false, resultType = Integer.class)
    int insert(UserCandy record);

    @InsertProvider(type = UserCandySqlProvider.class, method = "insertSelective")
    @SelectKey(statement = "SELECT LAST_INSERT_ID()", keyProperty = "id", before = false, resultType = Integer.class)
    int insertSelective(UserCandy record);

    @Select({
            "select",
            "id, user_id, reward_all, reward_invite_all, free_all, free_invite_all, create_time, ",
            "update_time",
            "from phone_user_candy",
            "where id = #{id,jdbcType=INTEGER}"
    })
    @Results({
            @Result(column = "id", property = "id", jdbcType = JdbcType.INTEGER, id = true),
            @Result(column = "user_id", property = "userId", jdbcType = JdbcType.VARCHAR),
            @Result(column = "reward_all", property = "rewardAll", jdbcType = JdbcType.DECIMAL),
            @Result(column = "reward_invite_all", property = "rewardInviteAll", jdbcType = JdbcType.DECIMAL),
            @Result(column = "free_all", property = "freeAll", jdbcType = JdbcType.DECIMAL),
            @Result(column = "free_invite_all", property = "freeInviteAll", jdbcType = JdbcType.DECIMAL),
            @Result(column = "create_time", property = "createTime", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "update_time", property = "updateTime", jdbcType = JdbcType.TIMESTAMP)
    })
    UserCandy selectByPrimaryKey(Integer id);

    @UpdateProvider(type = UserCandySqlProvider.class, method = "updateByPrimaryKeySelective")
    int updateByPrimaryKeySelective(UserCandy record);

    @Update({
            "update phone_user_candy",
            "set user_id = #{userId,jdbcType=VARCHAR},",
            "reward_all = #{rewardAll,jdbcType=DECIMAL},",
            "reward_invite_all = #{rewardInviteAll,jdbcType=DECIMAL},",
            "free_all = #{freeAll,jdbcType=DECIMAL},",
            "free_invite_all = #{freeInviteAll,jdbcType=DECIMAL},",
            "create_time = #{createTime,jdbcType=TIMESTAMP},",
            "update_time = #{updateTime,jdbcType=TIMESTAMP}",
            "where id = #{id,jdbcType=INTEGER}"
    })
    int updateByPrimaryKey(UserCandy record);

    @Select({
            "select",
            "id, user_id, reward_all, reward_invite_all, free_all, free_invite_all, create_time, ",
            "update_time",
            "from phone_user_candy",
            "where user_id = #{userId,jdbcType=INTEGER}"
    })
    @Results({
            @Result(column = "id", property = "id", jdbcType = JdbcType.INTEGER, id = true),
            @Result(column = "user_id", property = "userId", jdbcType = JdbcType.VARCHAR),
            @Result(column = "reward_all", property = "rewardAll", jdbcType = JdbcType.DECIMAL),
            @Result(column = "reward_invite_all", property = "rewardInviteAll", jdbcType = JdbcType.DECIMAL),
            @Result(column = "free_all", property = "freeAll", jdbcType = JdbcType.DECIMAL),
            @Result(column = "free_invite_all", property = "freeInviteAll", jdbcType = JdbcType.DECIMAL),
            @Result(column = "create_time", property = "createTime", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "update_time", property = "updateTime", jdbcType = JdbcType.TIMESTAMP)
    })
    UserCandy queryByUserId(@Param("userId") String userId);
}