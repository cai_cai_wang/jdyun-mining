package cn.com.jdyun.mapper;

import cn.com.jdyun.pojo.LockCoinReleaseLog;
import org.apache.ibatis.jdbc.SQL;

public class LockCoinReleaseLogSqlProvider {

    public String insertSelective(LockCoinReleaseLog record) {
        SQL sql = new SQL();
        sql.INSERT_INTO("phone_lock_coin_release_log");

        if (record.getUserId() != null) {
            sql.VALUES("user_id", "#{userId,jdbcType=VARCHAR}");
        }

        if (record.getLockCoinId() != null) {
            sql.VALUES("lock_coin_id", "#{lockCoinId,jdbcType=INTEGER}");
        }

        if (record.getCoinType() != null) {
            sql.VALUES("coin_type", "#{coinType,jdbcType=VARCHAR}");
        }

        if (record.getLockedCoin() != null) {
            sql.VALUES("locked_coin", "#{lockedCoin,jdbcType=DECIMAL}");
        }

        if (record.getAmount() != null) {
            sql.VALUES("amount", "#{amount,jdbcType=DECIMAL}");
        }

        if (record.getReleaseType() != null) {
            sql.VALUES("release_type", "#{releaseType,jdbcType=TINYINT}");
        }

        if (record.getCreateTime() != null) {
            sql.VALUES("create_time", "#{createTime,jdbcType=TIMESTAMP}");
        }

        return sql.toString();
    }

    public String updateByPrimaryKeySelective(LockCoinReleaseLog record) {
        SQL sql = new SQL();
        sql.UPDATE("phone_lock_coin_release_log");

        if (record.getUserId() != null) {
            sql.SET("user_id = #{userId,jdbcType=VARCHAR}");
        }

        if (record.getLockCoinId() != null) {
            sql.SET("lock_coin_id = #{lockCoinId,jdbcType=INTEGER}");
        }

        if (record.getCoinType() != null) {
            sql.SET("coin_type = #{coinType,jdbcType=VARCHAR}");
        }

        if (record.getLockedCoin() != null) {
            sql.SET("locked_coin = #{lockedCoin,jdbcType=DECIMAL}");
        }

        if (record.getAmount() != null) {
            sql.SET("amount = #{amount,jdbcType=DECIMAL}");
        }

        if (record.getReleaseType() != null) {
            sql.SET("release_type = #{releaseType,jdbcType=TINYINT}");
        }

        if (record.getCreateTime() != null) {
            sql.SET("create_time = #{createTime,jdbcType=TIMESTAMP}");
        }

        sql.WHERE("id = #{id,jdbcType=INTEGER}");

        return sql.toString();
    }
}