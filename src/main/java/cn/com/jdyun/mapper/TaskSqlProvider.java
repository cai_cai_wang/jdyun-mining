package cn.com.jdyun.mapper;

import cn.com.jdyun.pojo.Task;
import org.apache.ibatis.jdbc.SQL;

public class TaskSqlProvider {

    public String insertSelective(Task record) {
        SQL sql = new SQL();
        sql.INSERT_INTO("phone_task");

        if (record.getUserId() != null) {
            sql.VALUES("user_id", "#{userId,jdbcType=VARCHAR}");
        }

        if (record.getDate() != null) {
            sql.VALUES("date", "#{date,jdbcType=VARCHAR}");
        }

        if (record.getReward() != null) {
            sql.VALUES("reward", "#{reward,jdbcType=VARCHAR}");
        }

        if (record.getCreateTime() != null) {
            sql.VALUES("create_time", "#{createTime,jdbcType=TIMESTAMP}");
        }

        return sql.toString();
    }

    public String updateByPrimaryKeySelective(Task record) {
        SQL sql = new SQL();
        sql.UPDATE("phone_task");

        if (record.getUserId() != null) {
            sql.SET("user_id = #{userId,jdbcType=VARCHAR}");
        }

        if (record.getDate() != null) {
            sql.SET("date = #{date,jdbcType=VARCHAR}");
        }

        if (record.getReward() != null) {
            sql.SET("reward = #{reward,jdbcType=VARCHAR}");
        }

        if (record.getCreateTime() != null) {
            sql.SET("create_time = #{createTime,jdbcType=TIMESTAMP}");
        }

        sql.WHERE("id = #{id,jdbcType=INTEGER}");

        return sql.toString();
    }
}