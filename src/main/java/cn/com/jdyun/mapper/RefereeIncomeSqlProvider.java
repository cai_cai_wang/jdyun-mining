package cn.com.jdyun.mapper;

import cn.com.jdyun.pojo.RefereeIncome;
import org.apache.ibatis.jdbc.SQL;

public class RefereeIncomeSqlProvider {

    public String insertSelective(RefereeIncome record) {
        SQL sql = new SQL();
        sql.INSERT_INTO("phone_referee_income");

        if (record.getUserId() != null) {
            sql.VALUES("user_id", "#{userId,jdbcType=VARCHAR}");
        }

        if (record.getToBalance() != null) {
            sql.VALUES("to_balance", "#{toBalance,jdbcType=DECIMAL}");
        }

        if (record.getToLockedCoin() != null) {
            sql.VALUES("to_locked_coin", "#{toLockedCoin,jdbcType=DECIMAL}");
        }

        if (record.getDate() != null) {
            sql.VALUES("date", "#{date,jdbcType=VARCHAR}");
        }

        if (record.getCreateTime() != null) {
            sql.VALUES("create_time", "#{createTime,jdbcType=TIMESTAMP}");
        }

        if (record.getUpdateTime() != null) {
            sql.VALUES("update_time", "#{updateTime,jdbcType=TIMESTAMP}");
        }

        return sql.toString();
    }

    public String updateByPrimaryKeySelective(RefereeIncome record) {
        SQL sql = new SQL();
        sql.UPDATE("phone_referee_income");

        if (record.getUserId() != null) {
            sql.SET("user_id = #{userId,jdbcType=VARCHAR}");
        }

        if (record.getToBalance() != null) {
            sql.SET("to_balance = #{toBalance,jdbcType=DECIMAL}");
        }

        if (record.getToLockedCoin() != null) {
            sql.SET("to_locked_coin = #{toLockedCoin,jdbcType=DECIMAL}");
        }

        if (record.getDate() != null) {
            sql.SET("date = #{date,jdbcType=VARCHAR}");
        }

        if (record.getCreateTime() != null) {
            sql.SET("create_time = #{createTime,jdbcType=TIMESTAMP}");
        }

        if (record.getUpdateTime() != null) {
            sql.SET("update_time = #{updateTime,jdbcType=TIMESTAMP}");
        }

        sql.WHERE("id = #{id,jdbcType=INTEGER}");

        return sql.toString();
    }
}