package cn.com.jdyun.mapper;

import cn.com.jdyun.pojo.PoolLog;
import org.apache.ibatis.jdbc.SQL;

public class PoolLogSqlProvider {

    public String insertSelective(PoolLog record) {
        SQL sql = new SQL();
        sql.INSERT_INTO("phone_pool_log");

        if (record.getPoolBalance() != null) {
            sql.VALUES("pool_balance", "#{poolBalance,jdbcType=VARCHAR}");
        }

        if (record.getCoinAmount() != null) {
            sql.VALUES("coin_amount", "#{coinAmount,jdbcType=VARCHAR}");
        }

        if (record.getCoinType() != null) {
            sql.VALUES("coin_type", "#{coinType,jdbcType=VARCHAR}");
        }

        if (record.getUserId() != null) {
            sql.VALUES("user_id", "#{userId,jdbcType=VARCHAR}");
        }

        if (record.getMemo() != null) {
            sql.VALUES("memo", "#{memo,jdbcType=VARCHAR}");
        }

        if (record.getCreateTime() != null) {
            sql.VALUES("create_time", "#{createTime,jdbcType=TIMESTAMP}");
        }

        return sql.toString();
    }

    public String updateByPrimaryKeySelective(PoolLog record) {
        SQL sql = new SQL();
        sql.UPDATE("phone_pool_log");

        if (record.getPoolBalance() != null) {
            sql.SET("pool_balance = #{poolBalance,jdbcType=VARCHAR}");
        }

        if (record.getCoinAmount() != null) {
            sql.SET("coin_amount = #{coinAmount,jdbcType=VARCHAR}");
        }

        if (record.getCoinType() != null) {
            sql.SET("coin_type = #{coinType,jdbcType=VARCHAR}");
        }

        if (record.getUserId() != null) {
            sql.SET("user_id = #{userId,jdbcType=VARCHAR}");
        }

        if (record.getMemo() != null) {
            sql.SET("memo = #{memo,jdbcType=VARCHAR}");
        }

        if (record.getCreateTime() != null) {
            sql.SET("create_time = #{createTime,jdbcType=TIMESTAMP}");
        }

        sql.WHERE("id = #{id,jdbcType=INTEGER}");

        return sql.toString();
    }
}