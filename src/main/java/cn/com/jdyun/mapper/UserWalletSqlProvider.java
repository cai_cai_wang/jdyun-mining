package cn.com.jdyun.mapper;

import cn.com.jdyun.pojo.UserWallet;
import org.apache.ibatis.jdbc.SQL;

public class UserWalletSqlProvider {

    public String insertSelective(UserWallet record) {
        SQL sql = new SQL();
        sql.INSERT_INTO("phone_user_wallet");

        if (record.getUserId() != null) {
            sql.VALUES("user_id", "#{userId,jdbcType=VARCHAR}");
        }

        if (record.getCoinType() != null) {
            sql.VALUES("coin_type", "#{coinType,jdbcType=VARCHAR}");
        }

        if (record.getAsset() != null) {
            sql.VALUES("asset", "#{asset,jdbcType=VARCHAR}");
        }

        if (record.getBalance() != null) {
            sql.VALUES("balance", "#{balance,jdbcType=VARCHAR}");
        }

        if (record.getMingCoin() != null) {
            sql.VALUES("ming_coin", "#{mingCoin,jdbcType=VARCHAR}");
        }

        if (record.getTaskCoin() != null) {
            sql.VALUES("task_coin", "#{taskCoin,jdbcType=VARCHAR}");
        }

        if (record.getLockedCoin() != null) {
            sql.VALUES("locked_coin", "#{lockedCoin,jdbcType=DECIMAL}");
        }

        if (record.getFreezeCoin() != null) {
            sql.VALUES("freeze_coin", "#{freezeCoin,jdbcType=VARCHAR}");
        }

        if (record.getCreateTime() != null) {
            sql.VALUES("create_time", "#{createTime,jdbcType=TIMESTAMP}");
        }

        if (record.getUpdateTime() != null) {
            sql.VALUES("update_time", "#{updateTime,jdbcType=TIMESTAMP}");
        }

        return sql.toString();
    }

    public String updateByPrimaryKeySelective(UserWallet record) {
        SQL sql = new SQL();
        sql.UPDATE("phone_user_wallet");

        if (record.getUserId() != null) {
            sql.SET("user_id = #{userId,jdbcType=VARCHAR}");
        }

        if (record.getCoinType() != null) {
            sql.SET("coin_type = #{coinType,jdbcType=VARCHAR}");
        }

        if (record.getAsset() != null) {
            sql.SET("asset = #{asset,jdbcType=VARCHAR}");
        }

        if (record.getBalance() != null) {
            sql.SET("balance = #{balance,jdbcType=VARCHAR}");
        }

        if (record.getMingCoin() != null) {
            sql.SET("ming_coin = #{mingCoin,jdbcType=VARCHAR}");
        }

        if (record.getTaskCoin() != null) {
            sql.SET("task_coin = #{taskCoin,jdbcType=VARCHAR}");
        }

        if (record.getLockedCoin() != null) {
            sql.SET("locked_coin = #{lockedCoin,jdbcType=DECIMAL}");
        }

        if (record.getFreezeCoin() != null) {
            sql.SET("freeze_coin = #{freezeCoin,jdbcType=VARCHAR}");
        }

        if (record.getCreateTime() != null) {
            sql.SET("create_time = #{createTime,jdbcType=TIMESTAMP}");
        }

        if (record.getUpdateTime() != null) {
            sql.SET("update_time = #{updateTime,jdbcType=TIMESTAMP}");
        }

        sql.WHERE("id = #{id,jdbcType=INTEGER}");

        return sql.toString();
    }
}