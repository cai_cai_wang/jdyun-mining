package cn.com.jdyun.mapper;

import cn.com.jdyun.pojo.Message;
import com.github.pagehelper.Page;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.annotations.UpdateProvider;
import org.apache.ibatis.type.JdbcType;

import java.util.List;
import java.util.Map;

public interface MessageMapper {

    @Insert({
            "insert into phone_message (title, message, ",
            "amount, coin_type, ",
            "message_type, publisher, ",
            "flag,status, create_time, ",
            "update_time)",
            "values (#{title,jdbcType=VARCHAR}, #{message,jdbcType=VARCHAR}, ",
            "#{amount,jdbcType=VARCHAR}, #{coinType,jdbcType=VARCHAR}, ",
            "#{messageType,jdbcType=TINYINT}, #{publisher,jdbcType=VARCHAR}, ",
            "#{flag,jdbcType=TINYINT},#{status,jdbcType=TINYINT}, #{createTime,jdbcType=TIMESTAMP}, ",
            "#{updateTime,jdbcType=TIMESTAMP})"
    })
    @SelectKey(statement = "SELECT LAST_INSERT_ID()", keyProperty = "id", before = false, resultType = Integer.class)
    int insert(Message record);


    @InsertProvider(type = MessageSqlProvider.class, method = "insertSelective")
    @SelectKey(statement = "SELECT LAST_INSERT_ID()", keyProperty = "id", before = false, resultType = Integer.class)
    int insertSelective(Message record);

    @Select({
            "select",
            "id, title, message, amount, coin_type, message_type, publisher, flag,status, create_time, ",
            "update_time",
            "from phone_message",
            "where id = #{id,jdbcType=INTEGER}"
    })
    @Results({
            @Result(column = "id", property = "id", jdbcType = JdbcType.INTEGER, id = true),
            @Result(column = "title", property = "title", jdbcType = JdbcType.VARCHAR),
            @Result(column = "message", property = "message", jdbcType = JdbcType.VARCHAR),
            @Result(column = "amount", property = "amount", jdbcType = JdbcType.VARCHAR),
            @Result(column = "coin_type", property = "coinType", jdbcType = JdbcType.VARCHAR),
            @Result(column = "message_type", property = "messageType", jdbcType = JdbcType.TINYINT),
            @Result(column = "publisher", property = "publisher", jdbcType = JdbcType.VARCHAR),
            @Result(column = "flag", property = "flag", jdbcType = JdbcType.TINYINT),
            @Result(column = "status", property = "status", jdbcType = JdbcType.TINYINT),
            @Result(column = "create_time", property = "createTime", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "update_time", property = "updateTime", jdbcType = JdbcType.TIMESTAMP)
    })
    Message selectByPrimaryKey(Integer id);

    @UpdateProvider(type = MessageSqlProvider.class, method = "updateByPrimaryKeySelective")
    int updateByPrimaryKeySelective(Message record);

    @Update({
            "update phone_message",
            "set title = #{title,jdbcType=VARCHAR},",
            "message = #{message,jdbcType=VARCHAR},",
            "amount = #{amount,jdbcType=VARCHAR},",
            "coin_type = #{coinType,jdbcType=VARCHAR},",
            "message_type = #{messageType,jdbcType=TINYINT},",
            "publisher = #{publisher,jdbcType=VARCHAR},",
            "flag = #{flag,jdbcType=TINYINT},",
            "status = #{status,jdbcType=TINYINT},",
            "create_time = #{createTime,jdbcType=TIMESTAMP},",
            "update_time = #{updateTime,jdbcType=TIMESTAMP}",
            "where id = #{id,jdbcType=INTEGER}"
    })
    int updateByPrimaryKey(Message record);

    @Select({
            "select",
            "id, title, message, amount, coin_type, message_type, publisher, flag,status, create_time, update_time",
            "from phone_message",
            "where id = #{id,jdbcType=INTEGER} and message_type = 0 ",
            "order by create_time desc"
    })
    @Results({
            @Result(column = "id", property = "id", jdbcType = JdbcType.INTEGER, id = true),
            @Result(column = "title", property = "title", jdbcType = JdbcType.VARCHAR),
            @Result(column = "message", property = "message", jdbcType = JdbcType.VARCHAR),
            @Result(column = "amount", property = "amount", jdbcType = JdbcType.VARCHAR),
            @Result(column = "coin_type", property = "coinType", jdbcType = JdbcType.VARCHAR),
            @Result(column = "message_type", property = "messageType", jdbcType = JdbcType.TINYINT),
            @Result(column = "publisher", property = "publisher", jdbcType = JdbcType.VARCHAR),
            @Result(column = "flag", property = "flag", jdbcType = JdbcType.TINYINT),
            @Result(column = "status", property = "status", jdbcType = JdbcType.TINYINT),
            @Result(column = "create_time", property = "createTime", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "update_time", property = "updateTime", jdbcType = JdbcType.TIMESTAMP)
    })
    List<Message> queryMessageList();

    @Select({
            "select",
            "pm.id, pm.title, pm.message, pm.amount, pm.coin_type, pm.message_type, pm.publisher, pm.flag,pm.status, pm.create_time, pm.update_time",
            "from phone_message pm",
            "left join phone_message_user pmu on pm.id = pmu.message_id",
            "where pmu.user_id = #{userId,jdbcType=VARCHAR} and pmu.flag = #{flag,jdbcType=TINYINT} and pmu.flag != 2",
            "order by create_time desc"
    })
    @Results({
            @Result(column = "id", property = "id", jdbcType = JdbcType.INTEGER, id = true),
            @Result(column = "title", property = "title", jdbcType = JdbcType.VARCHAR),
            @Result(column = "message", property = "message", jdbcType = JdbcType.VARCHAR),
            @Result(column = "amount", property = "amount", jdbcType = JdbcType.VARCHAR),
            @Result(column = "coin_type", property = "coinType", jdbcType = JdbcType.VARCHAR),
            @Result(column = "message_type", property = "messageType", jdbcType = JdbcType.TINYINT),
            @Result(column = "publisher", property = "publisher", jdbcType = JdbcType.VARCHAR),
            @Result(column = "flag", property = "flag", jdbcType = JdbcType.TINYINT),
            @Result(column = "status", property = "status", jdbcType = JdbcType.TINYINT),
            @Result(column = "create_time", property = "createTime", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "update_time", property = "updateTime", jdbcType = JdbcType.TIMESTAMP)
    })
    Page<Message> pageData(@Param("userId") String userId, @Param("flag") Byte flag);

    @Results({
            @Result(column = "id", property = "id", jdbcType = JdbcType.INTEGER, id = true),
            @Result(column = "title", property = "title", jdbcType = JdbcType.VARCHAR),
            @Result(column = "message", property = "message", jdbcType = JdbcType.VARCHAR),
            @Result(column = "amount", property = "amount", jdbcType = JdbcType.VARCHAR),
            @Result(column = "coin_type", property = "coinType", jdbcType = JdbcType.VARCHAR),
            @Result(column = "message_type", property = "messageType", jdbcType = JdbcType.TINYINT),
            @Result(column = "publisher", property = "publisher", jdbcType = JdbcType.VARCHAR),
            @Result(column = "flag", property = "flag", jdbcType = JdbcType.TINYINT),
            @Result(column = "status", property = "status", jdbcType = JdbcType.TINYINT),
            @Result(column = "create_time", property = "createTime", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "update_time", property = "updateTime", jdbcType = JdbcType.TIMESTAMP)
    })
    @SelectProvider(type = MessageSqlProvider.class, method = "pageManagerData")
    Page<Message> pageManagerData(Map<String, Object> condition);
}