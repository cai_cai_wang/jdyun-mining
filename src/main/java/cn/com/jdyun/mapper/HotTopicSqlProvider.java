package cn.com.jdyun.mapper;

import cn.com.jdyun.pojo.HotTopic;
import org.apache.ibatis.jdbc.SQL;

import java.util.Map;

public class HotTopicSqlProvider {

    public String insertSelective(HotTopic record) {
        SQL sql = new SQL();
        sql.INSERT_INTO("phone_hot_topic");

        if (record.getTitle() != null) {
            sql.VALUES("title", "#{title,jdbcType=VARCHAR}");
        }

        if (record.getContent() != null) {
            sql.VALUES("content", "#{content,jdbcType=VARCHAR}");
        }


        if (record.getShowOrder() != null) {
            sql.VALUES("show_order", "#{showOrder,jdbcType=INTEGER}");
        }

        if (record.getCreateTime() != null) {
            sql.VALUES("create_time", "#{createTime,jdbcType=TIMESTAMP}");
        }

        if (record.getUpdateTime() != null) {
            sql.VALUES("update_time", "#{updateTime,jdbcType=TIMESTAMP}");
        }

        return sql.toString();
    }

    public String updateByPrimaryKeySelective(HotTopic record) {
        SQL sql = new SQL();
        sql.UPDATE("phone_hot_topic");

        if (record.getTitle() != null) {
            sql.SET("title = #{title,jdbcType=VARCHAR}");
        }

        if (record.getContent() != null) {
            sql.SET("content = #{content,jdbcType=VARCHAR}");
        }


        if (record.getShowOrder() != null) {
            sql.SET("show_order = #{showOrder,jdbcType=INTEGER}");
        }
        if (record.getCreateTime() != null) {
            sql.SET("create_time = #{createTime,jdbcType=TIMESTAMP}");
        }

        if (record.getUpdateTime() != null) {
            sql.SET("update_time = #{updateTime,jdbcType=TIMESTAMP}");
        }

        sql.WHERE("id = #{id,jdbcType=INTEGER}");

        return sql.toString();
    }

    public String pageData(Map<String, Object> condition) {
        SQL sql = new SQL();
        sql.SELECT("id, title, content, date, show_order, flag, create_time, update_time");
        sql.FROM("phone_hot_topic");

        StringBuffer where = new StringBuffer(" 1 = 1");
        if (condition != null) {
            if (condition.containsKey("title") && condition.get("title") != null) {
                where.append(" and title like concat('%',#{title,jdbcType=VARCHAR},'%')");
            }
            if (condition.containsKey("date") && condition.get("date") != null) {
                where.append(" and date = #{date,jdbcType=VARCHAR}");
            }
            if (condition.containsKey("flag") && condition.get("flag") != null) {
                where.append(" and flag = #{flag,jdbcType=TINYINT}");
            }
            if (condition.containsKey("dateStart") && condition.get("dateStart") != null) {
                where.append(" and create_time >= #{dateStart,jdbcType=TIMESTAMP}");
            }
            if (condition.containsKey("dateEnd") && condition.get("dateEnd") != null) {
                where.append(" and create_time < #{dateEnd,jdbcType=TIMESTAMP}");
            }
        }
        sql.WHERE(where.toString());
        sql.ORDER_BY("create_time desc");
        return sql.toString();
    }
}