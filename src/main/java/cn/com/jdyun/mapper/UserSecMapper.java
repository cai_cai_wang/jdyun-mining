package cn.com.jdyun.mapper;

import cn.com.jdyun.pojo.UserSec;
import com.github.pagehelper.Page;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.annotations.UpdateProvider;
import org.apache.ibatis.type.JdbcType;

import java.util.Map;

public interface UserSecMapper {
    @Delete({
            "delete from phone_user_sec",
            "where id = #{id,jdbcType=INTEGER}"
    })
    int deleteByPrimaryKey(Integer id);

    @Insert({
            "insert into phone_user_sec (user_id, sec_password, ",
            "id_card_no, id_card_name, ",
            "id_card_up, id_card_down, ",
            "flag, create_time, ",
            "update_time)",
            "values (#{userId,jdbcType=VARCHAR}, #{secPassword,jdbcType=VARCHAR}, ",
            "#{idCardNo,jdbcType=VARCHAR}, #{idCardName,jdbcType=VARCHAR}, ",
            "#{idCardUp,jdbcType=VARCHAR}, #{idCardDown,jdbcType=VARCHAR}, ",
            "#{flag,jdbcType=TINYINT}, #{createTime,jdbcType=TIMESTAMP}, ",
            "#{updateTime,jdbcType=TIMESTAMP})"
    })
    @SelectKey(statement = "SELECT LAST_INSERT_ID()", keyProperty = "id", before = false, resultType = Integer.class)
    int insert(UserSec record);

    @InsertProvider(type = UserSecSqlProvider.class, method = "insertSelective")
    @SelectKey(statement = "SELECT LAST_INSERT_ID()", keyProperty = "id", before = false, resultType = Integer.class)
    int insertSelective(UserSec record);

    @Select({
            "select",
            "id, user_id, sec_password, id_card_no, id_card_name, id_card_up, id_card_down, ",
            "flag, create_time, update_time",
            "from phone_user_sec",
            "where id = #{id,jdbcType=INTEGER}"
    })
    @Results({
            @Result(column = "id", property = "id", jdbcType = JdbcType.INTEGER, id = true),
            @Result(column = "user_id", property = "userId", jdbcType = JdbcType.VARCHAR),
            @Result(column = "sec_password", property = "secPassword", jdbcType = JdbcType.VARCHAR),
            @Result(column = "id_card_no", property = "idCardNo", jdbcType = JdbcType.VARCHAR),
            @Result(column = "id_card_name", property = "idCardName", jdbcType = JdbcType.VARCHAR),
            @Result(column = "id_card_up", property = "idCardUp", jdbcType = JdbcType.VARCHAR),
            @Result(column = "id_card_down", property = "idCardDown", jdbcType = JdbcType.VARCHAR),
            @Result(column = "flag", property = "flag", jdbcType = JdbcType.TINYINT),
            @Result(column = "create_time", property = "createTime", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "update_time", property = "updateTime", jdbcType = JdbcType.TIMESTAMP)
    })
    UserSec selectByPrimaryKey(Integer id);

    @UpdateProvider(type = UserSecSqlProvider.class, method = "updateByPrimaryKeySelective")
    int updateByPrimaryKeySelective(UserSec record);

    @Update({
            "update phone_user_sec",
            "set user_id = #{userId,jdbcType=VARCHAR},",
            "sec_password = #{secPassword,jdbcType=VARCHAR},",
            "id_card_no = #{idCardNo,jdbcType=VARCHAR},",
            "id_card_name = #{idCardName,jdbcType=VARCHAR},",
            "id_card_up = #{idCardUp,jdbcType=VARCHAR},",
            "id_card_down = #{idCardDown,jdbcType=VARCHAR},",
            "flag = #{flag,jdbcType=TINYINT},",
            "create_time = #{createTime,jdbcType=TIMESTAMP},",
            "update_time = #{updateTime,jdbcType=TIMESTAMP}",
            "where id = #{id,jdbcType=INTEGER}"
    })
    int updateByPrimaryKey(UserSec record);

    @Select({
            "select",
            "id, user_id, id_card_no, id_card_name, id_card_up, id_card_down, ",
            "flag, sec_password,create_time, update_time",
            "from phone_user_sec",
            "where user_id = #{userId,jdbcType=VARCHAR}"
    })
    @Results({
            @Result(column = "id", property = "id", jdbcType = JdbcType.INTEGER, id = true),
            @Result(column = "user_id", property = "userId", jdbcType = JdbcType.VARCHAR),
            @Result(column = "id_card_no", property = "idCardNo", jdbcType = JdbcType.VARCHAR),
            @Result(column = "id_card_name", property = "idCardName", jdbcType = JdbcType.VARCHAR),
            @Result(column = "id_card_up", property = "idCardUp", jdbcType = JdbcType.VARCHAR),
            @Result(column = "id_card_down", property = "idCardDown", jdbcType = JdbcType.VARCHAR),
            @Result(column = "flag", property = "flag", jdbcType = JdbcType.TINYINT),
            @Result(column = "sec_password", property = "secPassword", jdbcType = JdbcType.TINYINT),
            @Result(column = "create_time", property = "createTime", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "update_time", property = "updateTime", jdbcType = JdbcType.TIMESTAMP)
    })
    UserSec queryByUserId(@Param("userId") String userId);


    @Select({
            "select",
            "sec_password",
            "from phone_user_sec",
            "where user_id = #{userId,jdbcType=VARCHAR}"
    })
    String selectSecPwd(@Param("userId") String userId);

    @Select({
            "select",
            "count(id) c",
            "from phone_user_sec",
            "where user_id = #{userId,jdbcType=VARCHAR} and flag = 2"
    })
    boolean checkCertification(String userId);

    @Results({
            @Result(column = "id", property = "id", jdbcType = JdbcType.INTEGER, id = true),
            @Result(column = "user_id", property = "userId", jdbcType = JdbcType.VARCHAR),
            @Result(column = "id_card_no", property = "idCardNo", jdbcType = JdbcType.VARCHAR),
            @Result(column = "id_card_name", property = "idCardName", jdbcType = JdbcType.VARCHAR),
            @Result(column = "id_card_up", property = "idCardUp", jdbcType = JdbcType.VARCHAR),
            @Result(column = "id_card_down", property = "idCardDown", jdbcType = JdbcType.VARCHAR),
            @Result(column = "flag", property = "flag", jdbcType = JdbcType.TINYINT),
            @Result(column = "create_time", property = "createTime", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "update_time", property = "updateTime", jdbcType = JdbcType.TIMESTAMP)
    })
    @SelectProvider(type = UserSecSqlProvider.class, method = "pageData")
    Page<UserSec> pageData(Map<String, Object> condition);
}