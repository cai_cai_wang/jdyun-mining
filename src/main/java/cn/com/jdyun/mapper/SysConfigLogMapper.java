package cn.com.jdyun.mapper;

import cn.com.jdyun.pojo.SysConfigLog;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.annotations.UpdateProvider;
import org.apache.ibatis.type.JdbcType;

public interface SysConfigLogMapper {
    @Delete({
            "delete from phone_sys_config_log",
            "where id = #{id,jdbcType=INTEGER}"
    })
    int deleteByPrimaryKey(Integer id);

    @Insert({
            "insert into phone_sys_config_log (code, old_value, ",
            "new_value, create_time, ",
            "operator)",
            "values (#{code,jdbcType=VARCHAR}, #{oldValue,jdbcType=VARCHAR}, ",
            "#{newValue,jdbcType=VARCHAR}, #{createTime,jdbcType=TIMESTAMP}, ",
            "#{operator,jdbcType=VARCHAR})"
    })
    @SelectKey(statement = "SELECT LAST_INSERT_ID()", keyProperty = "id", before = false, resultType = Integer.class)
    int insert(SysConfigLog record);

    @InsertProvider(type = SysConfigLogSqlProvider.class, method = "insertSelective")
    @SelectKey(statement = "SELECT LAST_INSERT_ID()", keyProperty = "id", before = false, resultType = Integer.class)
    int insertSelective(SysConfigLog record);

    @Select({
            "select",
            "id, code, old_value, new_value, create_time, operator",
            "from phone_sys_config_log",
            "where id = #{id,jdbcType=INTEGER}"
    })
    @Results({
            @Result(column = "id", property = "id", jdbcType = JdbcType.INTEGER, id = true),
            @Result(column = "code", property = "code", jdbcType = JdbcType.VARCHAR),
            @Result(column = "old_value", property = "oldValue", jdbcType = JdbcType.VARCHAR),
            @Result(column = "new_value", property = "newValue", jdbcType = JdbcType.VARCHAR),
            @Result(column = "create_time", property = "createTime", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "operator", property = "operator", jdbcType = JdbcType.VARCHAR)
    })
    SysConfigLog selectByPrimaryKey(Integer id);

    @UpdateProvider(type = SysConfigLogSqlProvider.class, method = "updateByPrimaryKeySelective")
    int updateByPrimaryKeySelective(SysConfigLog record);

    @Update({
            "update phone_sys_config_log",
            "set code = #{code,jdbcType=VARCHAR},",
            "old_value = #{oldValue,jdbcType=VARCHAR},",
            "new_value = #{newValue,jdbcType=VARCHAR},",
            "create_time = #{createTime,jdbcType=TIMESTAMP},",
            "operator = #{operator,jdbcType=VARCHAR}",
            "where id = #{id,jdbcType=INTEGER}"
    })
    int updateByPrimaryKey(SysConfigLog record);
}