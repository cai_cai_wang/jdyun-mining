package cn.com.jdyun.mapper;

import cn.com.jdyun.pojo.PartnerIncome;
import org.apache.ibatis.jdbc.SQL;

public class PartnerIncomeSqlProvider {

    public String insertSelective(PartnerIncome record) {
        SQL sql = new SQL();
        sql.INSERT_INTO("phone_partner_income");

        if (record.getUserId() != null) {
            sql.VALUES("user_id", "#{userId,jdbcType=VARCHAR}");
        }

        if (record.getAmount() != null) {
            sql.VALUES("amount", "#{amount,jdbcType=DECIMAL}");
        }
        if (record.getCoinType() != null) {
            sql.VALUES("coin_type", "#{coinType,jdbcType=VARCHAR}");
        }

        if (record.getDate() != null) {
            sql.VALUES("date", "#{date,jdbcType=VARCHAR}");
        }

        if (record.getCreateTime() != null) {
            sql.VALUES("create_time", "#{createTime,jdbcType=TIMESTAMP}");
        }

        return sql.toString();
    }

    public String updateByPrimaryKeySelective(PartnerIncome record) {
        SQL sql = new SQL();
        sql.UPDATE("phone_partner_income");

        if (record.getUserId() != null) {
            sql.SET("user_id = #{userId,jdbcType=VARCHAR}");
        }

        if (record.getAmount() != null) {
            sql.SET("amount = #{amount,jdbcType=DECIMAL}");
        }
        if (record.getCoinType() != null) {
            sql.SET("coin_type = #{coinType,jdbcType=VARCHAR}");
        }
        if (record.getDate() != null) {
            sql.SET("date = #{date,jdbcType=VARCHAR}");
        }

        if (record.getCreateTime() != null) {
            sql.SET("create_time = #{createTime,jdbcType=TIMESTAMP}");
        }

        sql.WHERE("id = #{id,jdbcType=INTEGER}");

        return sql.toString();
    }
}