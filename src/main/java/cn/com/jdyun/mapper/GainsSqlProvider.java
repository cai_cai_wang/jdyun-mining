package cn.com.jdyun.mapper;

import cn.com.jdyun.pojo.Gains;
import org.apache.ibatis.jdbc.SQL;

public class GainsSqlProvider {

    public String insertSelective(Gains record) {
        SQL sql = new SQL();
        sql.INSERT_INTO("phone_gains");

        if (record.getUserId() != null) {
            sql.VALUES("user_id", "#{userId,jdbcType=VARCHAR}");
        }

        if (record.getDate() != null) {
            sql.VALUES("date", "#{date,jdbcType=VARCHAR}");
        }

        if (record.getVested() != null) {
            sql.VALUES("vested", "#{vested,jdbcType=VARCHAR}");
        }

        if (record.getRate() != null) {
            sql.VALUES("rate", "#{rate,jdbcType=VARCHAR}");
        }

        if (record.getTimes() != null) {
            sql.VALUES("times", "#{times,jdbcType=INTEGER}");
        }
        if (record.getCoinType()!= null) {
            sql.VALUES("coin_type", "#{coinType,jdbcType=VARCHAR}");
        }

        if (record.getCreateTime() != null) {
            sql.VALUES("create_time", "#{createTime,jdbcType=TIMESTAMP}");
        }

        if (record.getUpdateTime() != null) {
            sql.VALUES("update_time", "#{updateTime,jdbcType=TIMESTAMP}");
        }

        return sql.toString();
    }

    public String updateByPrimaryKeySelective(Gains record) {
        SQL sql = new SQL();
        sql.UPDATE("phone_gains");

        if (record.getUserId() != null) {
            sql.SET("user_id = #{userId,jdbcType=VARCHAR}");
        }

        if (record.getDate() != null) {
            sql.SET("date = #{date,jdbcType=VARCHAR}");
        }

        if (record.getVested() != null) {
            sql.SET("vested = #{vested,jdbcType=VARCHAR}");
        }

        if (record.getRate() != null) {
            sql.SET("rate = #{rate,jdbcType=VARCHAR}");
        }

        if (record.getTimes() != null) {
            sql.SET("times = #{times,jdbcType=INTEGER}");
        }
        if (record.getCoinType() != null) {
            sql.SET("coin_type = #{coinType,jdbcType=INTEGER}");
        }

        if (record.getCreateTime() != null) {
            sql.SET("create_time = #{createTime,jdbcType=TIMESTAMP}");
        }

        if (record.getUpdateTime() != null) {
            sql.SET("update_time = #{updateTime,jdbcType=TIMESTAMP}");
        }

        sql.WHERE("id = #{id,jdbcType=INTEGER}");

        return sql.toString();
    }
}