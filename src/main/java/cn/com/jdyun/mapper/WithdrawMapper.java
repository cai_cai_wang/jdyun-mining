package cn.com.jdyun.mapper;

import cn.com.jdyun.pojo.Withdraw;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.annotations.UpdateProvider;
import org.apache.ibatis.type.JdbcType;

public interface WithdrawMapper {
    @Delete({
            "delete from phone_withdraw",
            "where id = #{id,jdbcType=INTEGER}"
    })
    int deleteByPrimaryKey(Integer id);

    @Insert({
            "insert into phone_withdraw (user_id, coin_amount, ",
            "coin_type, flag, ",
            "create_time, update_time)",
            "values (#{userId,jdbcType=VARCHAR}, #{coinAmount,jdbcType=VARCHAR}, ",
            "#{coinType,jdbcType=VARCHAR}, #{flag,jdbcType=TINYINT}, ",
            "#{createTime,jdbcType=TIMESTAMP}, #{updateTime,jdbcType=TIMESTAMP})"
    })
    @SelectKey(statement = "SELECT LAST_INSERT_ID()", keyProperty = "id", before = false, resultType = Integer.class)
    int insert(Withdraw record);

    @InsertProvider(type = WithdrawSqlProvider.class, method = "insertSelective")
    @SelectKey(statement = "SELECT LAST_INSERT_ID()", keyProperty = "id", before = false, resultType = Integer.class)
    int insertSelective(Withdraw record);

    @Select({
            "select",
            "id, user_id, coin_amount, coin_type, flag, create_time, update_time",
            "from phone_withdraw",
            "where id = #{id,jdbcType=INTEGER}"
    })
    @Results({
            @Result(column = "id", property = "id", jdbcType = JdbcType.INTEGER, id = true),
            @Result(column = "user_id", property = "userId", jdbcType = JdbcType.VARCHAR),
            @Result(column = "coin_amount", property = "coinAmount", jdbcType = JdbcType.VARCHAR),
            @Result(column = "coin_type", property = "coinType", jdbcType = JdbcType.VARCHAR),
            @Result(column = "flag", property = "flag", jdbcType = JdbcType.TINYINT),
            @Result(column = "create_time", property = "createTime", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "update_time", property = "updateTime", jdbcType = JdbcType.TIMESTAMP)
    })
    Withdraw selectByPrimaryKey(Integer id);

    @UpdateProvider(type = WithdrawSqlProvider.class, method = "updateByPrimaryKeySelective")
    int updateByPrimaryKeySelective(Withdraw record);

    @Update({
            "update phone_withdraw",
            "set user_id = #{userId,jdbcType=VARCHAR},",
            "coin_amount = #{coinAmount,jdbcType=VARCHAR},",
            "coin_type = #{coinType,jdbcType=VARCHAR},",
            "flag = #{flag,jdbcType=TINYINT},",
            "create_time = #{createTime,jdbcType=TIMESTAMP},",
            "update_time = #{updateTime,jdbcType=TIMESTAMP}",
            "where id = #{id,jdbcType=INTEGER}"
    })
    int updateByPrimaryKey(Withdraw record);
}