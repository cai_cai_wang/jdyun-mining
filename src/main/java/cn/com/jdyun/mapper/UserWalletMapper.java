package cn.com.jdyun.mapper;

import cn.com.jdyun.pojo.UserWallet;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.annotations.UpdateProvider;
import org.apache.ibatis.type.JdbcType;

import java.util.List;

public interface UserWalletMapper {
    @Delete({
            "delete from phone_user_wallet",
            "where id = #{id,jdbcType=INTEGER}"
    })
    int deleteByPrimaryKey(Integer id);

    @Insert({
            "insert into phone_user_wallet (user_id, coin_type, ",
            "asset, balance, ",
            "ming_coin, task_coin, ",
            "locked_coin, freeze_coin, ",
            "create_time, update_time)",
            "values (#{userId,jdbcType=VARCHAR}, #{coinType,jdbcType=VARCHAR}, ",
            "#{asset,jdbcType=VARCHAR}, #{balance,jdbcType=VARCHAR}, ",
            "#{mingCoin,jdbcType=VARCHAR}, #{taskCoin,jdbcType=VARCHAR}, ",
            "#{lockedCoin,jdbcType=DECIMAL}, #{freezeCoin,jdbcType=VARCHAR}, ",
            "#{createTime,jdbcType=TIMESTAMP}, #{updateTime,jdbcType=TIMESTAMP})"
    })
    @SelectKey(statement = "SELECT LAST_INSERT_ID()", keyProperty = "id", before = false, resultType = Integer.class)
    int insert(UserWallet record);

    @InsertProvider(type = UserWalletSqlProvider.class, method = "insertSelective")
    @SelectKey(statement = "SELECT LAST_INSERT_ID()", keyProperty = "id", before = false, resultType = Integer.class)
    int insertSelective(UserWallet record);


    @Select({
            "select",
            "id, user_id, coin_type, asset, balance, ming_coin, task_coin, locked_coin, freeze_coin, ",
            "create_time, update_time",
            "from phone_user_wallet",
            "where id = #{id,jdbcType=INTEGER}"
    })
    @Results({
            @Result(column = "id", property = "id", jdbcType = JdbcType.INTEGER, id = true),
            @Result(column = "user_id", property = "userId", jdbcType = JdbcType.VARCHAR),
            @Result(column = "coin_type", property = "coinType", jdbcType = JdbcType.VARCHAR),
            @Result(column = "asset", property = "asset", jdbcType = JdbcType.VARCHAR),
            @Result(column = "balance", property = "balance", jdbcType = JdbcType.VARCHAR),
            @Result(column = "ming_coin", property = "mingCoin", jdbcType = JdbcType.VARCHAR),
            @Result(column = "task_coin", property = "taskCoin", jdbcType = JdbcType.VARCHAR),
            @Result(column = "locked_coin", property = "lockedCoin", jdbcType = JdbcType.DECIMAL),
            @Result(column = "freeze_coin", property = "freezeCoin", jdbcType = JdbcType.VARCHAR),
            @Result(column = "create_time", property = "createTime", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "update_time", property = "updateTime", jdbcType = JdbcType.TIMESTAMP)
    })
    UserWallet selectByPrimaryKey(Integer id);

    @Select({
            "select",
            "id, user_id, coin_type, asset, balance, ming_coin, task_coin, locked_coin, freeze_coin, ",
            "create_time, update_time",
            "from phone_user_wallet"

    })
    @Results({
            @Result(column = "id", property = "id", jdbcType = JdbcType.INTEGER, id = true),
            @Result(column = "user_id", property = "userId", jdbcType = JdbcType.VARCHAR),
            @Result(column = "coin_type", property = "coinType", jdbcType = JdbcType.VARCHAR),
            @Result(column = "asset", property = "asset", jdbcType = JdbcType.VARCHAR),
            @Result(column = "balance", property = "balance", jdbcType = JdbcType.VARCHAR),
            @Result(column = "ming_coin", property = "mingCoin", jdbcType = JdbcType.VARCHAR),
            @Result(column = "task_coin", property = "taskCoin", jdbcType = JdbcType.VARCHAR),
            @Result(column = "locked_coin", property = "lockedCoin", jdbcType = JdbcType.DECIMAL),
            @Result(column = "freeze_coin", property = "freezeCoin", jdbcType = JdbcType.VARCHAR),
            @Result(column = "create_time", property = "createTime", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "update_time", property = "updateTime", jdbcType = JdbcType.TIMESTAMP)
    })
    List<UserWallet> selectByPrimaryKey1();

    @Select("select distinct user_id  from phone_user_wallet where user_id not in (select  user_id from phone_user_wallet where coin_type = 'TEX')")
    List<String> selectByTexIdNot();

    @UpdateProvider(type = UserWalletSqlProvider.class, method = "updateByPrimaryKeySelective")
    int updateByPrimaryKeySelective(UserWallet record);

    @Insert("insert into phone_user_wallet(user_id,coin_type,asset,balance,ming_coin,task_coin,locked_coin,freeze_coin,create_time,update_time)" +
            " values(#{userId},#{coinType},#{asset},#{balance},#{mingCoin},#{taskCoin},#{lockedCoin},#{freezeCoin},#{createTime},#{updateTime})")
    int insertTexRecord(UserWallet userWallet);

    @Update({
            "update phone_user_wallet",
            "set user_id = #{userId,jdbcType=VARCHAR},",
            "coin_type = #{coinType,jdbcType=VARCHAR},",
            "asset = #{asset,jdbcType=VARCHAR},",
            "balance = #{balance,jdbcType=VARCHAR},",
            "ming_coin = #{mingCoin,jdbcType=VARCHAR},",
            "task_coin = #{taskCoin,jdbcType=VARCHAR},",
            "locked_coin = #{lockedCoin,jdbcType=DECIMAL},",
            "freeze_coin = #{freezeCoin,jdbcType=VARCHAR},",
            "create_time = #{createTime,jdbcType=TIMESTAMP},",
            "update_time = #{updateTime,jdbcType=TIMESTAMP}",
            "where id = #{id,jdbcType=INTEGER}"
    })
    int updateByPrimaryKey(UserWallet record);
}