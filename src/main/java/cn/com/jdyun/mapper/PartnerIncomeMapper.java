package cn.com.jdyun.mapper;

import cn.com.jdyun.pojo.PartnerIncome;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.annotations.UpdateProvider;
import org.apache.ibatis.type.JdbcType;

public interface PartnerIncomeMapper {
    @Delete({
            "delete from phone_partner_income",
            "where id = #{id,jdbcType=INTEGER}"
    })
    int deleteByPrimaryKey(Integer id);

    @Insert({
            "insert into phone_partner_income (user_id, amount,coin_type ,date, create_time)",
            "values (#{userId,jdbcType=VARCHAR}, #{amount,jdbcType=DECIMAL},#{coin_type,jdbcType=VARCHAR}, ",
            "#{date,jdbcType=VARCHAR}, #{createTime,jdbcType=TIMESTAMP})"
    })
    @SelectKey(statement = "SELECT LAST_INSERT_ID()", keyProperty = "id", before = false, resultType = Integer.class)
    int insert(PartnerIncome record);

    @InsertProvider(type = PartnerIncomeSqlProvider.class, method = "insertSelective")
    @SelectKey(statement = "SELECT LAST_INSERT_ID()", keyProperty = "id", before = false, resultType = Integer.class)
    int insertSelective(PartnerIncome record);

    @Select({
            "select",
            "id, user_id, amount, coin_type,date, create_time",
            "from phone_partner_income",
            "where id = #{id,jdbcType=INTEGER}"
    })
    @Results({
            @Result(column = "id", property = "id", jdbcType = JdbcType.INTEGER, id = true),
            @Result(column = "user_id", property = "userId", jdbcType = JdbcType.VARCHAR),
            @Result(column = "amount", property = "amount", jdbcType = JdbcType.DECIMAL),
            @Result(column = "coin_type", property = "coinType", jdbcType = JdbcType.VARCHAR),
            @Result(column = "date", property = "date", jdbcType = JdbcType.VARCHAR),
            @Result(column = "create_time", property = "createTime", jdbcType = JdbcType.TIMESTAMP)
    })
    PartnerIncome selectByPrimaryKey(Integer id);

    @UpdateProvider(type = PartnerIncomeSqlProvider.class, method = "updateByPrimaryKeySelective")
    int updateByPrimaryKeySelective(PartnerIncome record);

    @Update({
            "update phone_partner_income",
            "set user_id = #{userId,jdbcType=VARCHAR},",
            "amount = #{amount,jdbcType=DECIMAL},",
            "date = #{date,jdbcType=VARCHAR},",
            "coin_type = #{coinType,jdbcType=VARCHAR},",
            "create_time = #{createTime,jdbcType=TIMESTAMP}",
            "where id = #{id,jdbcType=INTEGER}"
    })
    int updateByPrimaryKey(PartnerIncome record);

    @Select("select id,user_id,coin_type,amount,date,create_time from phone_partner_income where user_id = #{userId} and date = #{date}")
    @Results({
            @Result(column = "id", property = "id", jdbcType = JdbcType.INTEGER, id = true),
            @Result(column = "user_id", property = "userId", jdbcType = JdbcType.VARCHAR),
            @Result(column = "coin_type", property = "coinType", jdbcType = JdbcType.VARCHAR),
            @Result(column = "amount", property = "amount", jdbcType = JdbcType.DECIMAL),
            @Result(column = "date", property = "date", jdbcType = JdbcType.VARCHAR),
            @Result(column = "create_time", property = "createTime", jdbcType = JdbcType.TIMESTAMP)
    })
    PartnerIncome inqueryByUserId(@Param("userId") String userId, @Param("date") String date);
}