package cn.com.jdyun.mapper;

import cn.com.jdyun.pojo.Referee;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.annotations.UpdateProvider;
import org.apache.ibatis.type.JdbcType;

public interface RefereeMapper {
    @Delete({
            "delete from phone_referee",
            "where id = #{id,jdbcType=INTEGER}"
    })
    int deleteByPrimaryKey(Integer id);

    @Insert({
            "insert into phone_referee (user_id, referee_id, ",
            "referee_parent_id, context, ",
            "create_time, update_time)",
            "values (#{userId,jdbcType=VARCHAR}, #{refereeId,jdbcType=VARCHAR}, ",
            "#{refereeParentId,jdbcType=VARCHAR}, #{context,jdbcType=VARCHAR}, ",
            "#{createTime,jdbcType=TIMESTAMP}, #{updateTime,jdbcType=TIMESTAMP})"
    })
    @SelectKey(statement = "SELECT LAST_INSERT_ID()", keyProperty = "id", before = false, resultType = Integer.class)
    int insert(Referee record);

    @InsertProvider(type = RefereeSqlProvider.class, method = "insertSelective")
    @SelectKey(statement = "SELECT LAST_INSERT_ID()", keyProperty = "id", before = false, resultType = Integer.class)
    int insertSelective(Referee record);

    @Select({
            "select",
            "id, user_id, referee_id, referee_parent_id, context, create_time, update_time",
            "from phone_referee",
            "where id = #{id,jdbcType=INTEGER}"
    })
    @Results({
            @Result(column = "id", property = "id", jdbcType = JdbcType.INTEGER, id = true),
            @Result(column = "user_id", property = "userId", jdbcType = JdbcType.VARCHAR),
            @Result(column = "referee_id", property = "refereeId", jdbcType = JdbcType.VARCHAR),
            @Result(column = "referee_parent_id", property = "refereeParentId", jdbcType = JdbcType.VARCHAR),
            @Result(column = "context", property = "context", jdbcType = JdbcType.VARCHAR),
            @Result(column = "create_time", property = "createTime", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "update_time", property = "updateTime", jdbcType = JdbcType.TIMESTAMP)
    })
    Referee selectByPrimaryKey(Integer id);

    @UpdateProvider(type = RefereeSqlProvider.class, method = "updateByPrimaryKeySelective")
    int updateByPrimaryKeySelective(Referee record);

    @Update({
            "update phone_referee",
            "set user_id = #{userId,jdbcType=VARCHAR},",
            "referee_id = #{refereeId,jdbcType=VARCHAR},",
            "referee_parent_id = #{refereeParentId,jdbcType=VARCHAR},",
            "context = #{context,jdbcType=VARCHAR},",
            "create_time = #{createTime,jdbcType=TIMESTAMP},",
            "update_time = #{updateTime,jdbcType=TIMESTAMP}",
            "where id = #{id,jdbcType=INTEGER}"
    })
    int updateByPrimaryKey(Referee record);
}