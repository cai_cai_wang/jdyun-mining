package cn.com.jdyun.mapper;

import org.apache.ibatis.jdbc.SQL;

import cn.com.jdyun.pojo.Exchange;

public class ExchangeSqlProvider {

    public String insertSelective(Exchange record) {
        SQL sql = new SQL();
        sql.INSERT_INTO("phone_exchange");
        
        if (record.getRate() != null) {
            sql.VALUES("rate", "#{rate,jdbcType=DECIMAL}");
        }
        
        if (record.getFromCoinType() != null) {
            sql.VALUES("from_coin_type", "#{fromCoinType,jdbcType=VARCHAR}");
        }
        
        if (record.getFromAmount() != null) {
            sql.VALUES("from_amount", "#{fromAmount,jdbcType=DECIMAL}");
        }
        
        if (record.getToCoinType() != null) {
            sql.VALUES("to_coin_type", "#{toCoinType,jdbcType=VARCHAR}");
        }
        
        if (record.getToAmount() != null) {
            sql.VALUES("to_amount", "#{toAmount,jdbcType=DECIMAL}");
        }
        
        if (record.getUserId() != null) {
            sql.VALUES("user_id", "#{userId,jdbcType=VARCHAR}");
        }
        
        if (record.getCreateTime() != null) {
            sql.VALUES("create_time", "#{createTime,jdbcType=TIMESTAMP}");
        }
        
        return sql.toString();
    }

    public String updateByPrimaryKeySelective(Exchange record) {
        SQL sql = new SQL();
        sql.UPDATE("phone_exchange");
        
        if (record.getRate() != null) {
            sql.SET("rate = #{rate,jdbcType=DECIMAL}");
        }
        
        if (record.getFromCoinType() != null) {
            sql.SET("from_coin_type = #{fromCoinType,jdbcType=VARCHAR}");
        }
        
        if (record.getFromAmount() != null) {
            sql.SET("from_amount = #{fromAmount,jdbcType=DECIMAL}");
        }
        
        if (record.getToCoinType() != null) {
            sql.SET("to_coin_type = #{toCoinType,jdbcType=VARCHAR}");
        }
        
        if (record.getToAmount() != null) {
            sql.SET("to_amount = #{toAmount,jdbcType=DECIMAL}");
        }
        
        if (record.getUserId() != null) {
            sql.SET("user_id = #{userId,jdbcType=VARCHAR}");
        }
        
        if (record.getCreateTime() != null) {
            sql.SET("create_time = #{createTime,jdbcType=TIMESTAMP}");
        }
        
        sql.WHERE("id = #{id,jdbcType=INTEGER}");
        
        return sql.toString();
    }
}