package cn.com.jdyun.mapper;

import cn.com.jdyun.pojo.UserSec;
import org.apache.ibatis.jdbc.SQL;

import java.util.Map;

public class UserSecSqlProvider {

    public String insertSelective(UserSec record) {
        SQL sql = new SQL();
        sql.INSERT_INTO("phone_user_sec");

        if (record.getUserId() != null) {
            sql.VALUES("user_id", "#{userId,jdbcType=VARCHAR}");
        }

        if (record.getSecPassword() != null) {
            sql.VALUES("sec_password", "#{secPassword,jdbcType=VARCHAR}");
        }

        if (record.getIdCardNo() != null) {
            sql.VALUES("id_card_no", "#{idCardNo,jdbcType=VARCHAR}");
        }

        if (record.getIdCardName() != null) {
            sql.VALUES("id_card_name", "#{idCardName,jdbcType=VARCHAR}");
        }

        if (record.getIdCardUp() != null) {
            sql.VALUES("id_card_up", "#{idCardUp,jdbcType=VARCHAR}");
        }

        if (record.getIdCardDown() != null) {
            sql.VALUES("id_card_down", "#{idCardDown,jdbcType=VARCHAR}");
        }

        if (record.getFlag() != null) {
            sql.VALUES("flag", "#{flag,jdbcType=TINYINT}");
        }

        if (record.getCreateTime() != null) {
            sql.VALUES("create_time", "#{createTime,jdbcType=TIMESTAMP}");
        }

        if (record.getUpdateTime() != null) {
            sql.VALUES("update_time", "#{updateTime,jdbcType=TIMESTAMP}");
        }

        return sql.toString();
    }

    public String updateByPrimaryKeySelective(UserSec record) {
        SQL sql = new SQL();
        sql.UPDATE("phone_user_sec");

        if (record.getUserId() != null) {
            sql.SET("user_id = #{userId,jdbcType=VARCHAR}");
        }

        if (record.getSecPassword() != null) {
            sql.SET("sec_password = #{secPassword,jdbcType=VARCHAR}");
        }

        if (record.getIdCardNo() != null) {
            sql.SET("id_card_no = #{idCardNo,jdbcType=VARCHAR}");
        }

        if (record.getIdCardName() != null) {
            sql.SET("id_card_name = #{idCardName,jdbcType=VARCHAR}");
        }

        if (record.getIdCardUp() != null) {
            sql.SET("id_card_up = #{idCardUp,jdbcType=VARCHAR}");
        }

        if (record.getIdCardDown() != null) {
            sql.SET("id_card_down = #{idCardDown,jdbcType=VARCHAR}");
        }

        if (record.getFlag() != null) {
            sql.SET("flag = #{flag,jdbcType=TINYINT}");
        }

        if (record.getCreateTime() != null) {
            sql.SET("create_time = #{createTime,jdbcType=TIMESTAMP}");
        }

        if (record.getUpdateTime() != null) {
            sql.SET("update_time = #{updateTime,jdbcType=TIMESTAMP}");
        }

        sql.WHERE("id = #{id,jdbcType=INTEGER}");

        return sql.toString();
    }

    public String pageData(Map<String, Object> condition) {

        SQL sql = new SQL();
        sql.SELECT("id, user_id, id_card_no, id_card_name, id_card_up, id_card_down, flag, create_time, update_time");
        sql.FROM("phone_user_sec");

        StringBuffer where = new StringBuffer(" 1 = 1");
        if (condition != null) {
            if (condition.containsKey("idCardName")) {
                where.append(" and id_card_name = #{idCardName,jdbcType=VARCHAR}");
            }
            if (condition.containsKey("idCardNo")) {
                where.append(" and id_card_no like concat('%',#{idCardNo,jdbcType=VARCHAR},'%')");
            }
            if (condition.containsKey("flag")) {
                where.append(" and flag = #{flag,jdbcType=TINYINT}");
            }
            if (condition.containsKey("dateStart")) {
                where.append(" and create_time >= #{dateStart,jdbcType=TIMESTAMP}");
            }
            if (condition.containsKey("dateEnd")) {
                where.append(" and create_time < #{dateEnd,jdbcType=TIMESTAMP}");
            }
        }
        sql.WHERE(where.toString());
        sql.ORDER_BY("create_time desc");
        return sql.toString();
    }
}