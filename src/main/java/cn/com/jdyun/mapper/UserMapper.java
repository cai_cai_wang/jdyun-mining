package cn.com.jdyun.mapper;

import cn.com.jdyun.pojo.User;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.annotations.UpdateProvider;
import org.apache.ibatis.type.JdbcType;

public interface UserMapper {
    @Delete({
            "delete from phone_user",
            "where id = #{id,jdbcType=VARCHAR}"
    })
    int deleteByPrimaryKey(String id);

    @Insert({
            "insert into phone_user (id, phone, ",
            "email, password, ",
            "nick_name, head_pic, ",
            "referee, code, flag, ",
            "admin, level,tex_level, create_time, ",
            "update_time)",
            "values (#{id,jdbcType=VARCHAR}, #{phone,jdbcType=VARCHAR}, ",
            "#{email,jdbcType=VARCHAR}, #{password,jdbcType=VARCHAR}, ",
            "#{nickName,jdbcType=VARCHAR}, #{headPic,jdbcType=VARCHAR}, ",
            "#{referee,jdbcType=VARCHAR}, #{code,jdbcType=VARCHAR}, #{flag,jdbcType=TINYINT}, ",
            "#{admin,jdbcType=TINYINT}, #{level,jdbcType=INTEGER},#{texLevel,jdbcType=INTEGER}, #{createTime,jdbcType=TIMESTAMP}, ",
            "#{updateTime,jdbcType=TIMESTAMP})"
    })
    int insert(User record);

    @InsertProvider(type = UserSqlProvider.class, method = "insertSelective")
    int insertSelective(User record);

    @Select({
            "select",
            "id, phone, email, password, nick_name, head_pic, referee, code, flag, admin, ",
            "level, tex_level,create_time, update_time",
            "from phone_user",
            "where id = #{id,jdbcType=VARCHAR}"
    })
    @Results({
            @Result(column = "id", property = "id", jdbcType = JdbcType.VARCHAR, id = true),
            @Result(column = "phone", property = "phone", jdbcType = JdbcType.VARCHAR),
            @Result(column = "email", property = "email", jdbcType = JdbcType.VARCHAR),
            @Result(column = "password", property = "password", jdbcType = JdbcType.VARCHAR),
            @Result(column = "nick_name", property = "nickName", jdbcType = JdbcType.VARCHAR),
            @Result(column = "head_pic", property = "headPic", jdbcType = JdbcType.VARCHAR),
            @Result(column = "referee", property = "referee", jdbcType = JdbcType.VARCHAR),
            @Result(column = "code", property = "code", jdbcType = JdbcType.VARCHAR),
            @Result(column = "flag", property = "flag", jdbcType = JdbcType.TINYINT),
            @Result(column = "admin", property = "admin", jdbcType = JdbcType.TINYINT),
            @Result(column = "level", property = "level", jdbcType = JdbcType.INTEGER),
            @Result(column = "tex_level", property = "texLevel", jdbcType = JdbcType.INTEGER),
            @Result(column = "create_time", property = "createTime", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "update_time", property = "updateTime", jdbcType = JdbcType.TIMESTAMP)
    })
    User selectByPrimaryKey(String id);

    @UpdateProvider(type = UserSqlProvider.class, method = "updateByPrimaryKeySelective")
    int updateByPrimaryKeySelective(User record);

    @Update({
            "update phone_user",
            "set phone = #{phone,jdbcType=VARCHAR},",
            "email = #{email,jdbcType=VARCHAR},",
            "password = #{password,jdbcType=VARCHAR},",
            "nick_name = #{nickName,jdbcType=VARCHAR},",
            "head_pic = #{headPic,jdbcType=VARCHAR},",
            "referee = #{referee,jdbcType=VARCHAR},",
            "code = #{code,jdbcType=VARCHAR},",
            "flag = #{flag,jdbcType=TINYINT},",
            "admin = #{admin,jdbcType=TINYINT},",
            "level = #{level,jdbcType=INTEGER},",
            "tex_level = #{texLevel,jdbcType=INTEGER},",
            "create_time = #{createTime,jdbcType=TIMESTAMP},",
            "update_time = #{updateTime,jdbcType=TIMESTAMP}",
            "where id = #{id,jdbcType=VARCHAR}"
    })
    int updateByPrimaryKey(User record);
}