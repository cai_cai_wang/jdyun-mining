package cn.com.jdyun.mapper;

import cn.com.jdyun.pojo.MessageUser;
import org.apache.ibatis.jdbc.SQL;

public class MessageUserSqlProvider {

    public String insertSelective(MessageUser record) {
        SQL sql = new SQL();
        sql.INSERT_INTO("phone_message_user");

        if (record.getUserId() != null) {
            sql.VALUES("user_id", "#{userId,jdbcType=VARCHAR}");
        }

        if (record.getMessageId() != null) {
            sql.VALUES("message_id", "#{messageId,jdbcType=INTEGER}");
        }

        if (record.getFlag() != null) {
            sql.VALUES("flag", "#{flag,jdbcType=TINYINT}");
        }

        if (record.getCreateTime() != null) {
            sql.VALUES("create_time", "#{createTime,jdbcType=TIMESTAMP}");
        }

        if (record.getUpdateTime() != null) {
            sql.VALUES("update_time", "#{updateTime,jdbcType=TIMESTAMP}");
        }

        return sql.toString();
    }

    public String updateByPrimaryKeySelective(MessageUser record) {
        SQL sql = new SQL();
        sql.UPDATE("phone_message_user");

        if (record.getUserId() != null) {
            sql.SET("user_id = #{userId,jdbcType=VARCHAR}");
        }

        if (record.getMessageId() != null) {
            sql.SET("message_id = #{messageId,jdbcType=INTEGER}");
        }

        if (record.getFlag() != null) {
            sql.SET("flag = #{flag,jdbcType=TINYINT}");
        }

        if (record.getCreateTime() != null) {
            sql.SET("create_time = #{createTime,jdbcType=TIMESTAMP}");
        }

        if (record.getUpdateTime() != null) {
            sql.SET("update_time = #{updateTime,jdbcType=TIMESTAMP}");
        }

        sql.WHERE("id = #{id,jdbcType=INTEGER}");

        return sql.toString();
    }
}