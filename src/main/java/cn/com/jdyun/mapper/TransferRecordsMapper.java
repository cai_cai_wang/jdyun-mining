package cn.com.jdyun.mapper;

import cn.com.jdyun.pojo.TransferRecords;
import com.github.pagehelper.Page;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.annotations.UpdateProvider;
import org.apache.ibatis.type.JdbcType;

public interface TransferRecordsMapper {
    @Delete({
            "delete from phone_transfer_records",
            "where id = #{id,jdbcType=INTEGER}"
    })
    int deleteByPrimaryKey(Integer id);

    @Insert({
            "insert into phone_transfer_records (order_id, transfer_type, ",
            "amount, fee, coin_type, ",
            "from_user, to_user, ",
            "memo, create_time)",
            "values (#{orderId,jdbcType=VARCHAR}, #{transferType,jdbcType=TINYINT}, ",
            "#{amount,jdbcType=VARCHAR}, #{fee,jdbcType=VARCHAR}, #{coinType,jdbcType=VARCHAR}, ",
            "#{fromUser,jdbcType=VARCHAR}, #{toUser,jdbcType=VARCHAR}, ",
            "#{memo,jdbcType=VARCHAR}, #{createTime,jdbcType=TIMESTAMP})"
    })
    @SelectKey(statement = "SELECT LAST_INSERT_ID()", keyProperty = "id", before = false, resultType = Integer.class)
    int insert(TransferRecords record);

    @InsertProvider(type = TransferRecordsSqlProvider.class, method = "insertSelective")
    @SelectKey(statement = "SELECT LAST_INSERT_ID()", keyProperty = "id", before = false, resultType = Integer.class)
    int insertSelective(TransferRecords record);

    @Select({
            "select",
            "id, order_id, transfer_type, amount, fee, coin_type, from_user, to_user, memo, ",
            "create_time",
            "from phone_transfer_records",
            "where id = #{id,jdbcType=INTEGER}"
    })
    @Results({
            @Result(column = "id", property = "id", jdbcType = JdbcType.INTEGER, id = true),
            @Result(column = "order_id", property = "orderId", jdbcType = JdbcType.VARCHAR),
            @Result(column = "transfer_type", property = "transferType", jdbcType = JdbcType.TINYINT),
            @Result(column = "amount", property = "amount", jdbcType = JdbcType.VARCHAR),
            @Result(column = "fee", property = "fee", jdbcType = JdbcType.VARCHAR),
            @Result(column = "coin_type", property = "coinType", jdbcType = JdbcType.VARCHAR),
            @Result(column = "from_user", property = "fromUser", jdbcType = JdbcType.VARCHAR),
            @Result(column = "to_user", property = "toUser", jdbcType = JdbcType.VARCHAR),
            @Result(column = "memo", property = "memo", jdbcType = JdbcType.VARCHAR),
            @Result(column = "create_time", property = "createTime", jdbcType = JdbcType.TIMESTAMP)
    })
    TransferRecords selectByPrimaryKey(Integer id);

    @UpdateProvider(type = TransferRecordsSqlProvider.class, method = "updateByPrimaryKeySelective")
    int updateByPrimaryKeySelective(TransferRecords record);

    @Update({
            "update phone_transfer_records",
            "set order_id = #{orderId,jdbcType=VARCHAR},",
            "transfer_type = #{transferType,jdbcType=TINYINT},",
            "amount = #{amount,jdbcType=VARCHAR},",
            "fee = #{fee,jdbcType=VARCHAR},",
            "coin_type = #{coinType,jdbcType=VARCHAR},",
            "from_user = #{fromUser,jdbcType=VARCHAR},",
            "to_user = #{toUser,jdbcType=VARCHAR},",
            "memo = #{memo,jdbcType=VARCHAR},",
            "create_time = #{createTime,jdbcType=TIMESTAMP}",
            "where id = #{id,jdbcType=INTEGER}"
    })
    int updateByPrimaryKey(TransferRecords record);

    @Select({
            "select",
            "id, order_id, transfer_type, amount, fee, coin_type, from_user, to_user, memo, ",
            "create_time",
            "from phone_transfer_records",
            "where coin_type = #{coinType,jdbcType=VARCHAR} and (from_user = #{userId,jdbcType=VARCHAR} or to_user = #{userId,jdbcType=VARCHAR})"
    })
    @Results({
            @Result(column = "id", property = "id", jdbcType = JdbcType.INTEGER, id = true),
            @Result(column = "order_id", property = "orderId", jdbcType = JdbcType.VARCHAR),
            @Result(column = "transfer_type", property = "transferType", jdbcType = JdbcType.TINYINT),
            @Result(column = "amount", property = "amount", jdbcType = JdbcType.VARCHAR),
            @Result(column = "fee", property = "fee", jdbcType = JdbcType.VARCHAR),
            @Result(column = "coin_type", property = "coinType", jdbcType = JdbcType.VARCHAR),
            @Result(column = "from_user", property = "fromUser", jdbcType = JdbcType.VARCHAR),
            @Result(column = "to_user", property = "toUser", jdbcType = JdbcType.VARCHAR),
            @Result(column = "memo", property = "memo", jdbcType = JdbcType.VARCHAR),
            @Result(column = "create_time", property = "createTime", jdbcType = JdbcType.TIMESTAMP)
    })
    Page<TransferRecords> pageData(@Param("userId") String userId, @Param("coinType") String coinType);
}