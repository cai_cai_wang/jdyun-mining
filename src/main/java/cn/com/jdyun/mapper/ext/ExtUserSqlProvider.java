package cn.com.jdyun.mapper.ext;

import cn.com.jdyun.mapper.UserSqlProvider;
import cn.com.jdyun.pojo.User;
import org.apache.ibatis.jdbc.SQL;

import java.util.Map;

public class ExtUserSqlProvider extends UserSqlProvider {
    public String selectBySelective(User record) {
        SQL sql = new SQL();
        sql.SELECT("id, phone, email, nick_name, head_pic, referee, code, flag, admin, level, tex_level,create_time, update_time");
        sql.FROM("phone_user");
        StringBuffer where = new StringBuffer(" 1 = 1");
        if (record != null) {
            if (record.getPhone() != null) {
                where.append(" and phone = #{phone,jdbcType=VARCHAR}");
            }

            if (record.getEmail() != null) {
                where.append(" and email = #{email,jdbcType=VARCHAR}");
            }

            if (record.getPassword() != null) {
                where.append(" and password = #{password,jdbcType=VARCHAR}");
            }

            if (record.getNickName() != null) {
                where.append(" and nick_name = #{nickName,jdbcType=VARCHAR}");
            }

            if (record.getHeadPic() != null) {
                where.append(" and head_pic = #{headPic,jdbcType=VARCHAR}");
            }

            if (record.getReferee() != null) {
                where.append(" and referee = #{referee,jdbcType=VARCHAR}");
            }

            if (record.getCode() != null) {
                where.append(" and code = #{code,jdbcType=VARCHAR}");
            }

            if (record.getFlag() != null) {
                where.append(" and flag = #{flag,jdbcType=TINYINT}");
            }

            if (record.getAdmin() != null) {
                where.append("admin = #{admin,jdbcType=TINYINT}");
            }

            if (record.getLevel() != null) {
                where.append(" and level = #{level,jdbcType=INTEGER}");
            }
            if (record.getTexLevel() != null) {
                where.append(" and tex_level = #{tex_level,jdbcType=INTEGER}");
            }

            if (record.getCreateTime() != null) {
                where.append(" and create_time = #{createTime,jdbcType=TIMESTAMP}");
            }

            if (record.getUpdateTime() != null) {
                where.append(" and update_time = #{updateTime,jdbcType=TIMESTAMP}");
            }
        }
        sql.WHERE(where.toString());
        sql.ORDER_BY("create_time desc");
        return sql.toString();
    }

    public String pageData(Map<String, Object> condition) {

        SQL sql = new SQL();
        sql.SELECT("id, phone, email, nick_name, head_pic, referee, code, flag, admin, level,tex_level, create_time, update_time");
        sql.FROM("phone_user");

        StringBuffer where = new StringBuffer(" 1 = 1");
        if (condition != null) {
            if (condition.containsKey("username") && condition.get("username") != null) {
                where.append(" and (phone like concat('%',#{username,jdbcType=VARCHAR},'%') or email like concat('%',#{username,jdbcType=VARCHAR},'%'))");
            }
            if (condition.containsKey("flag") && condition.get("flag") != null) {
                where.append(" and flag = #{flag,jdbcType=TINYINT}");
            }
            if (condition.containsKey("dateStart") && condition.get("dateStart") != null) {
                where.append(" and create_time >= #{dateStart,jdbcType=TIMESTAMP}");
            }
            if (condition.containsKey("dateEnd") && condition.get("dateEnd") != null) {
                where.append(" and create_time < #{dateEnd,jdbcType=TIMESTAMP}");
            }
        }
        sql.WHERE(where.toString());
        sql.ORDER_BY("create_time desc");
        return sql.toString();
    }
}
