package cn.com.jdyun.mapper.ext;

import cn.com.jdyun.pojo.SysConfig;
import org.apache.ibatis.jdbc.SQL;

/**
 * @author yzping
 * @date Created in 下午 4:16 2018/9/19 0019
 */
public class ExtSysConfigProvider {

    public String updateParameterValues(SysConfig sysConfig) {
        SQL sql = new SQL();
        sql.UPDATE("phone_sys_config");

        if (sysConfig.getCode() != null) {
            sql.SET("code = #{code,jdbcType=VARCHAR}");
        }
        if (sysConfig.getCodeName() != null) {
            sql.SET("code_name = #{codeName,jdbcType=VARCHAR}");
        }
        if (sysConfig.getInitValue()!= null) {
            sql.SET("init_value = #{initValue,jdbcType=VARCHAR}");
        }
        if (sysConfig.getCreateTime() != null) {
            sql.SET("create_time = #{createTime,jdbcType=TIMESTAMP}");
        }
        if (sysConfig.getUpdateTime() != null) {
            sql.SET("update_time = #{updateTime,jdbcType=TIMESTAMP}");
        }
        sql.WHERE("id = #{id,jdbcType=INTEGER}");
        return sql.toString();
    }
}
