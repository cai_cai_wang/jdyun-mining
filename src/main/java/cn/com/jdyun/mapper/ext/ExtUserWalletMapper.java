package cn.com.jdyun.mapper.ext;

import cn.com.jdyun.mapper.UserWalletMapper;
import cn.com.jdyun.pojo.UserWallet;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.type.JdbcType;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author yzping
 * @date Created in 下午 2:36 2018/8/18 0018
 */
public interface ExtUserWalletMapper extends UserWalletMapper {
    /**
     * 查询用户固定的算力,余额
     *
     * @param userId   用户id
     * @param coinType 代币的币种
     * @return
     */
    @Select({
            "select",
            "id,user_id,coin_type,asset,balance,locked_coin,freeze_coin",
            "from phone_user_wallet",
            "where user_id = #{userId} and coin_type = #{coinType}"
    })
    @Results({
            @Result(column = "id", property = "id", jdbcType = JdbcType.INTEGER, id = true),
            @Result(column = "user_id", property = "userId", jdbcType = JdbcType.VARCHAR),
            @Result(column = "coin_type", property = "coinType", jdbcType = JdbcType.VARCHAR),
            @Result(column = "asset", property = "asset", jdbcType = JdbcType.VARCHAR),
            @Result(column = "balance", property = "balance", jdbcType = JdbcType.VARCHAR),
            @Result(column = "locked_coin", property = "lockedCoin", jdbcType = JdbcType.DECIMAL),
            @Result(column = "freeze_coin", property = "freezeCoin", jdbcType = JdbcType.VARCHAR),
    })
    UserWallet inqueryLockedCoinAmount(@Param("userId") String userId, @Param("coinType") String coinType);

    /**
     * 更新用户固定的算力和可用余额
     *
     * @return
     */
    @Update({
            "update phone_user_wallet",
            "set balance = #{balance,jdbcType=VARCHAR},",
            "locked_coin = #{lockedCoin,jdbcType=DECIMAL},",
            "update_time = #{updateTime,jdbcType=TIMESTAMP},",
            "asset = #{asset,jdbcType=VARCHAR}",
            "where user_id = #{userId} and coin_type = #{coinType}"
    })
    int updateWalletCoinAmount(UserWallet userWallet);

    @Select({
            "select",
            "balance",
            "from phone_user_wallet",
            "where user_id = #{userId} and coin_type = #{coinType}"
    })
    String inqueryCoinAmount(@Param("userId") String userId, @Param("coinType") String coinType);

    @Select({
            "select",
            "id, user_id, coin_type, asset, balance, ming_coin, task_coin, locked_coin, freeze_coin, create_time",
            "from phone_user_wallet",
            "where user_id = #{userId} and coin_type = #{coinType}"
    })
    @Results({
            @Result(column = "id", property = "id", jdbcType = JdbcType.INTEGER, id = true),
            @Result(column = "user_id", property = "userId", jdbcType = JdbcType.VARCHAR),
            @Result(column = "coin_type", property = "coinType", jdbcType = JdbcType.VARCHAR),
            @Result(column = "asset", property = "asset", jdbcType = JdbcType.VARCHAR),
            @Result(column = "balance", property = "balance", jdbcType = JdbcType.VARCHAR),
            @Result(column = "ming_coin", property = "mingCoin", jdbcType = JdbcType.VARCHAR),
            @Result(column = "task_coin", property = "taskCoin", jdbcType = JdbcType.VARCHAR),
            @Result(column = "locked_coin", property = "lockedCoin", jdbcType = JdbcType.DECIMAL),
            @Result(column = "freeze_coin", property = "freezeCoin", jdbcType = JdbcType.VARCHAR),
            @Result(column = "create_time", property = "createTime", jdbcType = JdbcType.TIMESTAMP),
    })
    UserWallet queryByUserIdAndConiType(@Param("userId") String userId, @Param("coinType") String coinType);


    @Select({
            "select",
            "id,user_id,coin_type,asset,balance,ming_coin,task_coin,locked_coin,freeze_coin,create_time",
            "from phone_user_wallet",
            "where coin_type = #{coinType} and locked_coin >= #{amount}"
    })
    @Results({
            @Result(column = "id", property = "id", jdbcType = JdbcType.INTEGER, id = true),
            @Result(column = "user_id", property = "userId", jdbcType = JdbcType.VARCHAR),
            @Result(column = "coin_type", property = "coinType", jdbcType = JdbcType.VARCHAR),
            @Result(column = "asset", property = "asset", jdbcType = JdbcType.VARCHAR),
            @Result(column = "balance", property = "balance", jdbcType = JdbcType.VARCHAR),
            @Result(column = "ming_coin", property = "mingCoin", jdbcType = JdbcType.VARCHAR),
            @Result(column = "task_coin", property = "taskCoin", jdbcType = JdbcType.VARCHAR),
            @Result(column = "locked_coin", property = "lockedCoin", jdbcType = JdbcType.DECIMAL),
            @Result(column = "create_time", property = "createTime", jdbcType = JdbcType.TIMESTAMP),
    })
    List<UserWallet> findSuperNodes(@Param("coinType") String coinType, @Param("amount") BigDecimal amount);

    @Select({
            "select",
            "id, user_id, balance, asset, coin_type, locked_coin",
            "from phone_user_wallet",
            "where locked_coin > 0 and coin_type = #{coinType}"
    })
    @Results({
            @Result(column = "id", property = "id", jdbcType = JdbcType.INTEGER, id = true),
            @Result(column = "user_id", property = "userId", jdbcType = JdbcType.VARCHAR),
            @Result(column = "balance", property = "balance", jdbcType = JdbcType.VARCHAR),
            @Result(column = "asset", property = "asset", jdbcType = JdbcType.VARCHAR),
            @Result(column = "coin_type", property = "coinType", jdbcType = JdbcType.VARCHAR),
            @Result(column = "locked_coin", property = "lockedCoin", jdbcType = JdbcType.VARCHAR),
    })
    List<UserWallet> findLockedCoinWallet(String coinType);

    //查询是否是强挖矿
    @Select({"select count(1) from phone_user_wallet uw ",
            "LEFT JOIN phone_referee r ON r.user_id = uw.user_id ",
            "WHERE r.referee_id = #{userId,jdbcType=VARCHAR} AND uw.coin_type = #{coinType} AND uw.locked_coin>=#{lockedCoin}"})
    int queryStrongAmount(UserWallet userWallet);

    @Select({
            "select",
            "id,user_id,coin_type,asset,balance,ming_coin,task_coin,locked_coin,freeze_coin,create_time",
            "from phone_user_wallet",
            "where user_id = #{userId, jdbcType=VARCHAR}"
    })
    @Results({
            @Result(column = "id", property = "id", jdbcType = JdbcType.INTEGER, id = true),
            @Result(column = "user_id", property = "userId", jdbcType = JdbcType.VARCHAR),
            @Result(column = "coin_type", property = "coinType", jdbcType = JdbcType.VARCHAR),
            @Result(column = "asset", property = "asset", jdbcType = JdbcType.VARCHAR),
            @Result(column = "balance", property = "balance", jdbcType = JdbcType.VARCHAR),
            @Result(column = "ming_coin", property = "mingCoin", jdbcType = JdbcType.VARCHAR),
            @Result(column = "task_coin", property = "taskCoin", jdbcType = JdbcType.VARCHAR),
            @Result(column = "locked_coin", property = "lockedCoin", jdbcType = JdbcType.DECIMAL),
            @Result(column = "create_time", property = "createTime", jdbcType = JdbcType.TIMESTAMP),
    })
    List<UserWallet> queryUserWallets(String userId);

    @Select({
            "select",
            "count(id) ",
            "from phone_user_wallet",
            "where coin_type = #{cointype, jdbcType=VARCHAR} and locked_coin > #{amount, jdbcType=DECIMAL}"
    })
    int queryRankByAmountAndConiType(@Param("amount") BigDecimal amount, @Param("cointype") String cointype);
    
    @Select({
        "select",
        "id,user_id,coin_type,asset,balance,ming_coin,task_coin,locked_coin,freeze_coin,create_time",
        "from phone_user_wallet",
        "where coin_type = #{cointype, jdbcType=VARCHAR} and locked_coin > 0"
	})
	@Results({
	        @Result(column = "id", property = "id", jdbcType = JdbcType.INTEGER, id = true),
	        @Result(column = "user_id", property = "userId", jdbcType = JdbcType.VARCHAR),
	        @Result(column = "coin_type", property = "coinType", jdbcType = JdbcType.VARCHAR),
	        @Result(column = "asset", property = "asset", jdbcType = JdbcType.VARCHAR),
	        @Result(column = "balance", property = "balance", jdbcType = JdbcType.VARCHAR),
	        @Result(column = "ming_coin", property = "mingCoin", jdbcType = JdbcType.VARCHAR),
	        @Result(column = "task_coin", property = "taskCoin", jdbcType = JdbcType.VARCHAR),
	        @Result(column = "locked_coin", property = "lockedCoin", jdbcType = JdbcType.DECIMAL),
	        @Result(column = "create_time", property = "createTime", jdbcType = JdbcType.TIMESTAMP),
	})
	List<UserWallet> queryAllExistsLockedCoinWallet(@Param("cointype") String cointype);
}
