package cn.com.jdyun.mapper.ext;

import cn.com.jdyun.dto.UserCredit;
import cn.com.jdyun.mapper.UserMapper;
import cn.com.jdyun.pojo.User;
import com.github.pagehelper.Page;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.type.JdbcType;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface ExtUserMapper extends UserMapper {

    @Results({
            @Result(column = "id", property = "id", jdbcType = JdbcType.VARCHAR, id = true),
            @Result(column = "phone", property = "phone", jdbcType = JdbcType.VARCHAR),
            @Result(column = "email", property = "email", jdbcType = JdbcType.VARCHAR),
            @Result(column = "nick_name", property = "nickName", jdbcType = JdbcType.VARCHAR),
            @Result(column = "head_pic", property = "headPic", jdbcType = JdbcType.VARCHAR),
            @Result(column = "referee", property = "referee", jdbcType = JdbcType.VARCHAR),
            @Result(column = "code", property = "code", jdbcType = JdbcType.VARCHAR),
            @Result(column = "flag", property = "flag", jdbcType = JdbcType.TINYINT),
            @Result(column = "admin", property = "admin", jdbcType = JdbcType.TINYINT),
            @Result(column = "level", property = "level", jdbcType = JdbcType.INTEGER),
            @Result(column = "tex_level", property = "texLevel", jdbcType = JdbcType.INTEGER),
            @Result(column = "create_time", property = "createTime", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "update_time", property = "updateTime", jdbcType = JdbcType.TIMESTAMP)
    })
    @SelectProvider(type = ExtUserSqlProvider.class, method = "selectBySelective")
    List<User> selectBySelective(User record);

    @Select("select id from phone_user where phone=#{phone}")
    String inqueryUserId(String phone);

    @Results({
            @Result(column = "id", property = "id", jdbcType = JdbcType.VARCHAR, id = true),
            @Result(column = "phone", property = "phone", jdbcType = JdbcType.VARCHAR),
            @Result(column = "email", property = "email", jdbcType = JdbcType.VARCHAR),
            @Result(column = "nick_name", property = "nickName", jdbcType = JdbcType.VARCHAR),
            @Result(column = "head_pic", property = "headPic", jdbcType = JdbcType.VARCHAR),
            @Result(column = "referee", property = "referee", jdbcType = JdbcType.VARCHAR),
            @Result(column = "code", property = "code", jdbcType = JdbcType.VARCHAR),
            @Result(column = "flag", property = "flag", jdbcType = JdbcType.TINYINT),
            @Result(column = "admin", property = "admin", jdbcType = JdbcType.TINYINT),
            @Result(column = "level", property = "level", jdbcType = JdbcType.INTEGER),
            @Result(column = "tex_level", property = "texLevel", jdbcType = JdbcType.INTEGER),
            @Result(column = "create_time", property = "createTime", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "update_time", property = "updateTime", jdbcType = JdbcType.TIMESTAMP)
    })
    @SelectProvider(type = ExtUserSqlProvider.class, method = "pageData")
    Page<User> pageData(Map<String, Object> condition);

    @Select({
            "select",
            "id, #{username,jdbcType=VARCHAR} username, nick_name, head_pic, referee, code, ",
            "flag, admin, level,tex_level, create_time, update_time",
            "from phone_user",
            "where (phone = #{username,jdbcType=VARCHAR} or email = #{username,jdbcType=VARCHAR}) and password = #{password,jdbcType=VARCHAR} "
    })
    @Results({
            @Result(column = "id", property = "id", jdbcType = JdbcType.VARCHAR, id = true),
            @Result(column = "phone", property = "phone", jdbcType = JdbcType.VARCHAR),
            @Result(column = "email", property = "email", jdbcType = JdbcType.VARCHAR),
            @Result(column = "nick_name", property = "nickName", jdbcType = JdbcType.VARCHAR),
            @Result(column = "head_pic", property = "headPic", jdbcType = JdbcType.VARCHAR),
            @Result(column = "referee", property = "referee", jdbcType = JdbcType.VARCHAR),
            @Result(column = "code", property = "code", jdbcType = JdbcType.VARCHAR),
            @Result(column = "flag", property = "flag", jdbcType = JdbcType.TINYINT),
            @Result(column = "admin", property = "admin", jdbcType = JdbcType.TINYINT),
            @Result(column = "level", property = "level", jdbcType = JdbcType.INTEGER),
            @Result(column = "tex_level", property = "texLevel", jdbcType = JdbcType.INTEGER),
            @Result(column = "create_time", property = "createTime", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "update_time", property = "updateTime", jdbcType = JdbcType.TIMESTAMP)
    })
    UserCredit login(@Param("username") String username, @Param("password") String password);

    @Select({
            "select id",
            "from phone_user",
            "where code = #{refereeCode,jdbcType=VARCHAR} "
    })
    String selectByRefereeCode(String refereeCode);

    @Select({
            "select max(id)",
            "from phone_user"
    })
    String selectLastId();

    @Update("update phone_user set password = #{password} where email = #{email}")
    int updateUserPwdEmail(@Param("email") String email, @Param("password") String password);

    @Update("update phone_user set password = #{password} where phone = #{phone}")
    int updateUserPwdPhone(@Param("phone") String phone, @Param("password") String password);

    @Update("update phone_manage set password = #{password} where phone = #{phone}")
    int updateManagePwdPhone(@Param("phone") String phone, @Param("password") String password);

    @Select("select id from phone_manage where phone = #{phone}")
    String findManageId(User user);

    @Update("update phone_user set flag = #{flag} where id = #{userId}")
    int updateUserFlag(@Param("userId") String userId, @Param("flag") Byte flag);

    @Select("select code from phone_user where code = #{code}")
    String inqueryRefereeCode(String code);

    @Select({
            "select",
            "id, #{username,jdbcType=VARCHAR} username, nick_name, head_pic, referee, code, ",
            "flag, admin, level,tex_level,create_time, update_time",
            "from phone_user",
            "where flag = 1 "
    })
    @Results({
            @Result(column = "id", property = "id", jdbcType = JdbcType.VARCHAR, id = true),
            @Result(column = "phone", property = "phone", jdbcType = JdbcType.VARCHAR),
            @Result(column = "email", property = "email", jdbcType = JdbcType.VARCHAR),
            @Result(column = "nick_name", property = "nickName", jdbcType = JdbcType.VARCHAR),
            @Result(column = "head_pic", property = "headPic", jdbcType = JdbcType.VARCHAR),
            @Result(column = "referee", property = "referee", jdbcType = JdbcType.VARCHAR),
            @Result(column = "code", property = "code", jdbcType = JdbcType.VARCHAR),
            @Result(column = "flag", property = "flag", jdbcType = JdbcType.TINYINT),
            @Result(column = "admin", property = "admin", jdbcType = JdbcType.TINYINT),
            @Result(column = "level", property = "level", jdbcType = JdbcType.INTEGER),
            @Result(column = "tex_level", property = "texLevel", jdbcType = JdbcType.INTEGER),
            @Result(column = "create_time", property = "createTime", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "update_time", property = "updateTime", jdbcType = JdbcType.TIMESTAMP)
    })
    List<User> allRelieveLock();

    @Update("update phone_user set flag = 0 where id = #{userId}")
    int updateRelieveLock(String userId);
    
    @Select({
        "select",
        "id",
        "from phone_user",
        "where level = #{level}  "
    })
	Set<String> queryUserIdByLevel(@Param("level")int level);

    @Select({
        "select",
        "id",
        "from phone_user",
        "where tex_level = #{texLevel}  "
    })
	Set<String> queryUserIdByTexLevel(@Param("texLevel")int texLevel);
}
