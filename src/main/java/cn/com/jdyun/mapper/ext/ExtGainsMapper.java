package cn.com.jdyun.mapper.ext;
import cn.com.jdyun.dto.GainsDTO;
import cn.com.jdyun.mapper.GainsMapper;
import cn.com.jdyun.pojo.Gains;
import com.github.pagehelper.Page;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.type.JdbcType;

import java.util.List;


public interface ExtGainsMapper extends GainsMapper {

    @Select({
            "select",
            "id, date, vested, rate,coin_type, times, coin_type,create_time, update_time",
            "from phone_gains",
            "where date = #{date,jdbcType=VARCHAR} and user_id = #{userId,jdbcType=VARCHAR} and coin_type = #{coinType} limit 1"
    })
    @Results({
            @Result(column = "id", property = "id", jdbcType = JdbcType.INTEGER, id = true),
            @Result(column = "user_id", property = "userId", jdbcType = JdbcType.VARCHAR),
            @Result(column = "date", property = "date", jdbcType = JdbcType.VARCHAR),
            @Result(column = "vested", property = "vested", jdbcType = JdbcType.VARCHAR),
            @Result(column = "rate", property = "rate", jdbcType = JdbcType.VARCHAR),
            @Result(column = "times", property = "times", jdbcType = JdbcType.INTEGER),
            @Result(column = "coin_type", property = "coinType", jdbcType = JdbcType.VARCHAR),
            @Result(column = "create_time", property = "createTime", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "update_time", property = "updateTime", jdbcType = JdbcType.TIMESTAMP)
    })
    Gains selectByDateAndUserId(@Param("date") String date, @Param("userId") String userId,@Param("coinType") String coinType);

    @Select({
            "select",
            "id, date, vested, rate,coin_type, times, create_time, update_time",
            "from phone_gains",
            "where user_id = #{userId,jdbcType=VARCHAR} ",
            "order by create_time desc"
    })
    @Results({
            @Result(column = "id", property = "id", jdbcType = JdbcType.INTEGER, id = true),
            @Result(column = "user_id", property = "userId", jdbcType = JdbcType.VARCHAR),
            @Result(column = "date", property = "date", jdbcType = JdbcType.VARCHAR),
            @Result(column = "vested", property = "vested", jdbcType = JdbcType.VARCHAR),
            @Result(column = "rate", property = "rate", jdbcType = JdbcType.VARCHAR),
            @Result(column = "coin_type", property = "coinType", jdbcType = JdbcType.VARCHAR),
            @Result(column = "times", property = "times", jdbcType = JdbcType.INTEGER),
            @Result(column = "create_time", property = "createTime", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "update_time", property = "updateTime", jdbcType = JdbcType.TIMESTAMP)
    })
    Page<Gains> pageData(@Param("userId") String userId);

    @Select({
            "select",
            "g.id, g.user_id, g.date,g.coin_type, g.vested, r.context",
            "from phone_gains g",
            "LEFT JOIN phone_referee r ON g.user_id = r.user_id",
            "WHERE g.date = #{date,jdbcType=VARCHAR} and g.coin_type = #{coinType,jdbcType = VARCHAR}  AND r.user_id != #{userId,jdbcType=VARCHAR} and r.context LIKE CONCAT('%',#{userId,jdbcType=VARCHAR},'%')"
    })
    @Results({
            @Result(column = "id", property = "id", jdbcType = JdbcType.INTEGER, id = true),
            @Result(column = "user_id", property = "userId", jdbcType = JdbcType.VARCHAR),
            @Result(column = "date", property = "date", jdbcType = JdbcType.VARCHAR),
            @Result(column = "coin_type", property = "coinType", jdbcType = JdbcType.VARCHAR),
            @Result(column = "vested", property = "vested", jdbcType = JdbcType.VARCHAR),
            @Result(column = "context", property = "context", jdbcType = JdbcType.VARCHAR)
    })
    List<GainsDTO> queryPartnerRelation(@Param("userId") String userId, @Param("coinType") String coinType, @Param("date") String date);
}
