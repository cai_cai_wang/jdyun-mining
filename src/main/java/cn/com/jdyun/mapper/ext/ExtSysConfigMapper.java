package cn.com.jdyun.mapper.ext;

import cn.com.jdyun.mapper.LockCoinSqlProvider;
import cn.com.jdyun.mapper.SysConfigMapper;
import cn.com.jdyun.pojo.LockCoin;
import cn.com.jdyun.pojo.SysConfig;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.UpdateProvider;
import org.apache.ibatis.type.JdbcType;

import java.util.List;


public interface ExtSysConfigMapper extends SysConfigMapper {

    @Select({
            "select",
            "id, code, code_name, init_value, create_time, update_time",
            "from phone_sys_config",
            "where code = #{code,jdbcType=VARCHAR}"
    })
    @Results({
            @Result(column = "id", property = "id", jdbcType = JdbcType.INTEGER, id = true),
            @Result(column = "code", property = "code", jdbcType = JdbcType.VARCHAR),
            @Result(column = "code_name", property = "codeName", jdbcType = JdbcType.VARCHAR),
            @Result(column = "init_value", property = "initValue", jdbcType = JdbcType.VARCHAR),
            @Result(column = "create_time", property = "createTime", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "update_time", property = "updateTime", jdbcType = JdbcType.TIMESTAMP)
    })
    SysConfig selectByCode(@Param("code") String code);

    @UpdateProvider(type = ExtSysConfigProvider.class, method = "updateParameterValues")
    int updateParameterValues(SysConfig sysConfig);

    @Select({
            "select",
            "id, code, code_name, init_value, create_time, update_time, operator_id",
            "from phone_sys_config",
            "where init_value is not null"
    })
    @Results({
            @Result(column = "id", property = "id", jdbcType = JdbcType.INTEGER, id = true),
            @Result(column = "code", property = "code", jdbcType = JdbcType.VARCHAR),
            @Result(column = "code_name", property = "codeName", jdbcType = JdbcType.VARCHAR),
            @Result(column = "init_value", property = "initValue", jdbcType = JdbcType.VARCHAR),
            @Result(column = "create_time", property = "createTime", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "update_time", property = "updateTime", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "operator_id", property = "operatorId", jdbcType = JdbcType.VARCHAR)
    })
    List<SysConfig> selectAll();

}
