package cn.com.jdyun.mapper;

import cn.com.jdyun.pojo.SysConfig;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.annotations.UpdateProvider;
import org.apache.ibatis.type.JdbcType;

public interface SysConfigMapper {
    @Delete({
            "delete from phone_sys_config",
            "where id = #{id,jdbcType=INTEGER}"
    })
    int deleteByPrimaryKey(Integer id);

    @Insert({
            "insert into phone_sys_config (code, code_name, ",
            "init_value, create_time, ",
            "update_time, operator_id)",
            "values (#{code,jdbcType=VARCHAR}, #{codeName,jdbcType=VARCHAR}, ",
            "#{initValue,jdbcType=VARCHAR}, #{createTime,jdbcType=TIMESTAMP}, ",
            "#{updateTime,jdbcType=TIMESTAMP}, #{operatorId,jdbcType=VARCHAR})"
    })
    @SelectKey(statement = "SELECT LAST_INSERT_ID()", keyProperty = "id", before = false, resultType = Integer.class)
    int insert(SysConfig record);

    @InsertProvider(type = SysConfigSqlProvider.class, method = "insertSelective")
    @SelectKey(statement = "SELECT LAST_INSERT_ID()", keyProperty = "id", before = false, resultType = Integer.class)
    int insertSelective(SysConfig record);

    @Select({
            "select",
            "id, code, code_name, init_value, create_time, update_time, operator_id",
            "from phone_sys_config",
            "where id = #{id,jdbcType=INTEGER}"
    })
    @Results({
            @Result(column = "id", property = "id", jdbcType = JdbcType.INTEGER, id = true),
            @Result(column = "code", property = "code", jdbcType = JdbcType.VARCHAR),
            @Result(column = "code_name", property = "codeName", jdbcType = JdbcType.VARCHAR),
            @Result(column = "init_value", property = "initValue", jdbcType = JdbcType.VARCHAR),
            @Result(column = "create_time", property = "createTime", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "update_time", property = "updateTime", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "operator_id", property = "operatorId", jdbcType = JdbcType.VARCHAR)
    })
    SysConfig selectByPrimaryKey(Integer id);

    @UpdateProvider(type = SysConfigSqlProvider.class, method = "updateByPrimaryKeySelective")
    int updateByPrimaryKeySelective(SysConfig record);

    @Update({
            "update phone_sys_config",
            "set code = #{code,jdbcType=VARCHAR},",
            "code_name = #{codeName,jdbcType=VARCHAR},",
            "init_value = #{initValue,jdbcType=VARCHAR},",
            "create_time = #{createTime,jdbcType=TIMESTAMP},",
            "update_time = #{updateTime,jdbcType=TIMESTAMP},",
            "operator_id = #{operatorId,jdbcType=VARCHAR}",
            "where id = #{id,jdbcType=INTEGER}"
    })
    int updateByPrimaryKey(SysConfig record);
}