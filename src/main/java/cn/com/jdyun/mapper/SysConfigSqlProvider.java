package cn.com.jdyun.mapper;

import cn.com.jdyun.pojo.SysConfig;
import org.apache.ibatis.jdbc.SQL;

public class SysConfigSqlProvider {

    public String insertSelective(SysConfig record) {
        SQL sql = new SQL();
        sql.INSERT_INTO("phone_sys_config");

        if (record.getCode() != null) {
            sql.VALUES("code", "#{code,jdbcType=VARCHAR}");
        }

        if (record.getCodeName() != null) {
            sql.VALUES("code_name", "#{codeName,jdbcType=VARCHAR}");
        }

        if (record.getInitValue() != null) {
            sql.VALUES("init_value", "#{initValue,jdbcType=VARCHAR}");
        }

        if (record.getCreateTime() != null) {
            sql.VALUES("create_time", "#{createTime,jdbcType=TIMESTAMP}");
        }

        if (record.getUpdateTime() != null) {
            sql.VALUES("update_time", "#{updateTime,jdbcType=TIMESTAMP}");
        }

        if (record.getOperatorId() != null) {
            sql.VALUES("operator_id", "#{operatorId,jdbcType=VARCHAR}");
        }

        return sql.toString();
    }

    public String updateByPrimaryKeySelective(SysConfig record) {
        SQL sql = new SQL();
        sql.UPDATE("phone_sys_config");

        if (record.getCode() != null) {
            sql.SET("code = #{code,jdbcType=VARCHAR}");
        }

        if (record.getCodeName() != null) {
            sql.SET("code_name = #{codeName,jdbcType=VARCHAR}");
        }

        if (record.getInitValue() != null) {
            sql.SET("init_value = #{initValue,jdbcType=VARCHAR}");
        }

        if (record.getCreateTime() != null) {
            sql.SET("create_time = #{createTime,jdbcType=TIMESTAMP}");
        }

        if (record.getUpdateTime() != null) {
            sql.SET("update_time = #{updateTime,jdbcType=TIMESTAMP}");
        }

        if (record.getOperatorId() != null) {
            sql.SET("operator_id = #{operatorId,jdbcType=VARCHAR}");
        }

        sql.WHERE("id = #{id,jdbcType=INTEGER}");

        return sql.toString();
    }
}