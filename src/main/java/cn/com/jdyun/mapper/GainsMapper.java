package cn.com.jdyun.mapper;

import cn.com.jdyun.pojo.Gains;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.annotations.UpdateProvider;
import org.apache.ibatis.type.JdbcType;

public interface GainsMapper {
    @Delete({
            "delete from phone_gains",
            "where id = #{id,jdbcType=INTEGER}"
    })
    int deleteByPrimaryKey(Integer id);

    @Insert({
            "insert into phone_gains (user_id, date, ",
            "vested, rate,coin_type, times, ",
            "create_time, update_time)",
            "values (#{userId,jdbcType=VARCHAR}, #{date,jdbcType=VARCHAR}, ",
            "#{vested,jdbcType=VARCHAR}, #{rate,jdbcType=VARCHAR}, #{coinType,jdbcType=VARCHAR},#{times,jdbcType=INTEGER}, ",
            "#{createTime,jdbcType=TIMESTAMP}, #{updateTime,jdbcType=TIMESTAMP})"
    })
    @SelectKey(statement = "SELECT LAST_INSERT_ID()", keyProperty = "id", before = false, resultType = Integer.class)
    int insert(Gains record);

    @InsertProvider(type = GainsSqlProvider.class, method = "insertSelective")
    @SelectKey(statement = "SELECT LAST_INSERT_ID()", keyProperty = "id", before = false, resultType = Integer.class)
    int insertSelective(Gains record);

    @Select({
            "select",
            "id, user_id, date, vested, rate, times,coin_type, create_time, update_time",
            "from phone_gains",
            "where id = #{id,jdbcType=INTEGER}"
    })
    @Results({
            @Result(column = "id", property = "id", jdbcType = JdbcType.INTEGER, id = true),
            @Result(column = "user_id", property = "userId", jdbcType = JdbcType.VARCHAR),
            @Result(column = "date", property = "date", jdbcType = JdbcType.VARCHAR),
            @Result(column = "vested", property = "vested", jdbcType = JdbcType.VARCHAR),
            @Result(column = "rate", property = "rate", jdbcType = JdbcType.VARCHAR),
            @Result(column = "times", property = "times", jdbcType = JdbcType.INTEGER),
            @Result(column = "coin_type", property = "coinType", jdbcType = JdbcType.VARCHAR),
            @Result(column = "create_time", property = "createTime", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "update_time", property = "updateTime", jdbcType = JdbcType.TIMESTAMP)
    })
    Gains selectByPrimaryKey(Integer id);

    @UpdateProvider(type = GainsSqlProvider.class, method = "updateByPrimaryKeySelective")
    int updateByPrimaryKeySelective(Gains record);

    @Update({
            "update phone_gains",
            "set user_id = #{userId,jdbcType=VARCHAR},",
            "date = #{date,jdbcType=VARCHAR},",
            "vested = #{vested,jdbcType=VARCHAR},",
            "rate = #{rate,jdbcType=VARCHAR},",
            "times = #{times,jdbcType=INTEGER},",
            "coin_type = #{coinType,jdbcType=VARCHAR},",
            "create_time = #{createTime,jdbcType=TIMESTAMP},",
            "update_time = #{updateTime,jdbcType=TIMESTAMP}",
            "where id = #{id,jdbcType=INTEGER}"
    })
    int updateByPrimaryKey(Gains record);
}