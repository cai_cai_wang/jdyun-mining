package cn.com.jdyun.mapper;

import cn.com.jdyun.pojo.Lottery;
import org.apache.ibatis.jdbc.SQL;

public class LotterySqlProvider {

    public String insertSelective(Lottery record) {
        SQL sql = new SQL();
        sql.INSERT_INTO("phone_lottery");

        if (record.getUserId() != null) {
            sql.VALUES("user_id", "#{userId,jdbcType=VARCHAR}");
        }

        if (record.getRechargeId() != null) {
            sql.VALUES("recharge_id", "#{rechargeId,jdbcType=INTEGER}");
        }

        if (record.getRate() != null) {
            sql.VALUES("rate", "#{rate,jdbcType=VARCHAR}");
        }

        if (record.getCoinAmount() != null) {
            sql.VALUES("coin_amount", "#{coinAmount,jdbcType=VARCHAR}");
        }

        if (record.getCoinType() != null) {
            sql.VALUES("coin_type", "#{coinType,jdbcType=VARCHAR}");
        }

        if (record.getCreateTime() != null) {
            sql.VALUES("create_time", "#{createTime,jdbcType=TIMESTAMP}");
        }

        return sql.toString();
    }

    public String updateByPrimaryKeySelective(Lottery record) {
        SQL sql = new SQL();
        sql.UPDATE("phone_lottery");

        if (record.getUserId() != null) {
            sql.SET("user_id = #{userId,jdbcType=VARCHAR}");
        }

        if (record.getRechargeId() != null) {
            sql.SET("recharge_id = #{rechargeId,jdbcType=INTEGER}");
        }

        if (record.getRate() != null) {
            sql.SET("rate = #{rate,jdbcType=VARCHAR}");
        }

        if (record.getCoinAmount() != null) {
            sql.SET("coin_amount = #{coinAmount,jdbcType=VARCHAR}");
        }

        if (record.getCoinType() != null) {
            sql.SET("coin_type = #{coinType,jdbcType=VARCHAR}");
        }

        if (record.getCreateTime() != null) {
            sql.SET("create_time = #{createTime,jdbcType=TIMESTAMP}");
        }

        sql.WHERE("id = #{id,jdbcType=INTEGER}");

        return sql.toString();
    }
}