package cn.com.jdyun.mapper;

import cn.com.jdyun.pojo.MinerLog;
import org.apache.ibatis.jdbc.SQL;

public class MinerLogSqlProvider {

    public String insertSelective(MinerLog record) {
        SQL sql = new SQL();
        sql.INSERT_INTO("phone_miner_log");

        if (record.getUserId() != null) {
            sql.VALUES("user_id", "#{userId,jdbcType=VARCHAR}");
        }

        if (record.getRate() != null) {
            sql.VALUES("rate", "#{rate,jdbcType=VARCHAR}");
        }

        if (record.getCp() != null) {
            sql.VALUES("cp", "#{cp,jdbcType=VARCHAR}");
        }

        if (record.getCurCp() != null) {
            sql.VALUES("cur_cp", "#{curCp,jdbcType=VARCHAR}");
        }

        if (record.getMemo() != null) {
            sql.VALUES("memo", "#{memo,jdbcType=VARCHAR}");
        }

        if (record.getCreateTime() != null) {
            sql.VALUES("create_time", "#{createTime,jdbcType=TIMESTAMP}");
        }

        return sql.toString();
    }

    public String updateByPrimaryKeySelective(MinerLog record) {
        SQL sql = new SQL();
        sql.UPDATE("phone_miner_log");

        if (record.getUserId() != null) {
            sql.SET("user_id = #{userId,jdbcType=VARCHAR}");
        }

        if (record.getRate() != null) {
            sql.SET("rate = #{rate,jdbcType=VARCHAR}");
        }

        if (record.getCp() != null) {
            sql.SET("cp = #{cp,jdbcType=VARCHAR}");
        }

        if (record.getCurCp() != null) {
            sql.SET("cur_cp = #{curCp,jdbcType=VARCHAR}");
        }

        if (record.getMemo() != null) {
            sql.SET("memo = #{memo,jdbcType=VARCHAR}");
        }

        if (record.getCreateTime() != null) {
            sql.SET("create_time = #{createTime,jdbcType=TIMESTAMP}");
        }

        sql.WHERE("id = #{id,jdbcType=INTEGER}");

        return sql.toString();
    }
}