package cn.com.jdyun.mapper;

import cn.com.jdyun.pojo.TransferRecords;
import org.apache.ibatis.jdbc.SQL;

public class TransferRecordsSqlProvider {

    public String insertSelective(TransferRecords record) {
        SQL sql = new SQL();
        sql.INSERT_INTO("phone_transfer_records");

        if (record.getOrderId() != null) {
            sql.VALUES("order_id", "#{orderId,jdbcType=VARCHAR}");
        }

        if (record.getTransferType() != null) {
            sql.VALUES("transfer_type", "#{transferType,jdbcType=TINYINT}");
        }

        if (record.getAmount() != null) {
            sql.VALUES("amount", "#{amount,jdbcType=VARCHAR}");
        }

        if (record.getFee() != null) {
            sql.VALUES("fee", "#{fee,jdbcType=VARCHAR}");
        }

        if (record.getCoinType() != null) {
            sql.VALUES("coin_type", "#{coinType,jdbcType=VARCHAR}");
        }

        if (record.getFromUser() != null) {
            sql.VALUES("from_user", "#{fromUser,jdbcType=VARCHAR}");
        }

        if (record.getToUser() != null) {
            sql.VALUES("to_user", "#{toUser,jdbcType=VARCHAR}");
        }

        if (record.getMemo() != null) {
            sql.VALUES("memo", "#{memo,jdbcType=VARCHAR}");
        }

        if (record.getCreateTime() != null) {
            sql.VALUES("create_time", "#{createTime,jdbcType=TIMESTAMP}");
        }

        return sql.toString();
    }

    public String updateByPrimaryKeySelective(TransferRecords record) {
        SQL sql = new SQL();
        sql.UPDATE("phone_transfer_records");

        if (record.getOrderId() != null) {
            sql.SET("order_id = #{orderId,jdbcType=VARCHAR}");
        }

        if (record.getTransferType() != null) {
            sql.SET("transfer_type = #{transferType,jdbcType=TINYINT}");
        }

        if (record.getAmount() != null) {
            sql.SET("amount = #{amount,jdbcType=VARCHAR}");
        }

        if (record.getFee() != null) {
            sql.SET("fee = #{fee,jdbcType=VARCHAR}");
        }

        if (record.getCoinType() != null) {
            sql.SET("coin_type = #{coinType,jdbcType=VARCHAR}");
        }

        if (record.getFromUser() != null) {
            sql.SET("from_user = #{fromUser,jdbcType=VARCHAR}");
        }

        if (record.getToUser() != null) {
            sql.SET("to_user = #{toUser,jdbcType=VARCHAR}");
        }

        if (record.getMemo() != null) {
            sql.SET("memo = #{memo,jdbcType=VARCHAR}");
        }

        if (record.getCreateTime() != null) {
            sql.SET("create_time = #{createTime,jdbcType=TIMESTAMP}");
        }

        sql.WHERE("id = #{id,jdbcType=INTEGER}");

        return sql.toString();
    }
}