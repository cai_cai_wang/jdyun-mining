package cn.com.jdyun.mapper;

import cn.com.jdyun.pojo.LockCoin;
import org.apache.ibatis.jdbc.SQL;

public class LockCoinSqlProvider {

    public String insertSelective(LockCoin record) {
        SQL sql = new SQL();
        sql.INSERT_INTO("phone_lock_coin");

        if (record.getUserId() != null) {
            sql.VALUES("user_id", "#{userId,jdbcType=VARCHAR}");
        }

        if (record.getInitLockCoinAmount() != null) {
            sql.VALUES("init_lock_coin_amount", "#{initLockCoinAmount,jdbcType=DECIMAL}");
        }

        if (record.getLockCoinAmount() != null) {
            sql.VALUES("lock_coin_amount", "#{lockCoinAmount,jdbcType=DECIMAL}");
        }

        if (record.getIncome() != null) {
            sql.VALUES("income", "#{income,jdbcType=DECIMAL}");
        }

        if (record.getCoinType() != null) {
            sql.VALUES("coin_type", "#{coinType,jdbcType=VARCHAR}");
        }

        if (record.getSequenceId() != null) {
            sql.VALUES("sequence_id", "#{sequenceId,jdbcType=VARCHAR}");
        }

        if (record.getCreateTime() != null) {
            sql.VALUES("create_time", "#{createTime,jdbcType=TIMESTAMP}");
        }

        if (record.getUpdateTime() != null) {
            sql.VALUES("update_time", "#{updateTime,jdbcType=TIMESTAMP}");
        }

        return sql.toString();
    }

    public String updateByPrimaryKeySelective(LockCoin record) {
        SQL sql = new SQL();
        sql.UPDATE("phone_lock_coin");

        if (record.getUserId() != null) {
            sql.SET("user_id = #{userId,jdbcType=VARCHAR}");
        }

        if (record.getInitLockCoinAmount() != null) {
            sql.SET("init_lock_coin_amount = #{initLockCoinAmount,jdbcType=DECIMAL}");
        }

        if (record.getLockCoinAmount() != null) {
            sql.SET("lock_coin_amount = #{lockCoinAmount,jdbcType=DECIMAL}");
        }

        if (record.getIncome() != null) {
            sql.SET("income = #{income,jdbcType=DECIMAL}");
        }

        if (record.getCoinType() != null) {
            sql.SET("coin_type = #{coinType,jdbcType=VARCHAR}");
        }

        if (record.getSequenceId() != null) {
            sql.SET("sequence_id = #{sequenceId,jdbcType=VARCHAR}");
        }

        if (record.getCreateTime() != null) {
            sql.SET("create_time = #{createTime,jdbcType=TIMESTAMP}");
        }

        if (record.getUpdateTime() != null) {
            sql.SET("update_time = #{updateTime,jdbcType=TIMESTAMP}");
        }

        sql.WHERE("id = #{id,jdbcType=INTEGER}");

        return sql.toString();
    }
}