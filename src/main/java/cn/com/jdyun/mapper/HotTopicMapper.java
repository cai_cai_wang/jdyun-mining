package cn.com.jdyun.mapper;

import cn.com.jdyun.pojo.HotTopic;
import com.github.pagehelper.Page;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.annotations.UpdateProvider;
import org.apache.ibatis.type.JdbcType;

import java.util.List;
import java.util.Map;

public interface HotTopicMapper {
    @Delete({
            "delete from phone_hot_topic",
            "where id = #{id,jdbcType=INTEGER}"
    })
    int deleteByPrimaryKey(Integer id);

    @Insert({
            "insert into phone_hot_topic (title, content, ",
            "date, show_order, ",
            "flag, create_time, ",
            "update_time)",
            "values (#{title,jdbcType=VARCHAR}, #{content,jdbcType=VARCHAR}, ",
            "#{date,jdbcType=VARCHAR}, #{showOrder,jdbcType=INTEGER}, ",
            "#{flag,jdbcType=TINYINT}, #{createTime,jdbcType=TIMESTAMP}, ",
            "#{updateTime,jdbcType=TIMESTAMP})"
    })
    @SelectKey(statement = "SELECT LAST_INSERT_ID()", keyProperty = "id", before = false, resultType = Integer.class)
    int insert(HotTopic record);

    @InsertProvider(type = HotTopicSqlProvider.class, method = "insertSelective")
    @SelectKey(statement = "SELECT LAST_INSERT_ID()", keyProperty = "id", before = false, resultType = Integer.class)
    int insertSelective(HotTopic record);

    @Select({
            "select",
            "id, title, content,  show_order, create_time, update_time",
            "from phone_hot_topic",
            "where id = #{id,jdbcType=INTEGER}"
    })
    @Results({
            @Result(column = "id", property = "id", jdbcType = JdbcType.INTEGER, id = true),
            @Result(column = "title", property = "title", jdbcType = JdbcType.VARCHAR),
            @Result(column = "content", property = "content", jdbcType = JdbcType.VARCHAR),
            @Result(column = "show_order", property = "showOrder", jdbcType = JdbcType.INTEGER),
            @Result(column = "create_time", property = "createTime", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "update_time", property = "updateTime", jdbcType = JdbcType.TIMESTAMP)
    })
    HotTopic selectByPrimaryKey(Integer id);

    @UpdateProvider(type = HotTopicSqlProvider.class, method = "updateByPrimaryKeySelective")
    int updateByPrimaryKeySelective(HotTopic record);

    @Update({
            "update phone_hot_topic",
            "set title = #{title,jdbcType=VARCHAR},",
            "content = #{content,jdbcType=VARCHAR},",
            "show_order = #{showOrder,jdbcType=INTEGER},",
            "create_time = #{createTime,jdbcType=TIMESTAMP},",
            "update_time = #{updateTime,jdbcType=TIMESTAMP}",
            "where id = #{id,jdbcType=INTEGER}"
    })
    int updateByPrimaryKey(HotTopic record);

    @Results({
            @Result(column = "id", property = "id", jdbcType = JdbcType.INTEGER, id = true),
            @Result(column = "title", property = "title", jdbcType = JdbcType.VARCHAR),
            @Result(column = "content", property = "content", jdbcType = JdbcType.VARCHAR),
            @Result(column = "show_order", property = "showOrder", jdbcType = JdbcType.INTEGER),
            @Result(column = "create_time", property = "createTime", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "update_time", property = "updateTime", jdbcType = JdbcType.TIMESTAMP)
    })
    @SelectProvider(type = HotTopicSqlProvider.class, method = "pageData")
    Page<HotTopic> pageData(Map<String, Object> condition);

    @Select({
            "select",
            "id, title, show_order, create_time, update_time",
            "from phone_hot_topic",
            " order by show_order desc"
    })
    @Results({
            @Result(column = "id", property = "id", jdbcType = JdbcType.INTEGER, id = true),
            @Result(column = "title", property = "title", jdbcType = JdbcType.VARCHAR),
            @Result(column = "show_order", property = "showOrder", jdbcType = JdbcType.INTEGER),
            @Result(column = "create_time", property = "createTime", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "update_time", property = "updateTime", jdbcType = JdbcType.TIMESTAMP)
    })
    Page<HotTopic> pageDataForIndex();

    @Select({
            "select",
            "id, title, content,show_order",
            "from phone_hot_topic",
            "order by show_order desc",
            "limit #{number}"
    })
    @Results({
            @Result(column = "id", property = "id", jdbcType = JdbcType.INTEGER, id = true),
            @Result(column = "title", property = "title", jdbcType = JdbcType.VARCHAR),
            @Result(column = "content", property = "content", jdbcType = JdbcType.VARCHAR),
            @Result(column = "show_order", property = "showOrder", jdbcType = JdbcType.INTEGER)
    })
    List<HotTopic> lastHotTopic(int number);

    @Select({
            "select",
            "max(show_order)",
            "from phone_hot_topic",
    })
    int selectTopIndex();
}