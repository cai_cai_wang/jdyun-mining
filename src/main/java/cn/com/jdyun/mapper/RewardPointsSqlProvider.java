package cn.com.jdyun.mapper;

import cn.com.jdyun.pojo.RewardPoints;
import org.apache.ibatis.jdbc.SQL;

public class RewardPointsSqlProvider {

    public String insertSelective(RewardPoints record) {
        SQL sql = new SQL();
        sql.INSERT_INTO("phone_reward_points");

        if (record.getUserId() != null) {
            sql.VALUES("user_id", "#{userId,jdbcType=VARCHAR}");
        }

        if (record.getCoinAmount() != null) {
            sql.VALUES("coin_amount", "#{coinAmount,jdbcType=VARCHAR}");
        }

        if (record.getCoinType() != null) {
            sql.VALUES("coin_type", "#{coinType,jdbcType=VARCHAR}");
        }

        if (record.getTimes() != null) {
            sql.VALUES("times", "#{times,jdbcType=INTEGER}");
        }

        if (record.getLeaves() != null) {
            sql.VALUES("leaves", "#{leaves,jdbcType=INTEGER}");
        }

        if (record.getLeavesAmount() != null) {
            sql.VALUES("leaves_amount", "#{leavesAmount,jdbcType=VARCHAR}");
        }

        if (record.getCreateTime() != null) {
            sql.VALUES("create_time", "#{createTime,jdbcType=TIMESTAMP}");
        }

        if (record.getUpdateTime() != null) {
            sql.VALUES("update_time", "#{updateTime,jdbcType=TIMESTAMP}");
        }

        return sql.toString();
    }

    public String updateByPrimaryKeySelective(RewardPoints record) {
        SQL sql = new SQL();
        sql.UPDATE("phone_reward_points");

        if (record.getUserId() != null) {
            sql.SET("user_id = #{userId,jdbcType=VARCHAR}");
        }

        if (record.getCoinAmount() != null) {
            sql.SET("coin_amount = #{coinAmount,jdbcType=VARCHAR}");
        }

        if (record.getCoinType() != null) {
            sql.SET("coin_type = #{coinType,jdbcType=VARCHAR}");
        }

        if (record.getTimes() != null) {
            sql.SET("times = #{times,jdbcType=INTEGER}");
        }

        if (record.getLeaves() != null) {
            sql.SET("leaves = #{leaves,jdbcType=INTEGER}");
        }

        if (record.getLeavesAmount() != null) {
            sql.SET("leaves_amount = #{leavesAmount,jdbcType=VARCHAR}");
        }

        if (record.getCreateTime() != null) {
            sql.SET("create_time = #{createTime,jdbcType=TIMESTAMP}");
        }

        if (record.getUpdateTime() != null) {
            sql.SET("update_time = #{updateTime,jdbcType=TIMESTAMP}");
        }

        sql.WHERE("id = #{id,jdbcType=INTEGER}");

        return sql.toString();
    }
}