package cn.com.jdyun.mapper;

import cn.com.jdyun.pojo.SysConfigLog;
import org.apache.ibatis.jdbc.SQL;

public class SysConfigLogSqlProvider {

    public String insertSelective(SysConfigLog record) {
        SQL sql = new SQL();
        sql.INSERT_INTO("phone_sys_config_log");

        if (record.getCode() != null) {
            sql.VALUES("code", "#{code,jdbcType=VARCHAR}");
        }

        if (record.getOldValue() != null) {
            sql.VALUES("old_value", "#{oldValue,jdbcType=VARCHAR}");
        }

        if (record.getNewValue() != null) {
            sql.VALUES("new_value", "#{newValue,jdbcType=VARCHAR}");
        }

        if (record.getCreateTime() != null) {
            sql.VALUES("create_time", "#{createTime,jdbcType=TIMESTAMP}");
        }

        if (record.getOperator() != null) {
            sql.VALUES("operator", "#{operator,jdbcType=VARCHAR}");
        }

        return sql.toString();
    }

    public String updateByPrimaryKeySelective(SysConfigLog record) {
        SQL sql = new SQL();
        sql.UPDATE("phone_sys_config_log");

        if (record.getCode() != null) {
            sql.SET("code = #{code,jdbcType=VARCHAR}");
        }

        if (record.getOldValue() != null) {
            sql.SET("old_value = #{oldValue,jdbcType=VARCHAR}");
        }

        if (record.getNewValue() != null) {
            sql.SET("new_value = #{newValue,jdbcType=VARCHAR}");
        }

        if (record.getCreateTime() != null) {
            sql.SET("create_time = #{createTime,jdbcType=TIMESTAMP}");
        }

        if (record.getOperator() != null) {
            sql.SET("operator = #{operator,jdbcType=VARCHAR}");
        }

        sql.WHERE("id = #{id,jdbcType=INTEGER}");

        return sql.toString();
    }
}