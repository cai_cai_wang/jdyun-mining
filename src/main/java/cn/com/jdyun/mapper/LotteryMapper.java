package cn.com.jdyun.mapper;

import cn.com.jdyun.pojo.Lottery;
import com.github.pagehelper.Page;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.annotations.UpdateProvider;
import org.apache.ibatis.type.JdbcType;

public interface LotteryMapper {
    @Delete({
            "delete from phone_lottery",
            "where id = #{id,jdbcType=INTEGER}"
    })
    int deleteByPrimaryKey(Integer id);

    @Insert({
            "insert into phone_lottery (user_id, recharge_id, ",
            "rate, coin_amount, ",
            "coin_type, create_time)",
            "values (#{userId,jdbcType=VARCHAR}, #{rechargeId,jdbcType=INTEGER}, ",
            "#{rate,jdbcType=VARCHAR}, #{coinAmount,jdbcType=VARCHAR}, ",
            "#{coinType,jdbcType=VARCHAR}, #{createTime,jdbcType=TIMESTAMP})"
    })
    @SelectKey(statement = "SELECT LAST_INSERT_ID()", keyProperty = "id", before = false, resultType = Integer.class)
    int insert(Lottery record);

    @InsertProvider(type = LotterySqlProvider.class, method = "insertSelective")
    @SelectKey(statement = "SELECT LAST_INSERT_ID()", keyProperty = "id", before = false, resultType = Integer.class)
    int insertSelective(Lottery record);

    @Select({
            "select",
            "id, user_id, recharge_id, rate, coin_amount, coin_type, create_time",
            "from phone_lottery",
            "where id = #{id,jdbcType=INTEGER}"
    })
    @Results({
            @Result(column = "id", property = "id", jdbcType = JdbcType.INTEGER, id = true),
            @Result(column = "user_id", property = "userId", jdbcType = JdbcType.VARCHAR),
            @Result(column = "recharge_id", property = "rechargeId", jdbcType = JdbcType.INTEGER),
            @Result(column = "rate", property = "rate", jdbcType = JdbcType.VARCHAR),
            @Result(column = "coin_amount", property = "coinAmount", jdbcType = JdbcType.VARCHAR),
            @Result(column = "coin_type", property = "coinType", jdbcType = JdbcType.VARCHAR),
            @Result(column = "create_time", property = "createTime", jdbcType = JdbcType.TIMESTAMP)
    })
    Lottery selectByPrimaryKey(Integer id);

    @UpdateProvider(type = LotterySqlProvider.class, method = "updateByPrimaryKeySelective")
    int updateByPrimaryKeySelective(Lottery record);

    @Update({
            "update phone_lottery",
            "set user_id = #{userId,jdbcType=VARCHAR},",
            "recharge_id = #{rechargeId,jdbcType=INTEGER},",
            "rate = #{rate,jdbcType=VARCHAR},",
            "coin_amount = #{coinAmount,jdbcType=VARCHAR},",
            "coin_type = #{coinType,jdbcType=VARCHAR},",
            "create_time = #{createTime,jdbcType=TIMESTAMP}",
            "where id = #{id,jdbcType=INTEGER}"
    })
    int updateByPrimaryKey(Lottery record);

    @Select({
            "select",
            "id, user_id, recharge_id, rate, coin_amount, coin_type, create_time",
            "from phone_lottery",
            "where user_id = #{userId,jdbcType=VARCHAR} and coin_type = #{coinType,jdbcType=VARCHAR} order by create_time desc"
    })
    @Results({
            @Result(column = "id", property = "id", jdbcType = JdbcType.INTEGER, id = true),
            @Result(column = "user_id", property = "userId", jdbcType = JdbcType.VARCHAR),
            @Result(column = "recharge_id", property = "rechargeId", jdbcType = JdbcType.INTEGER),
            @Result(column = "rate", property = "rate", jdbcType = JdbcType.VARCHAR),
            @Result(column = "coin_amount", property = "coinAmount", jdbcType = JdbcType.VARCHAR),
            @Result(column = "coin_type", property = "coinType", jdbcType = JdbcType.VARCHAR),
            @Result(column = "create_time", property = "createTime", jdbcType = JdbcType.TIMESTAMP)
    })
    Page<Lottery> pageData(@Param("userId") String userId, String coinType);

    @Select({
            "select",
            "id, user_id, recharge_id, rate, coin_amount, coin_type, create_time",
            "from phone_lottery",
            "where user_id = #{userId,jdbcType=VARCHAR} and recharge_id = #{rechargeId,jdbcType=INTEGER}"
    })
    @Results({
            @Result(column = "id", property = "id", jdbcType = JdbcType.INTEGER, id = true),
            @Result(column = "user_id", property = "userId", jdbcType = JdbcType.VARCHAR),
            @Result(column = "recharge_id", property = "rechargeId", jdbcType = JdbcType.INTEGER),
            @Result(column = "rate", property = "rate", jdbcType = JdbcType.VARCHAR),
            @Result(column = "coin_amount", property = "coinAmount", jdbcType = JdbcType.VARCHAR),
            @Result(column = "coin_type", property = "coinType", jdbcType = JdbcType.VARCHAR),
            @Result(column = "create_time", property = "createTime", jdbcType = JdbcType.TIMESTAMP)
    })
    Lottery selectByUserIdAndRechargeId(@Param("userId") String userId, @Param("rechargeId") Integer rechargeId);
}