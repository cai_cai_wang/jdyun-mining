package cn.com.jdyun.mapper;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.annotations.UpdateProvider;
import org.apache.ibatis.type.JdbcType;

import cn.com.jdyun.pojo.Exchange;

public interface ExchangeMapper {
    @Delete({
        "delete from phone_exchange",
        "where id = #{id,jdbcType=INTEGER}"
    })
    int deleteByPrimaryKey(Integer id);

    @Insert({
        "insert into phone_exchange (rate, from_coin_type, ",
        "from_amount, to_coin_type, ",
        "to_amount, user_id, ",
        "create_time)",
        "values (#{rate,jdbcType=DECIMAL}, #{fromCoinType,jdbcType=VARCHAR}, ",
        "#{fromAmount,jdbcType=DECIMAL}, #{toCoinType,jdbcType=VARCHAR}, ",
        "#{toAmount,jdbcType=DECIMAL}, #{userId,jdbcType=VARCHAR}, ",
        "#{createTime,jdbcType=TIMESTAMP})"
    })
    @SelectKey(statement="SELECT LAST_INSERT_ID()", keyProperty="id", before=false, resultType=Integer.class)
    int insert(Exchange record);

    @InsertProvider(type=ExchangeSqlProvider.class, method="insertSelective")
    @SelectKey(statement="SELECT LAST_INSERT_ID()", keyProperty="id", before=false, resultType=Integer.class)
    int insertSelective(Exchange record);

    @Select({
        "select",
        "id, rate, from_coin_type, from_amount, to_coin_type, to_amount, user_id, create_time",
        "from phone_exchange",
        "where id = #{id,jdbcType=INTEGER}"
    })
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.INTEGER, id=true),
        @Result(column="rate", property="rate", jdbcType=JdbcType.DECIMAL),
        @Result(column="from_coin_type", property="fromCoinType", jdbcType=JdbcType.VARCHAR),
        @Result(column="from_amount", property="fromAmount", jdbcType=JdbcType.DECIMAL),
        @Result(column="to_coin_type", property="toCoinType", jdbcType=JdbcType.VARCHAR),
        @Result(column="to_amount", property="toAmount", jdbcType=JdbcType.DECIMAL),
        @Result(column="user_id", property="userId", jdbcType=JdbcType.VARCHAR),
        @Result(column="create_time", property="createTime", jdbcType=JdbcType.TIMESTAMP)
    })
    Exchange selectByPrimaryKey(Integer id);

    @UpdateProvider(type=ExchangeSqlProvider.class, method="updateByPrimaryKeySelective")
    int updateByPrimaryKeySelective(Exchange record);

    @Update({
        "update phone_exchange",
        "set rate = #{rate,jdbcType=DECIMAL},",
          "from_coin_type = #{fromCoinType,jdbcType=VARCHAR},",
          "from_amount = #{fromAmount,jdbcType=DECIMAL},",
          "to_coin_type = #{toCoinType,jdbcType=VARCHAR},",
          "to_amount = #{toAmount,jdbcType=DECIMAL},",
          "user_id = #{userId,jdbcType=VARCHAR},",
          "create_time = #{createTime,jdbcType=TIMESTAMP}",
        "where id = #{id,jdbcType=INTEGER}"
    })
    int updateByPrimaryKey(Exchange record);
}