package cn.com.jdyun.mapper;

import cn.com.jdyun.pojo.MinerLog;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.annotations.UpdateProvider;
import org.apache.ibatis.type.JdbcType;

public interface MinerLogMapper {
    @Delete({
            "delete from phone_miner_log",
            "where id = #{id,jdbcType=INTEGER}"
    })
    int deleteByPrimaryKey(Integer id);

    @Insert({
            "insert into phone_miner_log (user_id, rate, ",
            "cp, cur_cp, memo, ",
            "cur_type, create_time)",
            "values (#{userId,jdbcType=VARCHAR}, #{rate,jdbcType=VARCHAR}, ",
            "#{cp,jdbcType=VARCHAR}, #{curCp,jdbcType=VARCHAR}, #{memo,jdbcType=VARCHAR}, ",
            "#{createTime,jdbcType=TIMESTAMP})"
    })
    @SelectKey(statement = "SELECT LAST_INSERT_ID()", keyProperty = "id", before = false, resultType = Integer.class)
    int insert(MinerLog record);

    @InsertProvider(type = MinerLogSqlProvider.class, method = "insertSelective")
    @SelectKey(statement = "SELECT LAST_INSERT_ID()", keyProperty = "id", before = false, resultType = Integer.class)
    int insertSelective(MinerLog record);

    @Select({
            "select",
            "id, user_id, rate, cp, cur_cp, memo, create_time",
            "from phone_miner_log",
            "where id = #{id,jdbcType=INTEGER}"
    })
    @Results({
            @Result(column = "id", property = "id", jdbcType = JdbcType.INTEGER, id = true),
            @Result(column = "user_id", property = "userId", jdbcType = JdbcType.VARCHAR),
            @Result(column = "rate", property = "rate", jdbcType = JdbcType.VARCHAR),
            @Result(column = "cp", property = "cp", jdbcType = JdbcType.VARCHAR),
            @Result(column = "cur_cp", property = "curCp", jdbcType = JdbcType.VARCHAR),
            @Result(column = "memo", property = "memo", jdbcType = JdbcType.VARCHAR),
            @Result(column = "create_time", property = "createTime", jdbcType = JdbcType.TIMESTAMP)
    })
    MinerLog selectByPrimaryKey(Integer id);

    @UpdateProvider(type = MinerLogSqlProvider.class, method = "updateByPrimaryKeySelective")
    int updateByPrimaryKeySelective(MinerLog record);

    @Update({
            "update phone_miner_log",
            "set user_id = #{userId,jdbcType=VARCHAR},",
            "rate = #{rate,jdbcType=VARCHAR},",
            "cp = #{cp,jdbcType=VARCHAR},",
            "cur_cp = #{curCp,jdbcType=VARCHAR},",
            "memo = #{memo,jdbcType=VARCHAR},",
            "create_time = #{createTime,jdbcType=TIMESTAMP}",
            "where id = #{id,jdbcType=INTEGER}"
    })
    int updateByPrimaryKey(MinerLog record);
}