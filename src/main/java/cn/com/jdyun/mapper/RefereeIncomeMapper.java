package cn.com.jdyun.mapper;

import cn.com.jdyun.pojo.RefereeIncome;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.annotations.UpdateProvider;
import org.apache.ibatis.type.JdbcType;

public interface RefereeIncomeMapper {
    @Delete({
            "delete from phone_referee_income",
            "where id = #{id,jdbcType=INTEGER}"
    })
    int deleteByPrimaryKey(Integer id);

    @Insert({
            "insert into phone_referee_income (user_id, to_balance, ",
            "to_locked_coin, date, ",
            "create_time, update_time)",
            "values (#{userId,jdbcType=VARCHAR}, #{toBalance,jdbcType=DECIMAL}, ",
            "#{toLockedCoin,jdbcType=DECIMAL}, #{date,jdbcType=VARCHAR}, ",
            "#{createTime,jdbcType=TIMESTAMP}, #{updateTime,jdbcType=TIMESTAMP})"
    })
    @SelectKey(statement = "SELECT LAST_INSERT_ID()", keyProperty = "id", before = false, resultType = Integer.class)
    int insert(RefereeIncome record);

    @InsertProvider(type = RefereeIncomeSqlProvider.class, method = "insertSelective")
    @SelectKey(statement = "SELECT LAST_INSERT_ID()", keyProperty = "id", before = false, resultType = Integer.class)
    int insertSelective(RefereeIncome record);

    @Select({
            "select",
            "id, user_id, to_balance, to_locked_coin, date, create_time, update_time",
            "from phone_referee_income",
            "where id = #{id,jdbcType=INTEGER}"
    })
    @Results({
            @Result(column = "id", property = "id", jdbcType = JdbcType.INTEGER, id = true),
            @Result(column = "user_id", property = "userId", jdbcType = JdbcType.VARCHAR),
            @Result(column = "to_balance", property = "toBalance", jdbcType = JdbcType.DECIMAL),
            @Result(column = "to_locked_coin", property = "toLockedCoin", jdbcType = JdbcType.DECIMAL),
            @Result(column = "date", property = "date", jdbcType = JdbcType.VARCHAR),
            @Result(column = "create_time", property = "createTime", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "update_time", property = "updateTime", jdbcType = JdbcType.TIMESTAMP)
    })
    RefereeIncome selectByPrimaryKey(Integer id);

    @UpdateProvider(type = RefereeIncomeSqlProvider.class, method = "updateByPrimaryKeySelective")
    int updateByPrimaryKeySelective(RefereeIncome record);

    @Update({
            "update phone_referee_income",
            "set user_id = #{userId,jdbcType=VARCHAR},",
            "to_balance = #{toBalance,jdbcType=DECIMAL},",
            "to_locked_coin = #{toLockedCoin,jdbcType=DECIMAL},",
            "date = #{date,jdbcType=VARCHAR},",
            "create_time = #{createTime,jdbcType=TIMESTAMP},",
            "update_time = #{updateTime,jdbcType=TIMESTAMP}",
            "where id = #{id,jdbcType=INTEGER}"
    })
    int updateByPrimaryKey(RefereeIncome record);
}