package cn.com.jdyun.pojo;

import java.math.BigDecimal;
import java.util.Date;

import org.springframework.format.annotation.NumberFormat;

public class LockCoinReleaseLog {
    private Integer id;

    private String userId;

    private Integer lockCoinId;

    private String coinType;
    
    @NumberFormat(pattern="0.00")
    private BigDecimal lockedCoin;
    
    @NumberFormat(pattern="0.00")
    private BigDecimal amount;

    private Byte releaseType;

    private Date createTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    public Integer getLockCoinId() {
        return lockCoinId;
    }

    public void setLockCoinId(Integer lockCoinId) {
        this.lockCoinId = lockCoinId;
    }

    public String getCoinType() {
        return coinType;
    }

    public void setCoinType(String coinType) {
        this.coinType = coinType == null ? null : coinType.trim();
    }

    public BigDecimal getLockedCoin() {
        return lockedCoin;
    }

    public void setLockedCoin(BigDecimal lockedCoin) {
        this.lockedCoin = lockedCoin;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Byte getReleaseType() {
        return releaseType;
    }

    public void setReleaseType(Byte releaseType) {
        this.releaseType = releaseType;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}