package cn.com.jdyun.pojo;

import java.util.Date;

import org.springframework.format.annotation.NumberFormat;

public class TransferRecords {
    private Integer id;

    private String orderId;

    private Byte transferType;
    
    @NumberFormat(pattern="0.00")
    private String amount;
    
    @NumberFormat(pattern="0.00")
    private String fee;

    private String coinType;

    private String fromUser;

    private String toUser;

    private String memo;

    private Date createTime;

    public TransferRecords() {
        super();
    }

    public TransferRecords(String orderId, Byte transferType, String amount, String fee, String coinType,
                           String fromUser, String toUser, String memo, Date createTime) {
        super();
        this.orderId = orderId;
        this.transferType = transferType;
        this.amount = amount;
        this.fee = fee;
        this.coinType = coinType;
        this.fromUser = fromUser;
        this.toUser = toUser;
        this.memo = memo;
        this.createTime = createTime;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId == null ? null : orderId.trim();
    }

    public Byte getTransferType() {
        return transferType;
    }

    public void setTransferType(Byte transferType) {
        this.transferType = transferType;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount == null ? null : amount.trim();
    }

    public String getFee() {
        return fee;
    }

    public void setFee(String fee) {
        this.fee = fee == null ? null : fee.trim();
    }

    public String getCoinType() {
        return coinType;
    }

    public void setCoinType(String coinType) {
        this.coinType = coinType == null ? null : coinType.trim();
    }

    public String getFromUser() {
        return fromUser;
    }

    public void setFromUser(String fromUser) {
        this.fromUser = fromUser == null ? null : fromUser.trim();
    }

    public String getToUser() {
        return toUser;
    }

    public void setToUser(String toUser) {
        this.toUser = toUser == null ? null : toUser.trim();
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo == null ? null : memo.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}