package cn.com.jdyun.pojo;

import java.util.Date;

public class UserSec {
    private Integer id;
    /*用户id*/
    private String userId;
    /*用户支付密码*/
    private String secPassword;
    /*身份证号*/
    private String idCardNo;
    /*真实姓名*/
    private String idCardName;
    /*身份证正面图片路径*/
    private String idCardUp;
    /*身份证反面图片路径*/
    private String idCardDown;
    /*实名认证状态：0 未提交，1 审核中 2 审核通过，3审核不通过*/
    private Byte flag;
    /*创建时间*/
    private Date createTime;
    /*修改时间*/
    private Date updateTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    public String getSecPassword() {
        return secPassword;
    }

    public void setSecPassword(String secPassword) {
        this.secPassword = secPassword == null ? null : secPassword.trim();
    }

    public String getIdCardNo() {
        return idCardNo;
    }

    public void setIdCardNo(String idCardNo) {
        this.idCardNo = idCardNo == null ? null : idCardNo.trim();
    }

    public String getIdCardName() {
        return idCardName;
    }

    public void setIdCardName(String idCardName) {
        this.idCardName = idCardName == null ? null : idCardName.trim();
    }

    public String getIdCardUp() {
        return idCardUp;
    }

    public void setIdCardUp(String idCardUp) {
        this.idCardUp = idCardUp == null ? null : idCardUp.trim();
    }

    public String getIdCardDown() {
        return idCardDown;
    }

    public void setIdCardDown(String idCardDown) {
        this.idCardDown = idCardDown == null ? null : idCardDown.trim();
    }

    public Byte getFlag() {
        return flag;
    }

    public void setFlag(Byte flag) {
        this.flag = flag;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}