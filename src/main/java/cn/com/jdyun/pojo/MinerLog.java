package cn.com.jdyun.pojo;

import java.util.Date;

/**
 * 用户挖矿相关比例变动记录表
 * 工作量证明
 */
public class MinerLog {

    private Integer id;
    /*用户id*/
    private String userId;
    /*比例*/
    private String rate;
    /*算力变动*/
    private String cp;
    /*当前实时总算力*/
    private String curCp;
    /*备注*/
    private String memo;

    private Date createTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate == null ? null : rate.trim();
    }

    public String getCp() {
        return cp;
    }

    public void setCp(String cp) {
        this.cp = cp == null ? null : cp.trim();
    }

    public String getCurCp() {
        return curCp;
    }

    public void setCurCp(String curCp) {
        this.curCp = curCp == null ? null : curCp.trim();
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo == null ? null : memo.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}