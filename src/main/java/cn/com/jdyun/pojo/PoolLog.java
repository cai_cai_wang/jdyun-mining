package cn.com.jdyun.pojo;

import java.util.Date;

import org.springframework.format.annotation.NumberFormat;

public class PoolLog {
    private Integer id;
    
    @NumberFormat(pattern="0.00")
    private String poolBalance;
    
    @NumberFormat(pattern="0.00")
    private String coinAmount;

    private String coinType;

    private String userId;

    private String memo;

    private Date createTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPoolBalance() {
        return poolBalance;
    }

    public void setPoolBalance(String poolBalance) {
        this.poolBalance = poolBalance == null ? null : poolBalance.trim();
    }

    public String getCoinAmount() {
        return coinAmount;
    }

    public void setCoinAmount(String coinAmount) {
        this.coinAmount = coinAmount == null ? null : coinAmount.trim();
    }

    public String getCoinType() {
        return coinType;
    }

    public void setCoinType(String coinType) {
        this.coinType = coinType == null ? null : coinType.trim();
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo == null ? null : memo.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}