package cn.com.jdyun.pojo;

import java.util.Date;

import org.springframework.format.annotation.NumberFormat;

public class Gains {
    private Integer id;

    private String userId;

    private String date;
    
    @NumberFormat(pattern="0.00")
    private String vested;
    
    @NumberFormat(pattern="0.00000")
    private String rate;

    private String coinType;

    private Integer times;

    private Date createTime;

    private Date updateTime;

    public String getCoinType() {
        return coinType;
    }

    public void setCoinType(String coinType) {
        this.coinType = coinType;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date == null ? null : date.trim();
    }

    public String getVested() {
        return vested;
    }

    public void setVested(String vested) {
        this.vested = vested == null ? null : vested.trim();
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate == null ? null : rate.trim();
    }

    public Integer getTimes() {
        return times;
    }

    public void setTimes(Integer times) {
        this.times = times;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}