package cn.com.jdyun.pojo;

import java.math.BigDecimal;
import java.util.Date;

import org.springframework.format.annotation.NumberFormat;

public class RefereeIncome {
    private Integer id;

    private String userId;
    
    @NumberFormat(pattern="0.00")
    private BigDecimal toBalance;
    
    @NumberFormat(pattern="0.00")
    private BigDecimal toLockedCoin;

    private String date;

    private Date createTime;

    private Date updateTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    public BigDecimal getToBalance() {
        return toBalance;
    }

    public void setToBalance(BigDecimal toBalance) {
        this.toBalance = toBalance;
    }

    public BigDecimal getToLockedCoin() {
        return toLockedCoin;
    }

    public void setToLockedCoin(BigDecimal toLockedCoin) {
        this.toLockedCoin = toLockedCoin;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date == null ? null : date.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}