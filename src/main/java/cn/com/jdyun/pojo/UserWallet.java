package cn.com.jdyun.pojo;

import java.math.BigDecimal;
import java.util.Date;

import org.springframework.format.annotation.NumberFormat;

public class UserWallet {
    private Integer id;

    private String userId;

    private String coinType;
    
    @NumberFormat(pattern="0.00")
    private String asset;
    
    @NumberFormat(pattern="0.00")
    private String balance;
    
    @NumberFormat(pattern="0.00")
    private String mingCoin;
    
    @NumberFormat(pattern="0.00")
    private String taskCoin;
    
    @NumberFormat(pattern="0.00")
    private BigDecimal lockedCoin;
    
    @NumberFormat(pattern="0.00")
    private String freezeCoin;

    private Date createTime;

    private Date updateTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    public String getCoinType() {
        return coinType;
    }

    public void setCoinType(String coinType) {
        this.coinType = coinType == null ? null : coinType.trim();
    }

    public String getAsset() {
        return asset;
    }

    public void setAsset(String asset) {
        this.asset = asset == null ? null : asset.trim();
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance == null ? null : balance.trim();
    }

    public String getMingCoin() {
        return mingCoin;
    }

    public void setMingCoin(String mingCoin) {
        this.mingCoin = mingCoin == null ? null : mingCoin.trim();
    }

    public String getTaskCoin() {
        return taskCoin;
    }

    public void setTaskCoin(String taskCoin) {
        this.taskCoin = taskCoin == null ? null : taskCoin.trim();
    }

    public BigDecimal getLockedCoin() {
        return lockedCoin;
    }

    public void setLockedCoin(BigDecimal lockedCoin) {
        this.lockedCoin = lockedCoin;
    }

    public String getFreezeCoin() {
        return freezeCoin;
    }

    public void setFreezeCoin(String freezeCoin) {
        this.freezeCoin = freezeCoin == null ? null : freezeCoin.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}