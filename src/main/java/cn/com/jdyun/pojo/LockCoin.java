package cn.com.jdyun.pojo;

import java.math.BigDecimal;
import java.util.Date;

import org.springframework.format.annotation.NumberFormat;

public class LockCoin {
    private Integer id;

    private String userId;
    
    @NumberFormat(pattern="0.00")
    private BigDecimal initLockCoinAmount;
    
    @NumberFormat(pattern="0.00")
    private BigDecimal lockCoinAmount;
    
    @NumberFormat(pattern="0.00")
    private BigDecimal income;

    private String coinType;

    private String sequenceId;

    private Date createTime;

    private Date updateTime;

    public LockCoin() {
        super();
    }

    public LockCoin(String userId, BigDecimal initLockCoinAmount, BigDecimal lockCoinAmount, String coinType, String sequenceId,
                    Date createTime, Date updateTime) {
        super();
        this.userId = userId;
        this.initLockCoinAmount = initLockCoinAmount;
        this.lockCoinAmount = lockCoinAmount;
        this.coinType = coinType;
        this.sequenceId = sequenceId;
        this.createTime = createTime;
        this.updateTime = updateTime;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    public BigDecimal getInitLockCoinAmount() {
        return initLockCoinAmount;
    }

    public void setInitLockCoinAmount(BigDecimal initLockCoinAmount) {
        this.initLockCoinAmount = initLockCoinAmount;
    }

    public BigDecimal getLockCoinAmount() {
        return lockCoinAmount;
    }

    public void setLockCoinAmount(BigDecimal lockCoinAmount) {
        this.lockCoinAmount = lockCoinAmount;
    }

    public BigDecimal getIncome() {
        return income;
    }

    public void setIncome(BigDecimal income) {
        this.income = income;
    }

    public String getCoinType() {
        return coinType;
    }

    public void setCoinType(String coinType) {
        this.coinType = coinType == null ? null : coinType.trim();
    }

    public String getSequenceId() {
        return sequenceId;
    }

    public void setSequenceId(String sequenceId) {
        this.sequenceId = sequenceId == null ? null : sequenceId.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}