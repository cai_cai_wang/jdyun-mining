package cn.com.jdyun.pojo;

import java.math.BigDecimal;
import java.util.Date;

import org.springframework.format.annotation.NumberFormat;

public class Exchange {
    private Integer id;
    
    @NumberFormat(pattern="0.00000")
    private BigDecimal rate;
    
    private String fromCoinType;
    
    @NumberFormat(pattern="0.00")
    private BigDecimal fromAmount;

    private String toCoinType;
    
    @NumberFormat(pattern="0.00")
    private BigDecimal toAmount;

    private String userId;

    private Date createTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public BigDecimal getRate() {
        return rate;
    }

    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }

    public String getFromCoinType() {
        return fromCoinType;
    }

    public void setFromCoinType(String fromCoinType) {
        this.fromCoinType = fromCoinType == null ? null : fromCoinType.trim();
    }

    public BigDecimal getFromAmount() {
        return fromAmount;
    }

    public void setFromAmount(BigDecimal fromAmount) {
        this.fromAmount = fromAmount;
    }

    public String getToCoinType() {
        return toCoinType;
    }

    public void setToCoinType(String toCoinType) {
        this.toCoinType = toCoinType == null ? null : toCoinType.trim();
    }

    public BigDecimal getToAmount() {
        return toAmount;
    }

    public void setToAmount(BigDecimal toAmount) {
        this.toAmount = toAmount;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}