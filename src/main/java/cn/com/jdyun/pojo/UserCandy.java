package cn.com.jdyun.pojo;

import java.math.BigDecimal;
import java.util.Date;

import org.springframework.format.annotation.NumberFormat;

public class UserCandy {
    private Integer id;

    private String userId;
    
    @NumberFormat(pattern="0.00")
    private BigDecimal rewardAll;
    
    @NumberFormat(pattern="0.00")
    private BigDecimal rewardInviteAll;
    
    @NumberFormat(pattern="0.00")
    private BigDecimal freeAll;
    
    @NumberFormat(pattern="0.00")
    private BigDecimal freeInviteAll;

    private Date createTime;

    private Date updateTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    public BigDecimal getRewardAll() {
        return rewardAll;
    }

    public void setRewardAll(BigDecimal rewardAll) {
        this.rewardAll = rewardAll;
    }

    public BigDecimal getRewardInviteAll() {
        return rewardInviteAll;
    }

    public void setRewardInviteAll(BigDecimal rewardInviteAll) {
        this.rewardInviteAll = rewardInviteAll;
    }

    public BigDecimal getFreeAll() {
        return freeAll;
    }

    public void setFreeAll(BigDecimal freeAll) {
        this.freeAll = freeAll;
    }

    public BigDecimal getFreeInviteAll() {
        return freeInviteAll;
    }

    public void setFreeInviteAll(BigDecimal freeInviteAll) {
        this.freeInviteAll = freeInviteAll;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}