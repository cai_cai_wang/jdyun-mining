package cn.com.jdyun.dto;

import java.io.Serializable;
import java.util.List;

/**
 * @author yzping
 * @date Created in 下午 12:00 2018/8/25 0025
 */
public class BdexResult implements Serializable {

    private static final long serialVersionUID = 1L;
    private Integer total;        //记录总数

    private List<?> rows;        //当前页的记录

    public BdexResult() {
    }

    public BdexResult(Integer total, List<?> rows) {
        this.total = total;
        this.rows = rows;
    }

    public BdexResult(Long total, List<?> rows) {
        this.total = total.intValue();
        this.rows = rows;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public List<?> getRows() {
        return rows;
    }

    public void setRows(List<?> rows) {
        this.rows = rows;
    }
}
