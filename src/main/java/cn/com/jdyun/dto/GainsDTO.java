package cn.com.jdyun.dto;

import java.io.Serializable;

import org.springframework.format.annotation.NumberFormat;

public class GainsDTO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private Integer id;

    private String userId;
    
    @NumberFormat(pattern="0.00000")
    private String date;
    
    @NumberFormat(pattern="0.00")
    private String vested;

    private String coinType;

    private String context;

    public String getCoinType() {
        return coinType;
    }

    public void setCoinType(String coinType) {
        this.coinType = coinType;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getVested() {
        return vested;
    }

    public void setVested(String vested) {
        this.vested = vested;
    }

    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }

}
