package cn.com.jdyun.dto;

/**
 * @author yzping
 * @date Created in 上午 9:28 2018/8/21 0021
 */
public class ANS_ALERT {
    public static final String NORMAL = "normal"; // 正常状态，不需要处理
    public static final String WARNING = "warning"; // 告警提示，需要有提示信息
    public static final String SUCCESS = "success"; // 成功提示，需要有提示信息
    public static final String ERROR = "error"; // 失败提示，需要有提示信息
    public static final String TIMEOUT = "timeout"; // 超时提示，需要有提示信息
}
