package cn.com.jdyun.dto;


import cn.com.jdyun.pojo.TransferRecords;

public class TransferRecordsDTO extends TransferRecords {

    private String fromUserNickName;

    private String toUserNickName;

    public TransferRecordsDTO() {
        super();
    }

    public TransferRecordsDTO(TransferRecords record) {
        super();
        this.setId(record.getId());
        this.setOrderId(record.getOrderId());
        this.setTransferType(record.getTransferType());
        this.setAmount(record.getAmount());
        this.setFee(record.getFee());
        this.setCoinType(record.getCoinType());
        this.setFromUser(record.getFromUser());
        this.setToUser(record.getToUser());
        this.setMemo(record.getMemo());
        this.setCreateTime(record.getCreateTime());
    }

    public TransferRecordsDTO(String fromUserNickName, String toUserNickName) {
        super();
        this.fromUserNickName = fromUserNickName;
        this.toUserNickName = toUserNickName;
    }

    public String getFromUserNickName() {
        return fromUserNickName;
    }

    public void setFromUserNickName(String fromUserNickName) {
        this.fromUserNickName = fromUserNickName;
    }

    public String getToUserNickName() {
        return toUserNickName;
    }

    public void setToUserNickName(String toUserNickName) {
        this.toUserNickName = toUserNickName;
    }
}
