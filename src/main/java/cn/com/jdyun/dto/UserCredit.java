package cn.com.jdyun.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * 用户登陆之后的凭证信息
 *
 * @author Administrator
 */
public class UserCredit implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    //用户id
    private String id;
    //用户名
    private String username;
    //用户昵称
    private String nickName;
    //用户图像
    private String headPic;
    //超级节点：0 否 1是
    private Boolean superNode;
    //用户推荐码
    private String code;
    //用户状态：0 正常 1锁定 2禁用
    private Byte flag;
    //用户角色：0 普通用户，1管理员
    private Boolean admin;
    //推荐人id
    private String referee;
    //等级
    private Integer level;
    //等级
    private Integer texLevel;
    //创建时间
    private Date createTime;
    //最近更新时间
    private Date updateTime;
    //实名认证进度
    private Byte secFlag;

    public Integer getLevel() {
        return level;
    }

    public void setTexLevel(Integer texLevel) {
        this.texLevel = texLevel;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getHeadPic() {
        return headPic;
    }

    public void setHeadPic(String headPic) {
        this.headPic = headPic;
    }

    public Boolean getSuperNode() {
        return superNode;
    }

    public void setSuperNode(Boolean superNode) {
        this.superNode = superNode;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Byte getFlag() {
        return flag;
    }

    public void setFlag(Byte flag) {
        this.flag = flag;
    }

    public Boolean getAdmin() {
        return admin;
    }

    public void setAdmin(Boolean admin) {
        this.admin = admin;
    }

    public String getReferee() {
        return referee;
    }

    public void setReferee(String referee) {
        this.referee = referee;
    }

    public Integer getTexLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

	public Byte getSecFlag() {
		return secFlag;
	}

	public void setSecFlag(Byte secFlag) {
		this.secFlag = secFlag;
	}

}
