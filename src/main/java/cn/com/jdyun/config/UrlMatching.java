package cn.com.jdyun.config;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author yzping
 * @date Created in 下午 4:57 2018/9/14 0014
 */
@Configuration
@EnableAutoConfiguration
@ConfigurationProperties(prefix = "url.matching")
public class UrlMatching {

    private String pathes;

    public String getPaths() {
        return pathes;
    }

    public void setPaths(String pathes) {
        this.pathes = pathes;
    }

}
