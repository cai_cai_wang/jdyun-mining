package cn.com.jdyun.config;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author yzping
 * @date Created in 下午 2:10 2018/8/20 0020
 */
@Configuration
@EnableAutoConfiguration
@ConfigurationProperties(prefix = "bdex.private.tel")
public class TelConfig {

    private String userName;

    private String password;

    private String URL;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getURL() {
        return URL;
    }

    public void setURL(String URL) {
        this.URL = URL;
    }
}
