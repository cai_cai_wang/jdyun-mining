package cn.com.jdyun.config;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author yzping
 * @date Created in 下午 3:18 2018/8/20 0020
 */
@Configuration
@EnableAutoConfiguration
@ConfigurationProperties(prefix = "bdex.private.email")
public class EmailConfig {
    private String from;
    private String pwd;
    private String host;
    private String port;

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }
}
