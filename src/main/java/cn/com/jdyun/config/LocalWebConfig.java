package cn.com.jdyun.config;

import cn.com.jdyun.interceptor.AllowCrossDomainInterceptor;
import cn.com.jdyun.interceptor.AuthHandlerInterceptor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.CacheControl;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import java.util.concurrent.TimeUnit;

@Configuration
public class LocalWebConfig extends WebMvcConfigurationSupport {

    @Value("${spring.servlet.multipart.location}")
    private String filePath;

    @Value("${spring.profiles.active}")
    private String env;
    /**
     * 将自定义拦截器作为Bean写入配置
     * @return
     */
    @Bean
    public AuthHandlerInterceptor myInterceptor() {
        return new AuthHandlerInterceptor();
    }
    @Override
    protected void addInterceptors(InterceptorRegistry registry) {
        //跨域拦截器
        registry.addInterceptor(new AllowCrossDomainInterceptor(env)).addPathPatterns("/**");
        //授权认证
        registry.addInterceptor(myInterceptor()).addPathPatterns("/**").excludePathPatterns("/pic/**");

        super.addInterceptors(registry);
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        System.out.println(filePath);
        registry.addResourceHandler("/pic/**")
                .addResourceLocations("file:" + filePath)
                .setCacheControl(CacheControl.maxAge(1, TimeUnit.HOURS).cachePublic());
    }
}
