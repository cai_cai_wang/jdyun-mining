package cn.com.jdyun;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;

@SpringBootApplication
@EnableScheduling
@EnableRedisHttpSession(maxInactiveIntervalInSeconds = 1800)
public class JdyunApplication  extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(JdyunApplication.class, args);
        System.out.println("JdyunApplication 启动成功!");
    }
    
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(JdyunApplication.class);
    }
}
