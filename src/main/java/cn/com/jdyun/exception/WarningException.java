package cn.com.jdyun.exception;

/**
 * @author yzping
 * @date Created in 下午 3:14 2018/8/17 0017
 */
public class WarningException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    private String code;
    private String redirectURL;

    public WarningException(String message, Throwable cause) {
        super(message, cause);
    }

    public WarningException(Throwable cause) {
        super(cause.getMessage(), cause);
    }

    public WarningException(String message) {
        super(message);
    }

    public WarningException(String message, String code) {
        super(message);
        this.code = code;
    }

    public WarningException(String message, String code, String redirectURL) {
        super(message);
        this.code = code;
        this.redirectURL = redirectURL;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getRedirectURL() {
        return redirectURL;
    }

    public void setRedirectURL(String redirectURL) {
        this.redirectURL = redirectURL;
    }
}
