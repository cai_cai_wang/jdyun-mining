package cn.com.jdyun.exception;

/**
 * @author yzping
 * @date Created in 下午 2:07 2018/8/17 0017
 */
public class BdexGatewayException extends Exception {
    private static final long serialVersionUID = 1L;

    private Integer code;

    public BdexGatewayException(String message, Throwable cause) {
        super(message, cause);
    }

    public BdexGatewayException(String message) {
        super(message);
    }

    public BdexGatewayException(String message, Integer code) {
        super(message);
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

}
