package cn.com.jdyun.exception;


import cn.com.jdyun.util.reponse.Result;

public class AuthException extends Exception {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public AuthException(Result result) {
        super();
        this.result = result;
    }

    private Result result;

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

}
