package cn.com.jdyun.exception;

/**
 * @author yzping
 * @date Created in 上午 9:32 2018/8/21 0021
 */
public class SuccessAlertException extends Exception {

    private static final long serialVersionUID = 1L;

    public SuccessAlertException(String message, Throwable cause) {
        super(message, cause);
    }

    public SuccessAlertException(String message) {
        super(message);
    }

}
